import React from 'react';
import { Link } from "react-router-dom";

import './Breadcrumb.css';

class Breadcrumb extends React.Component {
    render() {
        return  <>
            { this.props.currentPage && 
                <nav className="breadcrumb">
                    <span>
                        <Link to={ `/${ this.props.currentSection.id }/` }>
                            { this.props.currentSection.completeName }
                        </Link>
                        <span className="separator">&gt;</span>
                    </span>
                    <span>
                        { this.props.currentPage.group.label }
                        <span className="separator">&gt;</span>
                    </span>
                    <span>
                        { this.props.currentPage.title }
                    </span>
                </nav>
            }
        </>
    }
}

export default Breadcrumb;
