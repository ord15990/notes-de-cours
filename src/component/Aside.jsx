import React from 'react';
import withOnClickOutside from './withOnClickOutside'
import AsideSection from './AsideSection'
import data from '../data';

import './Aside.css';

class Aside extends React.Component {
    constructor(props){
        super(props);
        this.asideRef = React.createRef();
        this.asideButtonRef = React.createRef();
    }

    componentDidMount() {
        this.props.addClickOutsideListener(this.props.closeAside);
        this.props.addInsideElement(this.asideRef.current);
        this.props.addInsideElement(this.asideButtonRef.current);
    }

    componentWillUnmount() {
        this.props.removeClickOutsideListener(this.props.closeAside);
        this.props.removeInsideElement(this.asideRef.current);
        this.props.removeInsideElement(this.asideButtonRef.current);
    }

    render() {
        return <>
            <aside ref={ this.asideRef } className={ this.props.open ? "open" : "" }>
                <nav>
                    <ul>
                        { data.sections.map((section, indexSection) => (
                            <li key={ indexSection }>
                                <AsideSection 
                                    { ...section } 
                                    current={ section === this.props.currentSection }
                                    currentPage={ this.props.currentPage }
                                    open={ this.props.open }
                                    closeAside={ this.props.closeAside } />
                            </li>
                        )) }
                    </ul>
                </nav>
            </aside>
            <button ref={ this.asideButtonRef } onClick={ this.props.toggleAside }>
                <i className="material-icons icon">keyboard_arrow_right</i>
            </button>
        </>
    }
}

export default withOnClickOutside(Aside);
