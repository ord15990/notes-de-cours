import React from 'react';
import { LightAsync as SyntaxHighlighter } from 'react-syntax-highlighter';
import { ThemeContext } from '../ThemeContext';
import settings from '../settings';

import styles from './CodeBlock.module.css';

const languages = settings.syntaxHiglight.languages;
const themes = settings.syntaxHiglight.themes;

for(let language in languages){
    if(!languages.hasOwnProperty(language)){
        continue;
    }

    SyntaxHighlighter.registerLanguage(language, languages[language].parser)
}

class CodeBlock extends React.Component {
    render(){
        return <div className={ styles.container }>
            <div className={ styles.tag }>{ languages[this.props.language].name }</div>
            <ThemeContext.Consumer>
                { themeContext => (
                    <SyntaxHighlighter language={ this.props.language } style={ themes[themeContext.getSyntaxHighlightStyle()] }>
                        { this.props.children }
                    </SyntaxHighlighter>
                )}
            </ThemeContext.Consumer>
        </div>
    }
}

export default CodeBlock
