import React from 'react'

import './Bubble.css'

class Bubble extends React.Component {
    render(){
        return  <div className="bubble">
                    <span className="bubble-name">{ this.props.name }</span>
                    <span className="bubble-number">{ this.props.number }</span>
                </div>
    }
}

export default Bubble
