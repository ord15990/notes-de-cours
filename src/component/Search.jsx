import React from 'react'
import { Link, withRouter } from 'react-router-dom';
import queryString from 'query-string'
import SearchEngine from '../searchEngine'
import './Search.css'

class Search extends React.Component {
    constructor(props){
        super(props);
        this.state = { results: [] }
        this.searchEngine = new SearchEngine();
    }

    componentDidMount() {
        const value = queryString.parse(this.props.location.search).value;
        if(!value){
            this.props.history.push('/');
        }

        let results = this.searchEngine.search(value);
        this.setState({ results: results })
    }

    render(){
        return <section className="search-result">
            <h1>Résultats de recherche</h1>
            <ul>
                { this.state.results.map((result, i) => (
                    <li key={ i }>
                        { result.page.component ? 
                            <Link to={ `/${ result.sectionId }/${ result.page.id }` }>{ result.page.title }</Link> :
                            <a href={ result.page.url } target="_blank" rel="noopener noreferrer">{ result.page.title }</a>
                        }
                        
                    </li>
                )) }
            </ul>
        </section>
    }
}

export default withRouter(Search);
