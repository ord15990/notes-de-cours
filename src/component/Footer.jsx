import React from 'react';

import logoECite from '../resources/e-cite.svg'
import logoLaCite from '../resources/la-cite.png'

import './Footer.css'

class Footer extends React.Component {
    render() {
        return  <footer className={ (this.props.hidden ? "hidden" : "") }>
                    <div className="link-images">
                        <a href="https://www.collegelacite.ca/" target="_blank" rel="noopener noreferrer"><img src={ logoLaCite } alt="Logo La Cité" /></a>
                        <a href="https://ecite.lacitec.on.ca/" target="_blank" rel="noopener noreferrer"><img src={ logoECite } alt="Logo eCité" /></a>
                    </div>
                    <p className="copyright">© Collège La Cité</p>
                    <p className="credits">Développé par Jonathan Wilkie | Design par Catherine Wilkie</p>
                </footer>
    }
}

export default Footer;
