import React from 'react';

import styles from './Situation.module.css';

class Situation extends React.Component {
    render() {
        return <p className={ styles.situation }>
            { this.props.children }
        </p>;
    }
}

export default Situation;