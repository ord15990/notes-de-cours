import React from 'react';

import styles from './ColoredBox.module.css'

class ColoredBox extends React.Component {
    render() {
        return <div className={ styles.box }>
            <p className={ styles.heading }>{ this.props.heading }</p>
            <div>{ this.props.children }</div>
        </div>
    }
}

export default ColoredBox;
