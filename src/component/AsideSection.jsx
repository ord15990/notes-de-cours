import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import styles from './AsideSection.module.css';

class AsideSection extends React.Component {
    currentPageIsInGroup = group => {
        return this.props.currentPage && 
               group.id === this.props.currentPage.group.id;
    }

    render() {
        return <>
            <Link to={ `/${ this.props.id }/` } onClick={ this.props.closeAside }>
                { this.props.completeName }
            </Link>
            { this.props.current && (
                <ul className={ styles.sublist }>
                    { Object.values(this.props.groups).map((group, i) => (
                        <li key={ i }>
                            <span className={ this.currentPageIsInGroup(group) ? styles.active : '' }>{ group.label }</span>
                            <ul className={ styles.sublist }>
                                { group.pages.map((page, i) => (
                                    <li key={ i }>
                                        { page.component ? 
                                            <NavLink to={ `./${ page.id }` } onClick={ this.props.closeAside } className={ styles['page-link'] } activeClassName={ styles.active }>
                                                { page.title }
                                            </NavLink> :
                                            <a href={ page.url } target="_blank" rel="noopener noreferrer" className={ styles['page-link'] }>{ page.title }</a>
                                        }
                                    </li>
                                )) }
                            </ul>
                        </li>
                    )) }
                </ul>
            ) }
        </>
    }
}

export default AsideSection;
