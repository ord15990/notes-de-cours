import React from 'react';

import styles from './Video.module.css'

class Video extends React.Component {
    render() {
        return <div className={ styles.video }>
            <div>
                <iframe 
                    title={ this.props.title }
                    src={ this.props.src }
                    frameBorder="0" 
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                    allowFullScreen>
                </iframe>
            </div>
        </div>
    }
}

export default Video;