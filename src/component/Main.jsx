import React from 'react';
import { Switch, Route, Redirect } from "react-router-dom";

import routes from '../routes'
import Breadcrumb from './Breadcrumb';
import ToTop from './ToTop';

import './Main.css'

function withCheckMainOverflow(WrappedComponent, checkCausingOverflow){
    return class extends React.Component {
        componentDidMount() {
            checkCausingOverflow();
        }

        render(){
            return <WrappedComponent { ...this.props } />
        }
    }
}

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.main = React.createRef();
        this.state = {
            overflow: undefined,
            bigOverflow: undefined
        }
        
        window.onresize = this.checkCausingOverflow;
    }

    checkCausingOverflow = () => {
        const overflow = window.innerHeight < document.body.offsetHeight;
        const bigOverflow = window.innerHeight * 1.5 < document.body.offsetHeight;
        if(this.state.overflow !== overflow || this.state.bigOverflow !== bigOverflow){
            this.setState({ 
                overflow: overflow,
                bigOverflow: bigOverflow 
            });
        }
    }

    render() {
        return  <main className={ 
                    (this.props.hidden ? "hidden" : "") + " " + 
                    (this.state.overflow ? "overflow" : "") + " " + 
                    (this.state.bigOverflow ? "big-overflow" : "") + " " +
                    (this.props.currentPage ? "page" : "") }>
                    <div>
                        <Breadcrumb 
                            currentSection={ this.props.currentSection } 
                            currentPage={ this.props.currentPage } />

                        { this.props.currentPage && 
                            <h1>{ this.props.currentPage.title }</h1>
                        }

                        <React.Suspense fallback={ <div>Chargement...</div> }>
                            <Switch>
                                { routes.map((route, i) => (
                                    <Route 
                                        key={ i } 
                                        path={ route.path } 
                                        exact={ route.exact }
                                        strict={ route.strict }
                                        render={ () => {
                                            const Component = withCheckMainOverflow(route.component, this.checkCausingOverflow);
                                            return <Component {...route.props} />
                                        } } />
                                )) }
                                <Redirect to="/" />
                            </Switch>
                        </React.Suspense>
                    </div>
                    <ToTop />
                </main>
    }
}

export default Main;
