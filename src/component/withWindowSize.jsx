import React from 'react';

function withWindowSize(WrappedComponent){
    return class WithWindowSize extends React.Component {
        constructor(props){
            super(props);
            this.state = {
                windowWidth: window.innerWidth,
                windowHeight: window.innerHeight
            }
        }

        updateSize = () => {
            this.setState({
                windowWidth: window.innerWidth,
                windowHeight: window.innerHeight
            });
        }

        componentDidMount() {
            window.addEventListener('resize', this.updateSize);
        }
        
        componentWillUnmount() {
            window.removeEventListener('resize', this.updateSize);
        }

        render(){
            return <WrappedComponent 
                windowWidth={ this.state.windowWidth } 
                windowHeight={ this.state.windowHeight } 
                {...this.props} />;
        }
    };
}

export default withWindowSize;
