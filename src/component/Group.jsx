import React from 'react';
import { Link, withRouter } from "react-router-dom";

import data from '../data'

import './Group.css'

class Group extends React.Component {
    render() {
        return <>
            <h1>{ this.props.title }</h1>
            { data.sections.filter((section) => section.groups[this.props.id]).map((section, i) => 
                <React.Fragment key={ i }>
                    <h2>{ section.completeName } - { section.title }</h2>
                    <ul className="link-list">
                        { section.groups[this.props.id].pages.map((page, i) => (
                            <li key={ i }>
                                { page.component ? 
                                    <Link to={ `/${ section.id }/${ page.id }` }>{ page.title }</Link> :
                                    <a href={ page.url } target="_blank" rel="noopener noreferrer">{ page.title }</a>
                                }
                            </li>
                        )) }
                    </ul>
                </React.Fragment>
            ) }
        </>
    }
}

export default withRouter(Group);
