import React from 'react';

import styles from './DownloadBlock.module.css';

class File extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            name: this.props.name.substring(
                0, this.props.name.lastIndexOf('.')
            ), 
            extension: this.props.name.substring(
                this.props.name.lastIndexOf('.')
            ).toLowerCase(), 
            size: 0 
        };
    }

    isArchive = () => {
        return ['.zip', '.7z', '.tar', '.gz', '.rar', '.bz2']
            .includes(this.state.extension);
    }

    displaySize = () => {
        const UNIT = ['B', 'KB', 'MB', 'GB'];
        let displaySize = this.state.size;
        let unitIndex = 0;
        while(displaySize > 100){
            displaySize /= 1024;
            unitIndex++;
        }

        return displaySize.toFixed(2) + UNIT[unitIndex];
    }

    componentDidMount(){
        fetch(this.props.path, { method: 'HEAD' }).then((response) => {
            this.setState({ size: response.headers.get('content-length') });
        });
    }

    render(){
        return <a className={ styles.file } href={ this.props.path } download={ this.props.name }>
            <i className="material-icons">
                { this.isArchive() ? 'archive' : 'insert_drive_file' }
            </i>
            <div className={ styles.info }>
                <span className={ styles.name }>
                    <span>{ this.state.name }</span>
                    <span>{ this.state.extension }</span>
                </span>
                <span className={ styles.size }>{ this.displaySize() }</span>
            </div>
        </a>
    }
}

class Download extends React.Component {
    static File = File;

    render(){
        return <div className={ styles.block }>
            { this.props.children }
        </div>
    }
}

export default Download
