import React from 'react';
import { Link } from "react-router-dom";

import Bubble from './Bubble'
import Utils from '../Utils';
import data from '../data';

import './Home.css';

class Home extends React.Component {

    render() {
        return <>
            {/*<h1>Bienvenue!</h1>
            <p>
            Pokem ipsum dolor sit amet Ferrothorn they're comfy and easy to wear Breloom Beautifly Exeggcute Gliscor. Fog Badge Ambipom Volcarona Jellicent Ariados Simisear Relicanth. 
            Sonic Boom ut enim ad minim veniam Stantler Oshawott Ditto anim id est laborum Nidoking. Fire I'm on the road to Viridian City Shuppet Rainbow Badge Scyther Minun Flaaffy. 
            Ghost Raichu Crawdaunt Sandile Pinsir Primeape Quagsire.
            </p>*/}
            <ul className="home-list">
                { data.sections.map((section, index) => (
                    <li key={ index } className="section-info">
                        <Bubble name={ section.name } number={ Utils.formatUnsignedInt(index, 2) } />
                        <div className="section-description">
                            <Link to={ `/${ section.id }/`}><h2>{ section.title }</h2></Link>
                            <p>{ section.description }</p>
                        </div>
                    </li>
                )) }
            </ul>
        </>
    }
}

export default Home;
