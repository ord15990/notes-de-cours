import React from 'react'

import './Switch.css'

class Switch extends React.Component {
    constructor(props){
        super(props);
        this.state = { checked: this.props.checked }
    }

    handleChange = (e) => {
        this.setState({ checked: e.target.checked });

        this.props.onChange(e);
    }

    render = () => {
        return  <label className="switch">
                    <input type="checkbox" checked={ this.state.checked } onChange={ this.handleChange } />
                    <span className="slider"></span>
                </label>
    }
}

export default Switch;