import React from 'react'

import './InlineCode.css'

class InlineCode extends React.Component {
    render(){
        return <span className="inline-code">
            { this.props.children }
        </span>
    }
}

export default InlineCode
