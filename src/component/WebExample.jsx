import React from 'react';
import PropTypes from 'prop-types';
import CodeBlock from './CodeBlock';

import styles from './WebExample.module.css';

/**
 * Additionnal height (in pixel) to give to iframe to be sure to clear an 
 * horizontal scrollbar.
 */
const IFRAME_HEIGHT_BUFFER = 20;

/**
 * Code component that sould be inside the WebExample component.
 */
class Code extends React.Component {
    render() {
        return <>
            { this.props.display &&
                <CodeBlock language={ this.props.type }>
                    { this.props.children }
                </CodeBlock>
            }
        </>
    }
}

// Properties validation
Code.propTypes = {
    display: PropTypes.bool,
    type: function(props, propName, componentName){
        if(!['html', 'css', 'js'].includes(props[propName])){
            return new Error(
                'Invalid prop `' + propName + '` supplied to `' + 
                componentName + '`. The prop `' + propName + '` should ' + 
                'have a value of either `html`, `css` or `js`. Validation ' + 
                'failed.'
              );
        }
    }
}

// Properties default value
Code.defaultProps = {
    display: true
}

/**
 * Web example component that can contain HTML, CSS or Javascript code to 
 * display as code and in a frame.
 */
class WebExample extends React.Component {
    static Code = Code;

    constructor(props){
        super(props)
        this.iframe = React.createRef();
    }

    /**
     * Get code text from child Code component and build it to a working html 
     * string.
     */
    getCode = () => {
        let code = { html: '', css: '', js: ''}
        const codeTypes = [
            { name: 'html' }, 
            { name: 'css', tag: 'style' }, 
            { name: 'js', tag: 'script' }
        ];

        React.Children.forEach(this.props.children, child => {
            if(child.type === Code){
                for(let type of codeTypes){
                    if(child.props.type === type.name){
                        if(type.tag){
                            code[type.name] = 
                                `<${ type.tag }>` + 
                                `${ child.props.children }` + 
                                `</${ type.tag }>`;
                        }
                        else{
                            code[type.name] = child.props.children;
                        }
                    }
                }
            }
        });

        return code.css + code.html + code.js;
    }

    /**
     * Write the code to the iframe when its loaded.
     */
    iframeLoaded = () => {
        const iFrameDocument = this.iframe.current.contentWindow.document;

        // Write to iframe
        iFrameDocument.open();
        iFrameDocument.write(this.getCode());
        iFrameDocument.close();

        // Set iframe height
        for(let img of iFrameDocument.images){
            img.onload = () => { 
                this.iframe.current.height = iFrameDocument.body.scrollHeight + IFRAME_HEIGHT_BUFFER;
            } 
        }
    
        this.iframe.current.height = iFrameDocument.body.scrollHeight + IFRAME_HEIGHT_BUFFER;
    }

    componentDidMount() {
        this.iframeLoaded();
    }

    render() {
        return <div className={ styles.container }>
            { this.props.children }
            <iframe ref={ this.iframe } title={ this.props.title }></iframe>
        </div>
    }
}

export default WebExample;
