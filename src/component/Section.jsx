import React from 'react'
import { Link } from 'react-router-dom';

import Bubble from './Bubble'

import './Section.css'

class Section extends React.Component {
    render(){
        return <>
            <div className="section-header">
                <Bubble name={ this.props.name } number={ this.props.number } />
                <h1>{ this.props.title }</h1>
            </div>

            { Object.values(this.props.groups).map((group, i) => (
                <section key={ i } className="pages-lists">
                    <h2>{ group.label }</h2>
                    <ul>
                        { group.pages.map((page, i) => (
                            <li key={ i }>
                                { page.component ?
                                    <Link to={ page.id }>{ page.title }</Link> :
                                    <a href={ page.url } target="_blank" rel="noopener noreferrer">{ page.title }</a>
                                }
                            </li>
                        )) }
                    </ul>
                </section>
            )) }
        </>
    }
}

export default Section;
