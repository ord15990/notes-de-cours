import React from 'react';
import { Link, NavLink } from "react-router-dom";
import { ThemeContext } from '../ThemeContext'
import Logo from './Logo'
import SearchBar from './SearchBar'
import Switch from './Switch'
import withOnClickOutside from './withOnClickOutside';

import groups from '../data/groups.json';

import './Header.css'

class Header extends React.Component {
    constructor(props){
        super(props);
        this.headerRef = React.createRef();
        this.state = { searchBarIsOpen: false }
    }

    handleThemeSwitch = (setTheme) => {
        return (e) => {
            setTheme(e.target.checked ? 'dark' : 'light');
        }
    }

    onToggleSearchBar = () => {
        this.setState((state) => {
            return { searchBarIsOpen: !state.searchBarIsOpen }
        });
    }

    componentDidMount() {
        this.props.addClickOutsideListener(this.props.closeMenu);
        this.props.addInsideElement(this.headerRef.current);
    }

    componentWillUnmount() {
        this.props.removeClickOutsideListener(this.props.closeMenu);
        this.props.removeInsideElement(this.headerRef.current);
    }

    render() {
        return <header id="top" ref={ this.headerRef } className={ this.props.isMenuOpen ? "open" : "" }>
            <nav>
                <div className="menu">
                    <Link to="/"><Logo /></Link>
                    <button onClick={ this.props.toggleMenu }><i className="material-icons">menu</i></button>
                </div>
                <ul className="list">
                    { Object.keys(groups).map((groupId, i) => (
                        <li key={ i } className={ this.state.searchBarIsOpen ? "hidden" : "" }>
                            <NavLink to={ '/group/' + groupId } activeClassName="active" onClick={ this.props.closeMenu }>
                                { groups[groupId].label }
                            </NavLink>
                        </li>
                    )) }
                    <li className="search-menu-item">
                        <SearchBar visible={ this.state.searchBarIsOpen } onSearch={ this.props.closeMenu } onToggleSearchBar={ this.onToggleSearchBar }/>
                    </li>
                    <li className="switch-menu-item">
                        <ThemeContext.Consumer> 
                            { themeContext => (
                                <Switch checked={ themeContext.theme === 'dark' } onChange={ this.handleThemeSwitch(themeContext.setTheme) } />
                            )} 
                        </ThemeContext.Consumer>
                    </li>
                </ul>
            </nav>
        </header>;
    }
}

export default withOnClickOutside(Header);
