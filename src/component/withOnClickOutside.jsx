import React from 'react';

function withOnClickOutside(WrappedComponent){
    return class WithOnClickOutside extends React.Component {
        constructor(props){
            super(props);
            this.listeners = new Set();
            this.elements = new Set();
        }

        onClick = (event) => {
            // If one of the elements contains the target, we dont execute the listeners
            for(let element of this.elements){
                if(element.contains(event.target)){
                    return;
                }
            }

            for (let listener of this.listeners){
                listener(event);
            }
        }

        addInsideElement = (element) => {
            this.elements.add(element);
        }

        removeInsideElement = (element) => {
            this.elements.delete(element);
        }

        addClickOutsideListener = (func) => {
            this.listeners.add(func);
        }

        removeClickOutsideListener = (func) => {
            this.listeners.delete(func)
        }

        componentDidMount() {
            document.addEventListener('mousedown', this.onClick);
        }
        
        componentWillUnmount() {
            document.removeEventListener('mousedown', this.onClick);
        }

        render(){
            return <WrappedComponent 
                addInsideElement={ this.addInsideElement }
                removeInsideElement={ this.removeInsideElement }
                addClickOutsideListener={ this.addClickOutsideListener } 
                removeClickOutsideListener={ this.removeClickOutsideListener }
                {...this.props} />;
        }
    };
}

export default withOnClickOutside;
