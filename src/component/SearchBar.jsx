import React from 'react';
import { withRouter } from 'react-router-dom'
import withWindowSize from './withWindowSize'
import withOnClickOutside from './withOnClickOutside'
import './SearchBar.css'

class SearchBar extends React.Component {
    constructor(props){
        super(props);
        this.searchInputRef = React.createRef();
        this.submitInputRef = React.createRef();
        this.isInTransition = false;
    }

    toggleSearchBar = () => {
        if(this.isInTransition || window.innerWidth < 640){
            return;
        }

        this.isInTransition = true;
        this.searchInputRef.current.addEventListener("transitionend", this.focusInput, false);

        if(this.props.onToggleSearchBar){
            this.props.onToggleSearchBar();
        }
    }

    closeSearchBar = () => {
        if(this.props.visible){
            this.toggleSearchBar();
        }
    }

    focusInput = () => {
        this.searchInputRef.current.removeEventListener("transitionend", this.focusInput);
        this.isInTransition = false;

        if(this.props.visible){
            this.searchInputRef.current.focus();
        }
    }

    submitSearch = (event) => {
        event.preventDefault();
        if(this.searchInputRef.current.value.length > 0){
            this.props.history.push(`/search?value=${ encodeURI(this.searchInputRef.current.value) }`);
            this.searchInputRef.current.value = '';
            this.closeSearchBar();
            if(this.props.onSearch){
                this.props.onSearch();
            }
        }
    }

    componentDidMount() {
        this.props.addClickOutsideListener(this.closeSearchBar);
        this.props.addInsideElement(this.searchInputRef.current);
        this.props.addInsideElement(this.submitInputRef.current);
    }

    componentWillUnmount() {
        this.props.removeClickOutsideListener(this.closeSearchBar);
        this.props.removeInsideElement(this.searchInputRef.current);
        this.props.removeInsideElement(this.submitInputRef.current);
    }

    render() {
        return <form className="search-bar" onSubmit={ this.submitSearch }>
            <button type="button" disabled={ this.props.windowWidth < 640 ? true : false } onClick={ this.toggleSearchBar }>
                <i className="material-icons">search</i>
            </button>
            <input type="search" ref={ this.searchInputRef } className={ (this.props.visible ? "visible" : "") } />
            <input type="submit" ref={ this.submitInputRef } className={ (this.props.visible ? "visible" : "") } value="Recherche" />
        </form>
    }
}

export default withOnClickOutside(withRouter(withWindowSize(SearchBar)));
