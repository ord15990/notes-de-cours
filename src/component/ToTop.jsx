import React from 'react';

import './ToTop.css';

class ToTop extends React.Component {
    render() {
        return  <a href="#top" className="to-top ">
                    <i className="arrow"></i>
                </a>
    }
}

export default ToTop;
