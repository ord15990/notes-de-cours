class Utils {
    static formatUnsignedInt = (number, size) => ('0'.repeat(size - 1) + (number + 1)).slice(-size);
}

export default Utils
