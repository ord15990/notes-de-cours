-- 1.
DELIMITER //
CREATE TRIGGER prevenir_delete
BEFORE DELETE ON suppliers FOR EACH ROW
BEGIN
	IF OLD.id IN (1, 4, 5, 9) THEN
		SET @message_text = CONCAT('La rangée ayant l''id ', OLD.id, ' ne peut être supprimée.');
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = @message_text;
	END IF;
END//
DELIMITER ;

-- 2.
ALTER TABLE orders
ADD COLUMN subtotal DECIMAL(15, 2) DEFAULT 0;

-- 2.1.
DELIMITER //
CREATE TRIGGER subtotal_insert
AFTER INSERT ON order_details FOR EACH ROW
BEGIN
	UPDATE orders
	SET subtotal = (
		SELECT SUM(quantity * unit_price)
		FROM order_details
		WHERE order_id = NEW.order_id
		GROUP BY order_id
	)
	WHERE id = NEW.order_id;
END//
DELIMITER ;

-- 2.2.
DELIMITER //
CREATE TRIGGER subtotal_delete
AFTER DELETE ON order_details FOR EACH ROW
BEGIN
	UPDATE orders
	SET subtotal = (
		SELECT SUM(quantity * unit_price)
		FROM order_details
		WHERE order_id = OLD.order_id
		GROUP BY order_id
	)
	WHERE id = OLD.order_id;
END//
DELIMITER ;

-- 2.3.
DELIMITER //
CREATE TRIGGER subtotal_update
AFTER UPDATE ON order_details FOR EACH ROW
BEGIN
	UPDATE orders
	SET subtotal = (
		SELECT SUM(quantity * unit_price)
		FROM order_details
		WHERE order_id = NEW.order_id
		GROUP BY order_id
	)
	WHERE id = NEW.order_id;
END//
DELIMITER ;

-- 3.1
DELIMITER //
CREATE TRIGGER unit_price_insert
BEFORE INSERT ON order_details FOR EACH ROW
BEGIN
	SET NEW.unit_price = (
		SELECT list_price 
		FROM products
		WHERE id = NEW.product_id
	);
END//
DELIMITER ;

-- 3.2
DELIMITER //
CREATE TRIGGER unit_price_update
BEFORE UPDATE ON order_details FOR EACH ROW
BEGIN
	SET NEW.unit_price = (
		SELECT list_price 
		FROM products
		WHERE id = NEW.product_id
	);
END//
DELIMITER ;

-- 4.
CREATE TABLE log_order_details(
	id INT NOT NULL AUTO_INCREMENT,
	order_detail_id INT NOT NULL,
	operation CHAR(6) NOT NULL,
	date DATETIME NOT NULL,
	CONSTRAINT pk_log_order_details PRIMARY KEY(id)
);

-- 4.1
DELIMITER //
CREATE TRIGGER log_insert
AFTER INSERT ON order_details FOR EACH ROW
BEGIN
	INSERT INTO log_order_details (order_detail_id, operation, date)
	VALUES (NEW.id, 'INSERT', NOW());
END//
DELIMITER ;

-- 4.2.
DELIMITER //
CREATE TRIGGER log_delete
AFTER DELETE ON order_details FOR EACH ROW
BEGIN
	INSERT INTO log_order_details (order_detail_id, operation, date)
	VALUES (OLD.id, 'DELETE', NOW());
END//
DELIMITER ;

-- 4.3.
DELIMITER //
CREATE TRIGGER log_update
AFTER UPDATE ON order_details FOR EACH ROW
BEGIN
	INSERT INTO log_order_details (order_detail_id, operation, date)
	VALUES (NEW.id, 'UPDATE', NOW());
END//
DELIMITER ;

-- 5.
CREATE VIEW produits_non_vendu AS
SELECT p.*
FROM products p
LEFT JOIN order_details od ON p.id = od.product_id
WHERE od.id IS NULL;

-- 6.
CREATE VIEW paires_employee_client AS
SELECT CONCAT(e.first_name, ' ', e.last_name) AS 'Employee',
       CONCAT(c.first_name, ' ', c.last_name) AS 'Customer'
FROM orders o
LEFT JOIN employees e on o.employee_id = e.id
LEFT JOIN customers c on o.customer_id = c.id;

-- 7.
INSERT INTO produits_non_vendu (product_code, product_name)
VALUES("JWTEST-14", "Un produit");
-- Non, c'est pas possible

-- 8.
UPDATE produits_non_vendu
SET description = "Une description"
WHERE id = 14;
-- Oui, c'est possible

-- 9.
DELETE FROM produits_non_vendu 
WHERE id = 14;
-- Non, c'est pas possible

-- 10.
CREATE VIEW certaines_commande AS
SELECT *
FROM orders
WHERE shipping_fee = 0;

-- 11.
INSERT INTO certaines_commande (employee_id, customer_id, order_date)
VALUES (6, 6, '2006-06-06');
-- Oui, c'est possible

-- 12.
DELETE FROM certaines_commande
WHERE id = 82
-- Oui c'est possible
