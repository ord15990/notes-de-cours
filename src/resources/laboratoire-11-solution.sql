-- 1.
SELECT address, city, state_province, zip_postal_code, country_region
FROM customers
UNION
SELECT address, city, state_province, zip_postal_code, country_region
FROM employees;

-- 2.
UPDATE products
SET product_name = REPLACE(product_name, 'Northwind Traders', 'Awesome Dealer');

-- 3.
SELECT *
FROM employees
WHERE LENGTH(last_name) + LENGTH(first_name) < (
    SELECT AVG(LENGTH(last_name) + LENGTH(first_name))
    FROM employees
);

-- 4.
UPDATE employees
SET email_address = CONCAT(LOWER(first_name), '.', LOWER(last_name), '@awesomedealers.ca');

-- 5.
SELECT id, DATEDIFF(shipped_date, order_date) AS diff
FROM orders
WHERE shipped_date IS NOT NULL;

-- 6.
SELECT DISTINCT c.first_name, c.last_name, p.product_name
FROM customers c
INNER JOIN orders o ON c.id = o.customer_id
INNER JOIN order_details od ON o.id = od.order_id
INNER JOIN products p ON od.product_id = p.id
ORDER BY c.first_name, c.last_name, p.product_name;

-- 7.
SELECT c.first_name, c.last_name, p.product_name
FROM customers c, products p
WHERE p.id IN (
	SELECT product_id 
    FROM order_details
    WHERE order_id IN (
    	SELECT id
        FROM orders
        WHERE customer_id = c.id
    )
)
ORDER BY c.first_name, c.last_name, p.product_name;

-- 8.
-- Les deux requêtes sont aussi efficace l'une que l'autre.
-- Le nombre de "row" visité sont les même et les types de jointures
-- sont les mêmes.

-- 9.
CREATE INDEX idx_category ON
products(category);

-- 10.
SELECT p.product_code, p.product_name
FROM products p
LEFT JOIN order_details od ON p.id = od.product_id
GROUP BY p.id
ORDER BY SUM(od.unit_price * od.quantity) DESC
LIMIT 1