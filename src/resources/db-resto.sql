/*
Nom:         resto-complete.sql
Auteur:      Jonathan Wilkie
Description: Fichier de création et de données de la base de données 
             resto.
*/
DROP DATABASE IF EXISTS `resto`;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `resto` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `resto`;

DROP TABLE IF EXISTS `commandes`;
CREATE TABLE IF NOT EXISTS `commandes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_table` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_etats_commande` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_commandes_tables` (`code_table`),
  KEY `fk_commandes_etats_commande` (`id_etats_commande`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE TABLE `commandes`;
INSERT INTO `commandes` (`id`, `code_table`, `id_etats_commande`) VALUES
(1, 'A1', 1),
(2, 'A1', 1),
(3, 'B4', 2),
(4, 'B5', 2),
(5, 'A2', 2),
(6, 'D3', 3),
(7, 'D3', 4),
(8, 'D3', 4),
(9, 'A3', 2),
(10, 'A4', 2),
(11, 'A1', 4),
(12, 'D1', 4),
(13, 'D2', 4),
(14, 'D2', 4),
(15, 'A5', 2),
(16, 'B3', 3);

DROP TABLE IF EXISTS `details_commande`;
CREATE TABLE IF NOT EXISTS `details_commande` (
  `id_commandes` int(11) NOT NULL,
  `id_elements_menu` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`id_commandes`,`id_elements_menu`),
  KEY `fk_elements_menu_commandes` (`id_elements_menu`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE TABLE `details_commande`;
INSERT INTO `details_commande` (`id_commandes`, `id_elements_menu`, `quantite`) VALUES
(1, 2, 1),
(1, 16, 1),
(2, 4, 1),
(2, 18, 1),
(3, 10, 2),
(4, 5, 2),
(4, 8, 2),
(4, 20, 1),
(5, 19, 2),
(6, 4, 1),
(6, 15, 1),
(7, 12, 1),
(7, 18, 1),
(8, 1, 2),
(9, 3, 1),
(9, 7, 1),
(9, 9, 1),
(9, 17, 3),
(10, 11, 1),
(10, 19, 1),
(11, 18, 2),
(12, 17, 1),
(12, 20, 1),
(13, 16, 1),
(13, 18, 1),
(14, 17, 2),
(15, 10, 1),
(16, 4, 1),
(16, 9, 2),
(16, 16, 2);

DROP TABLE IF EXISTS `elements_menu`;
CREATE TABLE IF NOT EXISTS `elements_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sections_menu` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `fk_elements_menu_sections_menu` (`id_sections_menu`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE TABLE `elements_menu`;
INSERT INTO `elements_menu` (`id`, `id_sections_menu`, `nom`, `description`) VALUES
(1, 1, 'Saumon fumé et fromage', 'Saumon fumé et fromage'),
(2, 1, 'Crème de carottes', 'Crème de carottes'),
(3, 1, 'Soupe aux légumes', 'Soupe aux légumes'),
(4, 1, 'Soupe à l\'oignon', 'Soupe à l\'oignon'),
(5, 1, 'Salade au poulet', 'Salade au poulet'),
(6, 2, 'Pâtes Alfredo', 'Pâtes Alfredo'),
(7, 2, 'Pizza à la saucisse', 'Pizza à la saucisse'),
(8, 2, 'Sandwich au jambon', 'Sandwich au jambon'),
(9, 2, 'Saumon à la moutarde', 'Saumon à la moutarde'),
(10, 2, 'Pizza Hawaïenne', 'Pizza Hawaïenne'),
(11, 2, 'Porc aux pommes', 'Porc aux pommes'),
(12, 2, 'Poulet au beurre', 'Poulet au beurre'),
(13, 2, 'Saucisses sur riz', 'Saucisses sur riz'),
(14, 2, 'Steak et frites', 'Steak et frites'),
(15, 2, 'Hamburger', 'Hamburger'),
(16, 3, 'Gâteau au chocolat', 'Gâteau au chocolat'),
(17, 3, 'Gâteau aux carottes', 'Gâteau aux carottes'),
(18, 3, 'Croustade aux pommes', 'Croustade aux pommes'),
(19, 3, 'Brownies', 'Brownies'),
(20, 3, 'Pain aux bananes', 'Pain aux bananes');

DROP TABLE IF EXISTS `elements_menu_ingredient`;
CREATE TABLE IF NOT EXISTS `elements_menu_ingredient` (
  `id_elements_menu` int(11) NOT NULL,
  `id_ingredient` int(11) NOT NULL,
  PRIMARY KEY (`id_elements_menu`,`id_ingredient`),
  KEY `fk_incredient_element_menu_ingredient` (`id_ingredient`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE TABLE `elements_menu_ingredient`;
INSERT INTO `elements_menu_ingredient` (`id_elements_menu`, `id_ingredient`) VALUES
(1, 6),
(1, 15),
(2, 17),
(2, 27),
(3, 1),
(3, 23),
(3, 24),
(3, 25),
(3, 27),
(4, 16),
(4, 25),
(5, 1),
(5, 13),
(5, 21),
(6, 6),
(6, 7),
(6, 17),
(7, 5),
(7, 24),
(8, 1),
(8, 12),
(8, 21),
(9, 9),
(9, 14),
(9, 15),
(10, 1),
(10, 4),
(10, 25),
(11, 8),
(11, 12),
(11, 19),
(12, 1),
(12, 13),
(12, 16),
(13, 3),
(13, 5),
(14, 10),
(14, 11),
(15, 1),
(15, 6),
(15, 11),
(15, 21),
(15, 25),
(16, 16),
(16, 18),
(16, 20),
(16, 28),
(17, 18),
(17, 26),
(17, 28),
(18, 8),
(18, 19),
(18, 22),
(19, 16),
(19, 20),
(19, 28),
(20, 22),
(20, 27),
(20, 28);

DROP TABLE IF EXISTS `etats_commande`;
CREATE TABLE IF NOT EXISTS `etats_commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE TABLE `etats_commande`;
INSERT INTO `etats_commande` (`id`, `nom`) VALUES
(1, 'RECU'),
(2, 'CUISINE'),
(3, 'SERVICE'),
(4, 'COMPLETE');

DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE IF NOT EXISTS `ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE TABLE `ingredient`;
INSERT INTO `ingredient` (`id`, `nom`) VALUES
(1, 'Tomate'),
(2, 'Brocoli'),
(3, 'Riz'),
(4, 'Ananas'),
(5, 'Saucisse'),
(6, 'Fromage'),
(7, 'Pâtes'),
(8, 'Pomme'),
(9, 'Syrop d\'érable'),
(10, 'Patate'),
(11, 'Boeuf'),
(12, 'Porc'),
(13, 'Poulet'),
(14, 'Moutarde'),
(15, 'Saumon'),
(16, 'Beurre'),
(17, 'Crème'),
(18, 'Lait'),
(19, 'Cassonade'),
(20, 'Chocolat'),
(21, 'Laitue'),
(22, 'Avoine'),
(23, 'Orge'),
(24, 'Poivron'),
(25, 'Oignon'),
(26, 'Carotte'),
(27, 'Banane'),
(28, 'Oeuf');

DROP TABLE IF EXISTS `sections_menu`;
CREATE TABLE IF NOT EXISTS `sections_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE TABLE `sections_menu`;
INSERT INTO `sections_menu` (`id`, `nom`) VALUES
(1, 'Entrée'),
(2, 'Plat principal'),
(3, 'Dessert');

DROP TABLE IF EXISTS `tables`;
CREATE TABLE IF NOT EXISTS `tables` (
  `code` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE TABLE `tables`;
INSERT INTO `tables` (`code`) VALUES
('A1'),
('A2'),
('A3'),
('A4'),
('A5'),
('B1'),
('B2'),
('B3'),
('B4'),
('B5'),
('C1'),
('C2'),
('C3'),
('C4'),
('C5'),
('D1'),
('D2'),
('D3'),
('D4'),
('D5');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
