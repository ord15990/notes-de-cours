import pages from './data/pages.json';
import groups from './data/groups.json';
import sections from './data/sections.json';
import settings from './settings';

import utils from './Utils';

const data = {};

// Format groups data
data.groups = groups;
for (let groupId of Object.keys(data.groups)) {
    data.groups[groupId].id = groupId;
    data.groups[groupId].pages = [];
}

// Format pages data
data.pages = pages;
for (let pageId of Object.keys(data.pages)) {
    data.pages[pageId].id = pageId;
    data.groups[data.pages[pageId].group].pages.push(data.pages[pageId]);
    data.pages[pageId].group = data.groups[data.pages[pageId].group];
}

// Format sections data
data.sections = sections;
for (let i = 0; i < data.sections.length; i++) {
    // Add section name
    data.sections[i].name = settings.naming.sections;
    data.sections[i].number = utils.formatUnsignedInt(i, 2);
    data.sections[i].completeName = data.sections[i].name + ' ' + data.sections[i].number;

    // Add page references to the sections
    for (let j = 0; j < data.sections[i].pages.length; j++) {
        data.sections[i].pages[j] = data.pages[data.sections[i].pages[j]];
    }

    // Add page references by groups to the section
    let pagesByGroups = {};
    for (let page of data.sections[i].pages) {
        if (!pagesByGroups[page.group.id]) {
            pagesByGroups[page.group.id] = [];
        }

        pagesByGroups[page.group.id].push(page);
    }

    // Add groups data to the section
    data.sections[i].groups = {};
    for (let groupId of Object.keys(pagesByGroups)) {
        data.sections[i].groups[groupId] = {
            id: groupId,
            label: data.groups[groupId].label,
            title: data.groups[groupId].title,
            pages: pagesByGroups[groupId]
        };
    }
}

export default data;