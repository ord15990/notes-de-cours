import React from 'react'
import settings from './settings';

// Get themes properties from settings
const defaultTheme = settings.themes.default;
const themes = settings.themes.properties;
const themeColorMeta = document.querySelector('meta[name=theme-color]');

/**
 * Update all CSS variables in the template with the value of the specified theme.
 * @param { String } theme The theme which we want all CSS variables to switch to.
 */
const updadeCssVar = theme => {
    let rootVars = document.documentElement.style;
    rootVars.setProperty('--bg-color', themes[theme].bgColor);
    rootVars.setProperty('--bg-accent-color', themes[theme].bgAccentColor);
    rootVars.setProperty('--bg-accent-gradient-color', themes[theme].bgAccentGradientColor);
    rootVars.setProperty('--bg-highlight', themes[theme].bgHighlight);
    rootVars.setProperty('--bg-highlight-accent', themes[theme].bgHighlightAccent);
    rootVars.setProperty('--bg-interactive-color', themes[theme].bgInteractiveColor);
    rootVars.setProperty('--bg-interactive-accent-color', themes[theme].bgInteractiveAccentColor);
    rootVars.setProperty('--border-interactive-color', themes[theme].borderInteractiveColor);
    rootVars.setProperty('--border-separator-color', themes[theme].borderSeparatorColor);
    rootVars.setProperty('--text-color', themes[theme].textColor);
    rootVars.setProperty('--text-inverted-color', themes[theme].textInvertedColor);
    rootVars.setProperty('--text-aside-color', themes[theme].textAsideColor);

    // Modify theme color in HTML
    themeColorMeta.setAttribute('content', themes[theme].bgAccentColor);
}

/**
 * Context managing the theme data.
 */
const ThemeContext = React.createContext();

/**
 * Context provider component used to manage th theme data
 */
class ThemeContextProvider extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            theme: localStorage.getItem('theme') || defaultTheme 
        }

        updadeCssVar(this.state.theme);
    }

    getSyntaxHighlightStyle = () => {
        return themes[this.state.theme].syntaxHighlightStyle;
    }

    setTheme = theme => {
        // Validate that the theme exists and that it is changing
        if (!themes.hasOwnProperty(theme) || this.state.theme === theme) {
            return;
        }

        // Change theme CSS variables
        updadeCssVar(theme);

        // Change state and set new theme in local storage
        this.setState({ theme: theme });
        localStorage.setItem('theme', theme);
    }

    render() {
        const contextValue = { 
            theme: this.state.theme, 
            getSyntaxHighlightStyle: this.getSyntaxHighlightStyle, 
            setTheme: this.setTheme 
        }
        
        return <ThemeContext.Provider value={ contextValue }>
            { this.props.children }
        </ThemeContext.Provider>
    }
}

export { ThemeContext, ThemeContextProvider };

