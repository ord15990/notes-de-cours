// Syntax highlighter imports for languages
import sql from 'react-syntax-highlighter/dist/esm/languages/hljs/sql';
import txt from 'react-syntax-highlighter/dist/esm/languages/hljs/plaintext';
import js from 'react-syntax-highlighter/dist/esm/languages/hljs/javascript';
import csharp from 'react-syntax-highlighter/dist/esm/languages/hljs/csharp';

// Syntax highlighter imports for styles
import github from 'react-syntax-highlighter/dist/esm/styles/hljs/github';
import vs2015 from 'react-syntax-highlighter/dist/esm/styles/hljs/vs2015';

// Settings object
export default {
    naming: {
        sections: 'Module'
    },
    themes: {
        default: 'light',
        properties: {
            light: {
                bgColor: "#fff",
                bgAccentColor: "#1ea8a6",
                bgAccentGradientColor: "#36cfcc",
                bgInteractiveColor: "#63696b",
                bgInteractiveAccentColor: "#c9fffa",
                bgHighlight: "#d6d6d6",
                bgHighlightAccent: '#c9fffa',
                borderInteractiveColor: "#97a1a0",
                borderSeparatorColor: "#c3c3c3",
                textColor: "#222",
                textInvertedColor: "#fff",
                textAsideColor: '#222',
                syntaxHighlightStyle: "github"
            },
            dark: {
                bgColor: "#333635",
                bgAccentColor: "#00ad90",
                bgAccentGradientColor: "#15d1b2",
                bgHighlight: "#737d7c",
                bgHighlightAccent: '#00ad90',
                bgInteractiveColor: "#dcf5f0",
                bgInteractiveAccentColor: "#dcf5f0",
                borderInteractiveColor: "#00ad90",
                borderSeparatorColor: "#c3c3c3",
                textColor: "#fff",
                textInvertedColor: "#fff",
                textAsideColor: '#c1f7ec',
                syntaxHighlightStyle: "vs2015"
            }
        }
    },
    syntaxHiglight: {
        languages: {
            sql: { name: 'SQL', parser: sql },
            txt: { name: 'Texte', parser: txt },
            js: { name: 'Javascript', parser: js },
            csharp: { name: 'C#', parser: csharp },
        },
        themes: {
            github: github,
            vs2015: vs2015
        }
    }
}