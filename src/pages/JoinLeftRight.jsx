import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import leftJoin from '../resources/join-left.png';
import rightJoin from '../resources/join-right.png';

const exempleLeft = 
`SELECT membre.prenom, membre.nom, cours.nom
FROM membre 
LEFT JOIN cours ON membre.id_cours = cours.id_cours`;

const exempleRight = 
`SELECT membre.prenom, membre.nom, cours.nom
FROM membre 
RIGHT JOIN cours ON membre.id_cours = cours.id_cours`;

const exempleFakeRight = 
`SELECT membre.prenom, membre.nom, cours.nom
FROM cours 
LEFT JOIN membre ON membre.id_cours = cours.id_cours`;

export default class JoinLeftRight extends React.Component {
    render() {
        return <>
            <section>
                <h2>Description</h2>
                <p>
                    Le <IC>LEFT JOIN</IC> et le <IC>RIGHT JOIN</IC> permettent de sélectionner toutes les données de 
                    la table de gauche ou de droite, même si elles n’ont pas de données dans la table opposée. Ce sont 
                    des jointures directionnelles.
                </p>
                <p>
                    Nous pouvons représenter les données retournées par ces <IC>JOIN</IC> avec les diagramme suivant:
                </p>
                <img src={ leftJoin } alt="Left Join"/>
                <img src={ rightJoin } alt="Right Join"/>
                <ColoredBox heading="Faire la différence entre la gauche et la droite">
                    Dans un <IC>JOIN</IC>, vous pouvez identifier la table de gauche et de droite de la façon 
                    suivante:
                    <ul>
                        <li>
                            La table de gauche est celle spécifié entre le <IC>FROM</IC> et le <IC>JOIN</IC>. Bref, la 
                            table spécifié à la gauche du mot-clé <IC>JOIN</IC>.
                        </li>
                        <li>
                            La table de droite est celle spécifié entre le <IC>JOIN</IC> et le <IC>ON</IC>. Bref, la 
                            table spécifié à la droite du mot-clé <IC>JOIN</IC>.
                        </li>
                    </ul>
                </ColoredBox>
            </section>

            <section>
                <h2>Exemple de la jointure à gauche</h2>
                <p>
                    Voici un exemple de <IC>LEFT JOIN</IC> avec notre situation d'exemple des cours du gym:
                </p>
                <CodeBlock language="sql">{ exempleLeft }</CodeBlock>
                <p>
                    La requête ci-dessus produira le résultat suivant:
                </p>
                <table>
                    <tr><th>membre.prenom</th><th>membre.nom</th><th>cours.nom</th></tr>
                    <tr><td>Maxime</td><td>Tremblay</td><td>Yoga</td></tr>
                    <tr><td>Bob</td><td>Ross</td><td>Yoga</td></tr>
                    <tr><td>John</td><td>Cena</td><td>Kickboxing</td></tr>
                    <tr><td>Sasha</td><td>Ketchum</td><td><IC>NULL</IC></td></tr>
                </table>
                <p>
                    Vous noterez que le membre Sasha Ketchum est présent dans ces résulats. Ceci est dû au fait que 
                    toutes les données de la table <IC>membre</IC>, qui est la table de gauche, sont sélectionnées dans 
                    un <IC>LEFT JOIN</IC>, même si elles n'ont pas de données associées dans la table de droite.
                </p>
                <p>
                    Ce n'est toutefois pas le cas avec la table <IC>cours</IC>, qui est la table de droite. Vous 
                    verrez en effet que le cours de pilates n'est pas présent dans les résultats de cette recherche.
                </p>
            </section>

            <section>
                <h2>Exemple de la jointure à droite</h2>
                <p>
                    Voici un exemple de <IC>RIGHT JOIN</IC> avec notre situation d'exemple des cours du gym:
                </p>
                <CodeBlock language="sql">{ exempleRight }</CodeBlock>
                <p>
                    La requête ci-dessus produira le résultat suivant:
                </p>
                <table>
                    <tr><th>membre.prenom</th><th>membre.nom</th><th>cours.nom</th></tr>
                    <tr><td>Maxime</td><td>Tremblay</td><td>Yoga</td></tr>
                    <tr><td>Bob</td><td>Ross</td><td>Yoga</td></tr>
                    <tr><td>John</td><td>Cena</td><td>Kickboxing</td></tr>
                    <tr><td><IC>NULL</IC></td><td><IC>NULL</IC></td><td>Pilates</td></tr>
                </table>
                <p>
                    Vous noterez que le cours de pilates est présent dans ces résulats. Ceci est dû au fait que 
                    toutes les données de la table <IC>cours</IC>, qui est la table de droite, sont sélectionnées dans 
                    un <IC>RIGHT JOIN</IC>, même si elles n'ont pas de données associées dans la table de gauche.
                </p>
                <p>
                    Ce n'est toutefois pas le cas avec la table <IC>membre</IC>, qui est la table de gauche. Vous 
                    verrez en effet que le membre Sasha Ketchum n'est pas présent dans les résultats de cette recherche.
                </p>
            </section>

            <section>
                <h2>Inutilité du <IC>RIGHT JOIN</IC></h2>
                <p>
                    Le <IC>RIGHT JOIN</IC> est, en fait, vraiment inutile. En effet, puisqu'il est le contraire 
                    du <IC>LEFT JOIN</IC>, il nous suffit simplement d'inverser les tables dans un <IC>LEFT JOIN</IC> pour 
                    simuler un <IC>RIGHT JOIN</IC>.
                </p>
                <p>
                    Si nous voulons faire la requête d'exemple du <IC>RIGHT JOIN</IC> avec un <IC>LEFT JOIN</IC>, nous 
                    pouvons écrire la requête de la façon suivante:
                </p>
                <CodeBlock language="sql">{ exempleFakeRight }</CodeBlock>
                <p>
                    Le <IC>RIGHT JOIN</IC> est tellement inutile que certains systèmes de gestion de bases de données 
                    ne le supporte même pas. Ces systèmes supporteront uniquement le <IC>LEFT JOIN</IC>.
                </p>
                <ColoredBox heading="À noter">
                    Par soucis d'écrire du SQL toujours valide, je vous recommande fortement de ne jamais utiliser 
                    le <IC>RIGHT JOIN</IC>.
                </ColoredBox>
            </section>
        </>;
    }
}
