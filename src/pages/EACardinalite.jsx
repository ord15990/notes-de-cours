import React from 'react';
import Video from '../component/Video';

import card01 from '../resources/ea-card-01.png'
import card11 from '../resources/ea-card-11.png'
import card0n from '../resources/ea-card-0n.png'
import card1n from '../resources/ea-card-1n.png'
import step3 from '../resources/ea-step3.png'

class EACardinatlite extends React.Component {
    render() {
        return <>
            <section>
                <h2>Types de cardinalités</h2>
                <p>
                    La cardinalité, aussi appelé multiplicité indique le nombre minimal et maximal qu’un entité intervient
                    dans une association. Il y a existe quatre sorte de cardinalité possible. Ces cardinalités seront ajoutées
                    de chaque côté d’une association, de la même manière que les noms des associations. Voici le tableau
                    contenant toutes les cardinalités ainsi que leur notation dans le diagramme entité-association:
                </p>
                <table>
                    <tr>
                        <th>Cardinalité</th><th>Notation UML</th><th>Notation Crow’s foot</th>
                    </tr>
                    <tr>
                        <td>Zéro ou un</td><td>0..1</td><td><img src={ card01 } alt="Cardinalité zéro ou un" /></td>
                    </tr>
                    <tr>
                        <td>Un et un seul</td><td>1..1</td><td><img src={ card11 } alt="Cardinalité un et un seul" /></td>
                    </tr>
                    <tr>
                        <td>Zéro ou plusieurs</td><td>0..N</td><td><img src={ card0n } alt="Cardinalité zéro ou plusieurs" /></td>
                    </tr>
                    <tr>
                        <td>Un ou plusieurs</td><td>1..N</td><td><img src={ card1n } alt="Cardinalité un ou plusieurs" /></td>
                    </tr>
                </table>
            </section>

            <section>
                <h2>Définir les cardinalités</h2>
                <p>
                    Il est beaucoup plus facile de comprendre les cardinalités avec un exemple. Utilisons donc notre
                    situation. Pour déterminer la cardinalité, nous devons nous poser la question suivante pour chaque
                    association: Combien de fois les entités sont-elles impliquées? Puisque nous devons trouver les 
                    cardinalités des 2 sens de l'association, dans notre situation, nous pourrions dire:
                </p>
                <ul>
                    <li>«&nbsp;Un client passe combien de commande&nbsp;»</li>
                    <li>«&nbsp;Une commande est passée par combien de client&nbsp;»</li>
                </ul>
                <p>
                    Dans notre cas, les réponses sont:
                </p>
                <ul>
                    <li>
                        «&nbsp;Un client passe de zéro à plusieurs commande&nbsp;»
                        <p>
                            Puisque un client pourrait exister dans le systême même s'il n'a pas encore fait de 
                            commande et qu'un client peut effectivement passer plusieurs commandes.
                        </p>
                    </li>
                    <li>
                        «&nbsp;Une commande est passé par un et un seul client&nbsp;»
                        <p>
                            Puisque une commande ne peut exister dans le systême que si un client l'a crée et qu'il 
                            est impossible d'avoir une commande pour plusieurs client (chaque client a ses propres 
                            commandes).
                        </p>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Déssiner les cardinalités</h2>
                <p>
                    Dans ce cours, nous utiliserons la notation « Crow’s foot » puisque sa symbolique est facile à comprendre.
                    Dans cette notation, le rond veut dire «&nbsp;zéro&nbsp;», la barre veut dire «&nbsp;un&nbsp;» et la patte d’oiseau veut dire
                    «&nbsp;plusieurs&nbsp;».
                </p>
                <p>
                    En ajoutant la cardinalité à notre diagramme, nous avons maintenant le schéma suivant:
                </p>
                <img src={ step3 } alt="Déssiner les cardinalités" />
                <p>
                    Assurez-vous de mettre la cardinalité du bon coté de l’association. Elle est placé du même côté que le
                    nom de l’association qui l’a décrit. On peut maintenant lire l’association entre l’entité commande et
                    produit de la façon suivante:
                </p>
                <ul>
                    <li>«&nbsp;Une commande contient un ou plusieurs produits&nbsp;»</li>
                    <li>«&nbsp;Un produit est contenu dans zéro ou plusieurs commandes&nbsp;»</li>
                </ul>
            </section>

            <section>
                <Video title="Modèle entité-association - Cardinalités" src="https://www.youtube.com/embed/3uDmdZc8fmw" />
            </section>
        </>;
    }
}

export default EACardinatlite;