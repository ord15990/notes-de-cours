import React from 'react';
import Video from '../component/Video';
import ColoredBox from '../component/ColoredBox';

import exemple from '../resources/physique-exemple.png'

class PhysiqueIntroduction extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Vous savez maintenant comment faire un modèle entité-association. Nous pouvons maintenant passer
                    à la deuxième étape de création d’une base de données: la création du modèle physique.
                </p>
                <ol>
                    <li>Création du modèle entité-association</li>
                    <li><strong>Création du modèle physique</strong></li>
                    <li>Génération du code SQL DDL</li>
                </ol>
                <p>
                    Le modèle physique est essentiellement un plan de la base de données tel qu’elle sera dans le SGBD. 
                    Le modèle physique est donc notre plan final que nous utiliserons par la suite pour créer la base de données.
                </p>
                <p>
                    La création du modèle physique est généralement basé sur le modèle entité-association. La méthode
                    de création expliquée dans ce module utilise le diagramme entité-association. Toutefois, sachez que
                    les administrateurs et gestionnaires de base des données expérimentés vont parfois directement
                    commencer à cette étape lorsqu’il crée une base de données.
                </p>
                <ColoredBox heading="Attention">
                    Dans le cours, vous devrez toujours faire votre diagramme entité-association avant votre diagramme
                    physique, même si vous trouvez le processus long et facile.
                </ColoredBox>
            </section>

            <section>
                <h2>Création du modèle physique</h2>
                <p>
                    Comme mentionné ci-dessus, nous utiliserons le modèle entité association pour créer notre modèle
                    physique. Pour les étapes suivantes, nous utiliserons donc le modèle entité-association ci-dessous
                    à titre d'exemple:
                </p>
                <img src={ exemple } alt="Exemple de modèle entité-association de départ"></img>
                <p>
                    La création du modèle physique à partir du modèle entité-association se fait en 6 étapes:
                </p>
                <ol>
                    <li>Transformer les entités en table</li>
                    <li>Transformer les identifiants d’entité en clé primaire</li>
                    <li>Transformer les attributs d’entité en colonnes</li>
                    <li>Traiter les associations en fonction de leur type</li>
                    <li>Normaliser les tables</li>
                    <li>Ajouter les types de données pour tous les attributs</li>
                </ol>
            </section>

            <section>
                <Video title="Modèle physique - Introduction" src="https://www.youtube.com/embed/GLGHMnOJcI4" />
            </section>
        </>;
    }
}

export default PhysiqueIntroduction;