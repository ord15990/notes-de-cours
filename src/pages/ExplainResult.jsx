import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const example = 
`EXPLAIN
SELECT *
FROM products p
INNER JOIN order_details od ON p.id = od.product_id
INNER JOIN orders o ON od.order_id = o.id
INNER JOIN employees e ON o.employee_id = e.id
WHERE e.first_name = 'Steven';`;

export default class ExplainResult extends React.Component {
    render() {
        return <>
            <section>
                <h2>Exemple</h2>
                <p>
                    Pour bien démontrer le résultat de la commande <IC>EXPLAIN</IC>, nous utiliserons une requête simple en
                    exemple sur la base de données Nortdwind. La requête suivante va chercher tous les produits vendus par 
                    les employés ayant le prénom <IC>Steven</IC>:
                </p>
                <CodeBlock language="sql">{ example }</CodeBlock>
                <p>
                    Si nous exécutons cette requête, nous obtiendrons le résultat ci-dessous. 
                </p>
                <div className="overflow-protection">
                    <table>
                        <thead>
                            <tr>
                                <th>select_type</th><th>table</th><th>type</th><th>possible_keys</th><th>key</th><th>ref</th><th>rows</th><th>Extra</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>SIMPLE</td><td>e</td><td>ref</td><td>PRIMARY, first_name</td><td>first_name</td><td>const</td><td>1</td><td>Using index condition</td>
                            </tr>
                            <tr>
                                <td>SIMPLE</td><td>o</td><td>ref</td><td>PRIMARY, employee_id, employee_id_2, id, id_2, id_3</td><td>employee_id</td><td>e.id</td><td>3</td><td></td>
                            </tr>
                            <tr>
                                <td>SIMPLE</td><td>od</td><td>ref</td><td>product_id, product_id_2, fk_order_..._idx</td><td>fk_order_..._idx</td><td>o.id</td><td>1</td><td>Using where</td>
                            </tr>
                            <tr>
                                <td>SIMPLE</td><td>p</td><td>eq_ref</td><td>PRIMARY</td><td>PRIMARY</td><td>od.product_id</td><td>1</td><td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <ColoredBox heading="À noter">
                    Certaines colonnes ont été retirées et certaines valeurs ont été raccourci pour être bien affiché 
                    dans votre navigateur Web.
                </ColoredBox>
            </section>

            <section>
                <h2>Analyse des résultats</h2>
                <p>
                    Voici un bref aperçu des colonnes importantes dans les résultats d'une commande <IC>EXPLAIN</IC>:
                </p>
                <dl>
                    <dt>table</dt>
                    <dd>
                        L'ordre des rangées retournées est important. C'est l'ordre dans lequel MySQL va lire votre
                        requête. Dans l'exemple ci-dessus, MySQL traite dans l'ordre la 
                        table <IC>employees</IC>, <IC>orders</IC>, <IC>order_details</IC> puis 
                        finalement <IC>products</IC>. La colonne table nous donne le nom de la table qui est traité.
                    </dd>
                    <dt>type</dt>
                    <dd>
                        La colonne type décrit comment les tables sont jointes. Voici les valeurs typiques que peut
                        prendre le type en ordre du meilleur au pire en terme de performance:
                        <table>
                            <thead>
                                <tr>
                                    <th>Ordre de performance</th><th>Type</th><th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>system</td>
                                    <td>La table contient une seule rangée, il n'y a donc pas de choix à faire.</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>const</td>
                                    <td>
                                        On utilise une table en la comparant seulement avec des colonnes qui ont les 
                                        contraintes <IC>PRIMARY KEY</IC> ou <IC>UNIQUE</IC>.
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>eq_ref</td>
                                    <td>
                                        Une combinaison de rangées des tables précédentes retourne une seule rangée de 
                                        cette table. La comparaison des colonnes est faite à partir 
                                        d'une <IC>PRIMARY KEY</IC> ou d'une colonne <IC>UNIQUE</IC>.
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>ref</td>
                                    <td>
                                        Une combinaison de rangées des tables précédentes retourne une ou plusieurs 
                                        rangées d'un index de cette table. La comparaison des colonnes n'est pas faite 
                                        à partir d'une <IC>PRIMARY KEY</IC> ou d'une colonne <IC>UNIQUE</IC>.
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>range</td>
                                    <td>
                                        On utilise la table en la comparerant à plusieurs valeur. Vous verrez 
                                        généralement ce type de jointure lorsque vous utilisez 
                                        des <IC>IN</IC>, des <IC>BETWEEN</IC> ou des opérateurs de comparaison, 
                                        comme le <IC>&lt;</IC> ou le <IC>&gt;</IC>.
                                    </td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>index</td>
                                    <td>
                                        Un index est utilisé pour rechercher les rangées à joindre, mais il ne peut 
                                        pas être utilisé efficacement, donc on doit regarder chacune de ses valeurs.
                                    </td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>all</td>
                                    <td>
                                        On appelle aussi ce type de jointure un "Table scan". Les performance de cette 
                                        jointure sont les pires puisqu'on regarde chaque rangée une par une. Si vous 
                                        avez 10 milliards de rangées et que la donnée que vous cherchez se trouve à 
                                        la fin, vous allez parcourir 10 milliards de rangées.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dd>
                    <dt>possible_keys</dt>
                    <dd>
                        Les clés qui peuvent être potentiellement utilisé par MySQL. Par clés, on veut dire 
                        des <IC>PRIMARY KEY</IC>, des <IC>FOREIGN KEY</IC>, des contrainte <IC>UNIQUE</IC> ou des 
                        index. On n'utilisera pas vraiment cette colonne. On utilisera beaucoup plus la 
                        colonne <IC>key</IC>.
                    </dd>
                    <dt>key</dt>
                    <dd>
                        C'est la clé qui a été sélectionnée par MySQL pour exécuter sa requête. Si la clé est NULL,
                        aucune clé n'est utilisé. Les engin de base de données vont généralement essayer de choisir la 
                        clé qui permettra la recherche la plus efficace.
                    </dd>
                    <dt>ref</dt>
                    <dd>
                        Indique la colonne qui est comparé à la clé utilisé. Nous verrons parfois, la valeur <IC>const</IC>,
                        comme dans l'exemple ci-dessus. Cela veut dire que nous comparons avec une constante. Dans
                        l'exemple ci-dessus, la constante est <IC>Steven</IC>.
                    </dd>
                    <dt>rows</dt>
                    <dd>
                        Cette colonne est l'un de vos meilleurs indicateurs de performance. C'est le nombre estimé de
                        rangées que l'engin de MySQL devra regarder. Plus le nombre est petit, plus la requête est
                        performante. Il faut toutefois faire attention puisque ce nombre est "par table" ou "par
                        jointure". Pour avoir le nombre approximatif pour la requête au complet, nous devons
                        multiplier ces chiffres. Dans l'exemple ci-dessus, nous avons 
                        donc <IC>1&nbsp;&times;&nbsp;3&nbsp;&times;&nbsp;1&nbsp;&times;&nbsp;1&nbsp;=&nbsp;3</IC>. Donc environ 3 
                        rangées au total à parcourir, ce qui est très bon.
                    </dd>
                </dl>
            </section>
        </>;
    }
}
