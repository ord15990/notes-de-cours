import React from 'react';

import fn3 from '../resources/normalization-3fn.png';
import fn3Age from '../resources/normalization-3fn-age.png';
import step6 from '../resources/physique-step6.png';

class PhysiqueNormalisation extends React.Component {
    render() {
        return <>
            <section>
                <h2>3<sup>ème</sup> forme normale (3FN)</h2>
                <p>
                    Cette forme normale indique qu'un groupe de colonne dans une table devrait toujours dépendre de la clé primaire.
                    Bref, si vous semblez avoir 2 entités/tables dans la même entité/table, il faudrait les séparer.
                </p>
                <img src={ fn3 } alt="Utilisation de la troisième forme normale" />
                <p>
                    Parfois, certaines colonnes vont dépendre d'une autre colonne qui n'est pas présente dans la table. Un bon exemple
                    est l'âge d'une personne dans une table. L'âge dépends en fait de la date de naissance, mais celle-ci 
                    n'est pas nécessairement présente dans la table. Dans ce genre de cas, je vous suggère de garder uniquement la 
                    date de naissance puisqu'elle constante dans le temps et causera moins de changements dans la base de données.
                </p>
                <img src={ fn3Age } alt="Utilisation de la troisième forme normale" />
                <p>
                    Vous aurez rarement à appliquer cette forme normale si vous avez bien fait votre modèle entité-association.
                </p>
            </section>

            <section>
                <h2>Normaliser la situation d'exemple</h2>
                <p>
                    Dans notre situation d’exemple, notre structure de données respecte déjà la 3<sup>ème</sup> forme normale.
                    Nous n'avons donc pas besoin de modifier le modèle.
                </p>
                <img src={ step6 } alt="Normalisation d'un modèle" />
            </section>
        </>;
    }
}

export default PhysiqueNormalisation;