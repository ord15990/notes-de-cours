import React from 'react';
import ColoredBox from '../component/ColoredBox';
import phpMyAdmin from '../resources/php-my-admin.png';
import workbenchMenu from '../resources/workbench-menu.png';
import workbenchConnect from '../resources/workbench-connect.png';
import Video from '../component/Video';

class Outils extends React.Component {
    render() {
        return <>
            <section>
                <h2>Les logiciels</h2>
                <p>
                    Nous utiliserons plusieurs outils et logiciels dans ce cours. Voici un bref aperçu des logiciels dont vous
                    aurez besoin :
                </p>
                <ul>
                    <li>
                        XAMPP (Alternative: WAMP, MAMP ou LAMP)
                        <ul>
                            <li>MySQL / MariaDB</li>
                            <li>phpMyAdmin (Apache + PHP)</li>
                        </ul>
                    </li>
                    <li>Visual Studio Code</li>
                    <li>MySQL Workbench</li>
                </ul>
                <p>
                    Dans ce document, nous verrons comment installer, configurer et accéder à ces divers logiciels et outils
                    pour le cours.
                </p>
            </section>

            <section>
                <h2>XAMPP</h2>
                <p>
                    XAMPP est un logiciel permettant de simplifier l’installation d’un serveur Web avec une base de
                    données. Dans notre cas, nous utiliserons XAMPP seulement puisqu’il facilite l’installation de 
                    MySQL&nbsp;/&nbsp;MariaDB avec l’outil PHPMyAdmin. Pour télécharger XAMPP, aller le chercher sur son site Web. 
                </p>
                <p>
                    <a href="https://www.apachefriends.org/" target="_blank" rel="noopener noreferrer">XAMPP</a>
                </p>
                <p>
                    Exécutez le fichier téléchargé et suivez les instructions à l’écran. Durant l'installation, portez attention 
                    aux points suivants:
                </p>
                <ul>
                    <li>
                        Lorsqu’on vous demande de sélectionner les composants, sélectionnez les tous si vous voulez
                        les utiliser plus tard. Sinon, pour le cours, vous aurez simplement besoin de Apache, MySQL,
                        PHP et phpMyAdmin.
                    </li>
                    <li>
                        Lorsqu’on vous demande de sélectionner le dossier d’installation, gardez celui par défaut dans
                        le « C:\xampp ».
                    </li>
                    <li>
                        Lorsqu’on vous demande si vous voulez plus d’info sur Bitnami, vous pouvez simplement
                        ignorer. Nous n’utiliserons pas Bitnami pour le cours.
                    </li>
                </ul>
                <p>
                    Si XAMPP semble causer des problèmes, vous pouvez aussi utiliser les logiciels ci-dessous comme alternative.
                </p>
                <ul>
                    <li>Windows: <a href="https://www.wampserver.com/" target="_blank" rel="noopener noreferrer">WAMP</a></li>
                    <li>MacOS: <a href="https://www.mamp.info/" target="_blank" rel="noopener noreferrer">MAMP</a></li>
                    <li>Linux: <a href="https://ubuntu.com/server/docs/lamp-applications" target="_blank" rel="noopener noreferrer">LAMP</a></li>
                </ul>
            </section>

            <section>
                <h2>Panneau de configuation de XAMPP</h2>
                <p>
                    Pour utiliser XAMPP et ses composants, nous utiliserons son «&nbsp;Control Panel&nbsp;». Après l’installation,
                    vous trouverez ce logiciel dans votre menu rapide ou dans le dossier «&nbsp;C:\xampp&nbsp;». Une fois le «&nbsp;Control
                    Panel&nbsp;» ouvert, vous retrouverez l’icone de XAMPP dans la barre de tâche de votre système d'exploitation.
                </p>
                <p>
                    Dans le «&nbsp;&nbsp;Control Panel&nbsp;», vous trouverez les différents services offert par XAMPP. Dans notre cas, pour
                    le cours, nous avons uniquement besoin de Apache et MySQL. Vous n’avez qu’a cliquer sur le bouton
                    «&nbsp;Start&nbsp;» à côté de chacun des services pour les démarrer. Si tout fonctionne bien, le nom des deux
                    services devrait avoir la couleur verte pour indiquer qu’ils sont bien démarrés.
                </p>
                <p>
                    Il est possible que le parefeu de votre système d'exploitation demande des accès lors du démarrage des services de Apache 
                    et MySQL. Assurez-vous d'accepter ces demandes d'accès puisque sinon, les 2 services ne fonctionneront pas nécessairement 
                    correctement.
                </p>
                <ColoredBox heading="Attention: ">
                    Même si vous fermer la fenêtre du «&nbsp;Control Panel&nbsp;», le logiciel reste ouvert. N’essayez donc pas de
                    l’ouvrir plusieurs fois. Cela pourrait vous causer quelques problèmes plus tard. Pour rouvrir le
                    «&nbsp;Control Panel&nbsp;» vous n’avez qu’a faire un clique droit sur l’icone de XAMPP dans la barre de tâche et
                    cliquer sur «&nbsp;Show&nbsp;/&nbsp;Hide&nbsp;». Pour complètement fermer le logiciel, vous pouvez faire un clique droit
                    sur l’icone de XAMPP dans la barre de tâche et cliquer sur «&nbsp;Quit&nbsp;».
                </ColoredBox>
            </section>

            <section>
                <h2>MySQL&nbsp;/&nbsp;MariaDB</h2>
                <p>
                    MySQL ou MariaDB sera notre système de gestion de base de données (SGBD). Ne paniquez pas si au démarrage de
                    celui-ci vous ne voyez aucune fenêtre apparaître. MySQL et MariaDB sont des services. Il s’exécute donc dans
                    l’arrière-plan de votre ordinateur. Le SGBD ne nous offre pas d’interface pour l’utiliser. Nous pouvons
                    donc rien faire sans un logiciel qui se connectera au SGBD. Dans le passé, ces logiciels étaient des
                    programmes consoles. Heureusement, aujourd’hui nous avons de belles interfaces graphiques pour
                    nous aider. Dans ce cours, nous utiliserons principalement l’interface phpMyAdmin ou MySQL Workbench. 
                    Toutefois, si vous trouvez une autre interface graphique que vous préférez sur le Web, n'hésitez pas à 
                    l'utiliser.
                </p>
                <p>
                    Nous apprendrons à manipuler ce SGBD durant le cours.
                </p>
            </section>

            <section>
                <h2>PhpMyAdmin</h2>
                <p>
                    La seule raison pour laquelle nous avons démarré le service Apache dans XAMPP est pour l’interface
                    phpMyAdmin. Ce petit logiciel est une plate-forme Web nous permettant d’intéragir avec une base de
                    données. Puisque c’est un logiciel Web, nous avons donc besoin d’un serveur Web et le serveur Web,
                    dans notre cas, c’est Apache. Pour utiliser phpMyAdmin, ouvrez un navigateur Web et rendez-vous à
                    l’adresse suivante :
                </p>
                <p>
                    <a href="http://localhost/phpmyadmin/" target="_blank" rel="noopener noreferrer">http://localhost/phpmyadmin/</a>
                </p>
                <p>
                    Nous apprendrons à utiliser phpMyAdmin durant la session. Toutefois, il est quand même important
                    de connaître un peu son interface graphique.
                </p>
                <img src={ phpMyAdmin } alt="phpMyAdmin" />
                <dl>
                    <dt>1. Liste des bases de données</dt>
                    <dd>
                        Nous trouvons dans cette liste toutes les
                        bases de données présentes sur notre serveur MySQL. En cliquant sur les éléments de cette liste, vous
                        serez redirigé vers une page permettant d’y faire des opération. Nous verrons ces interfaces plus en
                        détail durant le cours quand nous ferons du SQL.
                    </dd>
                    <dt>2. Bouton de rechargement</dt>
                    <dd>
                        L’icône la plus importante sous le logo du logiciel est probablement la petite flèche verte. En cliquant dessus, cela vous
                        permettra de rafraîchir la liste des bases de données ainsi que de leurs tables. Si vous créez des éléments et
                        que ceux-ci ne s’affichent pas, cliquez simplement sur la flèche verte pour recharger l'interface graphique.
                    </dd>
                    <dt>3. Barre d'emplacement</dt>
                    <dd>
                        Cette barre vous indique à quel endroit vous vous trouvez dans l'interface graphique. Dans l'image, nous somme directement sur
                        le serveur, mais si vous cliquez sur une base de données dans le menu de gauche, vous verez votre emplacement changer. Il arrive
                        fréquemment de se tromper d'interfaces lorsques nous avous plusieurs bases de données avec plusieurs tables, je vous suggère 
                        donc fortement de vous assurez que vous êtes au bon emplacement en regardant cette barre.
                    </dd>
                    <dt>4. Barre d'outils</dt>
                    <dd>
                        Cette barre nous permet de choisir quelles opérations nous voulons faire sur les tables ou les bases de données. Les opérations 
                        affichées seront différentes dépendant d'où vous vous trouver dans l'interface graphique.
                    </dd>
                </dl>
            </section>

            <section>
                <h2>MySQL Workbench</h2>
                <p>
                    MySQL Workbench est un environnement de développement spécialisé pour les bases de données et le code SQL. C'est une belle 
                    alternative à l'utilisation de phpMyAdmin. Je vous recommande d'essayer les 2 et d'utiliser par la suite celui que vous préférez. 
                    Vous pouvez télécharger ce logiciel au lien suivant :
                </p>
                <p>
                    <a href="https://www.mysql.com/products/workbench/" target="_blank" rel="noopener noreferrer">MySQL Workbench</a>
                </p>
                <p>
                    L'installation de MySQL Workbench peut nécessiter l'installation d'autres librairies de code pour pouvoir s'installer. Par 
                    exemple, sur Windows, il est nécessaire d'avoir la plateforme .NET la plus récente pour l'installation. Celle-ci est disponible 
                    en téléchargeant l'éditeur Visual Studio Community.
                </p>
                <p>
                    Nous apprendrons à utiliser MySQL Workbench durant la session. Voici toutefois quelques informations sur son interface graphique:
                </p>
                <img src={ workbenchMenu } alt="MySQL Workbench menu" />
                <img src={ workbenchConnect } alt="MySQL Workbench connect" />
                <p>
                    Pour commencer à utiliser MySQL Workbench, assurez-vous tout d’abord que le service MySQL est
                    bien démarré dans XAMPP. Vous devrez par la suite vous connecter à la base de données. Pour ce
                    faire, allez dans le menu dans «&nbsp;Database&nbsp;», puis dans «&nbsp;Manage Connections...&nbsp;»
                </p>
                <p>
                    Une fenêtre de connection apparaîtra. Dans cette fenêtre, vous pourrez enregistrer une nouvelle
                    connection. Puisque notre base de données est en local, vous n’avez qu’à garder les informations déjà
                    présente dans le formulaire et à cliquer sur «&nbsp;New&nbsp;» dans le bas à gauche pour créer la connection.
                </p>
                <p>
                    Vous pouvez maintenant fermer cette fenêtre sans problème. Vous trouverez maintenant sur la page
                    d’accueil de MySQL Workbench votre nouvelle connection. Pour vous connecter, cliquez simplement
                    sur celle-ci.
                </p>
            </section>

            <section>
                <h2>Visual Studio Code</h2>
                <p>
                    Nous utiliserons Visual Studio Code comme éditeur de texte lorsque nous écrirons du code SQL plus tard dans le cours. Je vous 
                    recommande fortement cet éditeur, mais n'hésitez pas à en utiliser un autre si vous préférez. Vous pouvez télécharger ici:
                </p>
                <p>
                    <a href="https://code.visualstudio.com/" target="_blank" rel="noopener noreferrer">Visual Studio Code</a>
                </p>
                <p>
                    Voici quelques autres éditeurs de texte que vous pourriez aimer:
                </p>
                <ul>
                    <li>
                        <a href="https://notepad-plus-plus.org/" target="_blank" rel="noopener noreferrer">Notepad++</a>
                    </li>
                    <li>
                        <a href="https://www.sublimetext.com/" target="_blank" rel="noopener noreferrer">Sublime Text</a>
                    </li>
                    <li>
                        <a href="https://atom.io/" target="_blank" rel="noopener noreferrer">Atom</a>
                    </li>
                </ul>
            </section>

            <section>
                <Video title="Vidéo sur la classification des bases de données" src="https://www.youtube.com/embed/atmYZNFYdU0" />
            </section>
        </>;
    }
}

export default Outils;
