import React from 'react';
import IC from '../component/InlineCode';

import fn1 from '../resources/normalization-1fn.png'
import step6 from '../resources/physique-step6.png'

class PhysiqueNormalisation extends React.Component {
    render() {
        return <>
            <section>
                <h2>1<sup>ère</sup> forme normale (1FN)</h2>
                <p>
                    C'est la forme normale la plus importante. Elle indique que toutes les colonnes des tables devraient 
                    être atomique. Une colonne atomique a les caractéristiques suivantes:
                </p>
                <ul>
                    <li>La colonne ne peut pas se subdiviser</li>
                    <li>La colonne ne se répète pas plusieurs fois dans la table</li>
                </ul>
                <p>
                    Voici quelques cas typique d'application de la 1<sup>ère</sup> forme normale:
                </p>
                <ul>
                    <li>
                        Si vous avez une colonne <IC>nom</IC> ou <IC>nom_complet</IC>, assurez-vous de les séparer en 
                        colonne <IC>nom</IC> et <IC>prénom</IC>.
                    </li>
                    <li>
                        Si vous avez une colonne <IC>adresse</IC>, il faut généralement la subdiviser en plusieurs colonne 
                        qui contiendront chaque partie de l'adresse, comme le numéro de porte, la rue, la ville, la province ou 
                        état ainsi que le pays.
                    </li>
                    <li>
                        Si vous avez une colonne qui contient plusieurs valeurs ou plusieurs colonnes qui représente la même chose, 
                        séparez les dans leur propre table
                    </li>
                    {/*<li>
                        Si vous avez une colonne <IC>age</IC>, il n’est clairement pas constant dans le temps. Effectivement, 
                        une colonne de la sorte nécessiterait un changement à chaque année. Pour régler ce problème, nous avons 
                        simplement à utiliser la date de naissance comme colonne.
                    </li>*/}
                </ul>
                <img src={ fn1 } alt="Utilisation de la première forme normale" />
            </section>

            <section>
                <h2>Normaliser la situation d'exemple</h2>
                <p>
                    Dans notre situation d’exemple, notre structure de données ne respecte pas la 1<sup>ère</sup> forme normale.
                    Toutefois, les seules petites corrections à faire se retrouvent dans la table client où nous avons la 
                    colonne <IC>nom</IC> et <IC>adresse</IC> qui ne respectent pas la 1<sup>ère</sup> forme normale. Pour les 
                    corriger, nous avons simplement à séparer le nom du client ainsi que son adresse dans plusieurs attributs 
                    distincts.
                </p>
                <img src={ step6 } alt="Normalisation d'un modèle" />
            </section>
        </>;
    }
}

export default PhysiqueNormalisation;