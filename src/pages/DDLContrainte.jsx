import React from 'react';
import Video from '../component/Video';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const index = 
`-- Créer un index
CREATE INDEX idx_nom ON nom_table (colonne1);

-- Créer un index unique sur multiple colonne
CREATE UNIQUE INDEX idx_nom
ON nom_table (colonne1, colonne2, /* ... */);

-- Supprimer un index
ALTER TABLE nom_table DROP INDEX idx_nom;`;

const constraintInline = 
`CREATE TABLE nom_table (
    colonne1 TYPE_DONNEE CONTRAINTE,
    colonne2 TYPE_DONNEE CONTRAINTE,
    -- ...
);`;

const notNullDefault = 
`CREATE TABLE compte (
    -- Ajouter la contrainte NOT NULL
    prenom VARCHAR(50) NOT NULL,

    -- Ajouter la contrainte DEFAULT
    point INT DEFAULT 500,

    -- Ajouter les 2 contraintes sur la même colonne
    encodage VARCHAR(20) NOT NULL DEFAULT 'utf-8'
);`;

const constraintEnd = 
`CREATE TABLE nom_table (
    colonne1 type_donnee,
    colonne2 type_donnee,
    -- ...
    CONSTRAINT nom_contrainte1 CONTRAINTE(),
    CONSTRAINT nom_contrainte2 CONTRAINTE()
);`;

const constraintUniqueCheck = 
`CREATE TABLE compte (
    courriel VARCHAR(320),
    karma INT,

    -- Ajouter la contrainte UNIQUE
    CONSTRAINT uq_courriel UNIQUE(courriel),

    -- Ajouter la contrainte CHECK
    CONSTRAINT ck_karma CHECK(
        karma > -1000 AND karma < 1000
    )
);`;

const exemple = 
`-- Créer la base de données
DROP DATABASE IF EXISTS boutique_jouet;
CREATE DATABASE boutique_jouet 
CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

-- Utiliser la base de données pour le reste des opérations
USE boutique_jouet;

-- Créer la table client
DROP TABLE IF EXISTS client;
CREATE TABLE client(
    id_client INT,
    prenom VARCHAR(50) NOT NULL,
    nom VARCHAR(50) NOT NULL,
    adresse VARCHAR(100) NOT NULL,
    ville VARCHAR(50) NOT NULL,
    province VARCHAR(50) NOT NULL,
    pays VARCHAR(50) NOT NULL
);

-- Créer la table commande
DROP TABLE IF EXISTS commande;
CREATE TABLE commande(
    id_commande INT,
    id_client INT NOT NULL,
    prix_total FLOAT,
    date DATE,
    CONSTRAINT prix_total CHECK(
        prix_total >= 0
    )
);

-- Créer la table categorie
DROP TABLE IF EXISTS categorie;
CREATE TABLE categorie(
    id_categorie INT,
    nom_categorie VARCHAR(50) NOT NULL
);

-- Créer la table produit
DROP TABLE IF EXISTS produit;
CREATE TABLE produit(
    id_produit INT,
    id_categorie INT ,
    nom VARCHAR(100) NOT NULL,
    prix FLOAT,
    CONSTRAINT ck_prix CHECK(
        prix >= 0
    )
);

-- Créer la table commande_produit
DROP TABLE IF EXISTS commande_produit;
CREATE TABLE commande_produit(
    id_commande INT,
    id_produit INT,
    quantite INT NOT NULL DEFAULT 1,
    prix_total_produit FLOAT,
    CONSTRAINT ck_prix_total_produit CHECK(
        prix_total_produit >= 0
    )
);

-- Ajouter les index
CREATE INDEX idx_nom_produit ON produit(nom);`;

class DDLTable extends React.Component {
    render() {
        return <>
            <section>
                <h2>Types de contraintes</h2>
                <p>
                    Les contraintes sont un moyen de mettre des conditions de validation ou des comportements 
                    différents sur les colonnes de vos tables. Voici une liste des contraintes généralement utilisées
                    dans les bases de données relationnelles:
                </p>
                <dl>
                    <dt><IC>NOT NULL</IC></dt>
                    <dd>Indique que les données dans une colonne ne peuvent être NULL.</dd>
                    <dt><IC>DEFAULT</IC></dt>
                    <dd>Indique une valeur par défaut dans une colonne quand aucune valeur n’est spécifiée.</dd>
                    <dt><IC>UNIQUE</IC></dt>
                    <dd>Indique que toutes les données dans une colonne doivent être unique.</dd>
                    <dt><IC>CHECK</IC></dt>
                    <dd>
                        Indique une condition que doivent suivre les données dans une colonne. On l'utilise souvent 
                        pour valider des nombres.
                    </dd>
                    <dt><IC>INDEX</IC></dt>
                    <dd>
                        Indique que la colonne doit être indexée pour accélérer les recherches. Vous aurez plus de 
                        détails dans un autre module.
                    </dd>
                    <dt><IC>PRIMARY KEY</IC></dt>
                    <dd>Indique la clé primaire de la table (NOT NULL et UNIQUE par défaut).</dd>
                    <dt><IC>FOREIGN KEY</IC></dt>
                    <dd>Indique une clé étrangère de la table.</dd>
                </dl>
                <p>
                    Nous ne couvrirons pas la <IC>PRIMARY KEY</IC> et la <IC>FOREIGN KEY</IC> dans cette page 
                    puisqu'elles nécessitent un peu plus de détails, mais nous couvrirons les autres ici.
                </p>
            </section>

            <section>
                <h2>NOT NULL et DEFAULT</h2>
                <p>
                    Les contrainte <IC>NOT NULL</IC> et <IC>DEFAULT</IC> s'utilise de façon similaire. Nous les 
                    insèrerons directement dans les tables à la droite de chaque colonne concernée:
                </p>
                <CodeBlock language="sql">{ constraintInline }</CodeBlock>
                <p>
                    Comme mentionné ci-dessus, la contrainte <IC>NOT NULL</IC> permet d'indiquer qu'une colonne ne 
                    peut pas contenir la valeur <IC>NULL</IC>. La contrainte <IC>DEFAULT</IC> quant à elle, permet de 
                    définir une valeur par défaut à une colonne lors de l'insertion de données si aucune valeur n'est 
                    spécifiée.
                </p>
                <p>
                    Vous pouvez spécifier ces contraintes de la façon suivante:
                </p>
                <CodeBlock language="sql">{ notNullDefault }</CodeBlock>
            </section>

            <section>
                <h2>UNIQUE et CHECK</h2>
                <p>
                    Les contrainte <IC>UNIQUE</IC> et <IC>CHECK</IC> s'utilise de façon similaire aussi. Nous les 
                    insèrerons dans les tables, mais à la fin de la définition des colonnes:
                </p>
                <CodeBlock language="sql">{ constraintEnd }</CodeBlock>
                <p>
                    Comme mentionné ci-dessus, la contrainte <IC>UNIQUE</IC> permet d'indiquer qu'une colonne ne 
                    ne peut pas contenir de doublons. Toutes les entrées seront donc unique. La 
                    contrainte <IC>CHECK</IC> quant à elle, permet de spécifier une condition à valider sur la colonne 
                    directement. Si la condition échoue, la données ne pourra pas être ajoutée ou modifiée.
                </p>
                <CodeBlock language="sql">{ constraintUniqueCheck }</CodeBlock>
                <p>
                    Par convention, je vous recommande de toujours débuté le nom d'une contrainte <IC>UNIQUE</IC> par 
                    le préfix <IC>uq_</IC> et le nom d'une contrainte <IC>CHECK</IC> par le préfix <IC>ck_</IC>.
                </p>
            </section>

            <section>
                <h2>Manipulation d'index</h2>
                <p>
                    Les index vont s'ajouter de façon différente aux tables. Ceci est dû au fait que les index sont 
                    une contrainte très spéciale qui peut contenir beaucoup de paramètre de configuration. Toutefois,
                    à la base, on peut manipuler un index de la façon suivante:
                </p>
                <CodeBlock language="sql">{ index }</CodeBlock>
                <p>
                    Par convention, je vous recommande de toujours débuté le nom d'un index par le préfix <IC>idx_</IC>.
                </p>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Dans note situation d'exemple, il pourrait être intéressant d'avoir les contraintes suivantes:
                </p>
                <ul>
                    <li><IC>NOT NULL</IC> pour toutes les colonnes du client</li>
                    <li><IC>NOT NULL</IC> pour le <IC>id_client</IC> des commandes</li>
                    <li><IC>NOT NULL</IC> pour le nom des catégories et des produits</li>
                    <li><IC>NOT NULL</IC> et un <IC>DEFAULT</IC> de 1 pour la quantité dans <IC>commande_produit</IC></li>
                    <li><IC>CHECK</IC> sur tous les prix pour s'assurer qu'il sont supérieur ou égal à 0</li>
                    <li>Un index sur le nom des produits pour accélérer la recherche</li>
                </ul>
                <p>
                    Voici le code SQL pour générer notre base de données avec ces nouvelles contraintes ajoutées:
                </p>
                <CodeBlock language="sql">{ exemple }</CodeBlock>
            </section>

            <section>
                <Video title="SQL - DDL - Contraintes" src="https://www.youtube.com/embed/Y8PZyMKtsfs" />
            </section>
        </>;
    }
}

export default DDLTable;