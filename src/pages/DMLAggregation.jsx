import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock'

const max = 
`-- Retourne le plus haut pointage
SELECT MAX(pointage) AS 'max_pointage'
FROM partie`;

const min = 
`-- Retourne le plus bas pointage
SELECT MIN(pointage) AS 'min_pointage'
FROM partie`;

const count = 
`-- Retourne le nombre de pointages plus grand que 9000
SELECT COUNT(*) AS 'nombre'
FROM partie
WHERE pointage > 9000`;

const avg = 
`-- Retourne la moyenne du pointage
SELECT AVG(pointage) AS 'moyenne'
FROM partie`;

const sum = 
`-- Retourne la somme des profits pour toutes les années
SELECT SUM(profits) AS 'profit_total'
FROM finance_annuelle`;

const limitation = 
`SELECT nom, COUNT(*)
FROM produit;`;

const limitationOk = 
`SELECT COUNT(nom), MAX(prix)
FROM produit;`;

class DMLAggregatiion extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Une fonction d'agrégation permet de s'exécuter sur la valeur d'une colonne pour toutes les 
                    données dans une table, mais retourne uniquement une seule valeur au lieu de retourner une valeur 
                    pour chaque donnée. Le concept général est un peu difficile à comprendre, mais les fonctions 
                    d'agrégation de base sont assez explicite. Voici les 5 fonctions d'agrégation que nous 
                    utiliserons généralement en SQL.
                </p>
                <ul>
                    <li><IC>MAX()</IC></li>
                    <li><IC>MIN()</IC></li>
                    <li><IC>COUNT()</IC></li>
                    <li><IC>AVG()</IC></li>
                    <li><IC>SUM()</IC></li>
                </ul>
                <p>
                    Il est recommandé d'utiliser ces fonctions avec des alias pour que le nom de la colonne retournée 
                    soit plus facilement lisible.
                </p>
            </section>

            <section>
                <h2>Max</h2>
                <p>
                    Cette fonction nous permet de retourner le maximum d'une colonne parmi toutes les données.
                </p>
                <CodeBlock language="sql">{ max }</CodeBlock>
            </section>

            <section>
                <h2>Min</h2>
                <p>
                    Cette fonction nous permet de retourner le minimum d'une colonne parmi toutes les données.
                </p>
                <CodeBlock language="sql">{ min }</CodeBlock>
            </section>

            <section>
                <h2>Count</h2>
                <p>
                    Permet de compter le nombre de valeur parmi toutes le données.
                </p>
                <CodeBlock language="sql">{ count }</CodeBlock>
                <p>
                    Par défaut, si vous faite un compte sur une colonne au lieu de <IC>*</IC>, la fonction <IC>COUNT</IC> ne 
                    comptera pas les données qui ont une valeur <IC>NULL</IC> dans cette colonne.
                </p>
            </section>

            <section>
                <h2>Avg</h2>
                <p>
                    Cette fonction nous permet de retourner la moyenne des données dans une colonne.
                </p>
                <CodeBlock language="sql">{ avg }</CodeBlock>
            </section>

            <section>
                <h2>Sum</h2>
                <p>
                    Cette fonction nous permet de retourner la somme des données dans une colonne.
                </p>
                <CodeBlock language="sql">{ sum }</CodeBlock>
            </section>

            <section>
                <h2>Limitation</h2>
                <p>
                    Le gros problème des fonctions d'agrégation, c'est qu'elles ne s'utilisent pas bien si nous avons 
                    des colonnes qui agrège leurs données et d'autres non. Voici un exemple:
                </p>
                <CodeBlock language="sql">{ limitation }</CodeBlock>
                <p>
                    Dans l'exemple ci-dessus, votre SGBD ne saura pas trop quoi faire avec la colonne du nom 
                    puisqu'elle n'est pas agrégé, contrairement au <IC>COUNT</IC>. Dans ce genre de cas, nous ne 
                    pouvons pas nécessairement déterminer ce que le SGBD va nous retourner comme donnée pour cette 
                    colonne. Ça dépends grandement du type de votre SGBD.
                </p>
                <p>
                    Puisque le retour est inconstant, vous devez utiliser les fonctions d'agrégation seulement si 
                    toutes les colonnes dans votre requête en utilisent.
                </p>
                <CodeBlock language="sql">{ limitationOk }</CodeBlock>
            </section>
        </>;
    }
}

export default DMLAggregatiion;