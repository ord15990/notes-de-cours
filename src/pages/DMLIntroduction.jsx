import React from 'react';
import IC from '../component/InlineCode'

class DMLIntrocution extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Le code SQL est un langage pour gérer, maintenir et rechercher dans les bases de données 
                    relationnelle. Ce langage est divisé en 4 types de SQL, chacun ayant son propre rôle:
                </p>
                <ul>
                    <li>Data Definition Language (DDL)</li>
                    <li><strong>Data Manipulation Language (DML)</strong></li>
                    <li>Data Control Language (DCL)</li>
                    <li>Transaction Control Language (TCL)</li>
                </ul>
                <p>
                    Dans ce module, nous verrons les bases du DML. Le Data Manipulation Language (DML) est la 
                    sous-section du SQL qui nous permet de manipuler les données d’une base de données.
                </p>
            </section>

            <section>
                <h2>CRUD</h2>
                <p>
                    Le CRUD est un acronyme définissant les 4 types de manipulations que nous pouvons faire sur les 
                    données d'une base de données:
                </p>
                <dl>
                    <dt>Create</dt>
                    <dd>Créer et insérer des données. En SQL on le fait avec la commande <IC>INSERT</IC>.</dd>

                    <dt>Read</dt>
                    <dd>Rechercher et lire des données. En SQL on le fait avec la commande <IC>SELECT</IC>.</dd>

                    <dt>Update</dt>
                    <dd>Mettre à jour des données. En SQL on le fait avec la commande <IC>UPDATE</IC>.</dd>

                    <dt>Delete</dt>
                    <dd>Supprimer des données. En SQL on le fait avec la commande <IC>DELETE</IC>.</dd>
                </dl>
                <p>
                    Dans les autre pages de ce module, vous verrez comment utiliser les différentes commande SQL du 
                    CRUD pour manipuler les données dans vos bases de données.
                </p>
            </section>
        </>;
    }
}

export default DMLIntrocution;