import React from 'react';
import IC from '../component/InlineCode';

import gymPhysique from '../resources/join-exemple.png';

export default class JoinIntro extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Il est souvent voulu de pouvoir sélectionner des données appartenant à plusieurs tables dans une
                    base de données. Bien qu'il soit possible de le faire en faisant plusieurs 
                    recherches <IC>SELECT</IC> différentes, nous avons avantage à le faire en une seule requête. Le 
                    rôle des jointures est justement de pouvoir sélectionner les données de plusieurs tables en une 
                    seule requête. Voici quelques avantages à utiliser les jointures :
                </p>
                <ul>
                    <li>
                        On économise sur la bande passante du réseau puisqu’on exécute une seule requête.
                    </li>
                    <li>
                        La requête est considéré comme une seule transaction, les données ne peuvent pas avoir
                        changé entre la recherche dans chaque table (cohérence des données).
                    </li>
                    <li>
                        Dans des requêtes plus complexe utilisant les jointures, les systèmes de gestion de bases de
                        données peuvent optimiser les performances de la requête.
                    </li>
                </ul>
                <p>
                    Nous utiliserons principalement les jointures avec les clés étrangères puisque celles-ci servent à 
                    faire les liens entre les tables. Une jointure sur une clé étrangère sera toujours beaucoup plus 
                    performante qu'une jointure sur un champ qui n'est pas une clé étrangère.
                </p>
            </section>

            <section>
                <h2>Exemple de données</h2>
                <p>
                    Nous utiliserons un exemple simple de base de données pour expliquer les différents types de
                    jointures pour ce module. Voici cette situation d'exemple:
                </p>
                <p>
                    Dans un gym, on veut que chaque membre puisse s’inscrire à un maximum d’un seul cours spécialisé à
                    la fois. Il peut y avoir plusieurs personnes dans chaque cours. Un membre peut aussi décider de
                    s’inscrire à aucun cours. Voici le modèle physique des données que nous utiliserons:
                </p>
                <img src={ gymPhysique } alt="Modèle physique des cours du Gym"/>
                <p>
                    Voici les données contenues dans la table <IC>membre</IC>:
                </p>
                <table>
                    <tr>
                        <th>id_membre</th><th>nom</th><th>prenom</th><th>telephone</th><th>id_cours</th>
                    </tr>
                    <tr><td>1</td><td>Tremblay</td><td>Maxime</td><td>11234567890</td><td>1</td></tr>
                    <tr><td>2</td><td>Ross</td><td>Bob</td><td>19870654321</td><td>1</td></tr>
                    <tr><td>3</td><td>Ketchum</td><td>Sasha</td><td>14560789123</td><td>NULL</td></tr>
                    <tr><td>4</td><td>Cena</td><td>John</td><td>16549870321</td><td>3</td></tr>
                </table>
                <p>
                    Voici les données contenues dans la table <IC>cours</IC>:
                </p>
                <table>
                    <tr>
                        <th>id_cours</th><th>nom</th><th>description</th>
                    </tr>
                    <tr><td>1</td><td>Yoga</td><td>Un cours de Yoga</td></tr>
                    <tr><td>2</td><td>Pilates</td><td>Un cours de Pilates</td></tr>
                    <tr><td>3</td><td>Kickboxing</td><td>Un cours de Kickboxing</td></tr>
                </table>
            </section>
        </>;
    }
}
