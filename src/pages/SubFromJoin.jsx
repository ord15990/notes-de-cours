import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const syntax1 = 
`SELECT colonne1, ...
FROM (
    SELECT colonne1, ... 
    FROM table1 
    WHERE ...
) AS alias
WHERE ...;`;

const syntax2 = 
`SELECT colonne1, ...
FROM table1
JOIN (
    SELECT colonne1, ... 
    FROM table2 
    WHERE ...
) AS autre_table ON table1.colonne1 = autre_table.colonne1
WHERE ...`;

const exampleFrom = 
`SELECT MAX(montant) AS max
FROM (
    SELECT order_id, SUM(quantity * unit_price) AS montant
    FROM order_details
    GROUP BY order_id
) AS OrderPrice;`;

const exampleJoin = 
`SELECT od.order_id, od.product_id, (od.quantity * od.unit_price) AS montant
FROM order_details od
INNER JOIN (
    SELECT order_id, MAX(quantity * unit_price) AS montant
    FROM order_details
    GROUP BY order_id
) AS odmax ON od.order_id = odmax.order_id AND 
    (od.quantity * od.unit_price) = odmax.montant`;

export default class SubFromJoin extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe</h2>
                <p>
                    Il est possible d'utiliser les sous-requêtes dans le <IC>FROM</IC> ou dans le <IC>JOIN</IC>. Dans 
                    ce type de requête, la sous-requête crée une forme de table temporaire dans laquelle nous irons 
                    chercher ou que nous joindrons à nos requêtes présentes. Voici leur syntaxe:
                </p>
                <ul>
                    <li>
                        Dans le <IC>FROM</IC>:
                        <CodeBlock language="sql">{ syntax1 }</CodeBlock>
                    </li>
                    <li>
                        Dans le <IC>JOIN</IC>:
                        <CodeBlock language="sql">{ syntax2 }</CodeBlock>
                    </li>
                </ul>
                <p>
                    Vous noterez dans les 2 cas l'utilisation de l'alias pour nommer la table temporaire retournée par 
                    votre sous-requête. Cet alias est nécessaire pour les sous-requêtes dans le <IC>FROM</IC> ou 
                    le <IC>JOIN</IC>. Sans l'alias, votre SQL aura une erreur.
                </p>
                <ColoredBox heading="À noter">
                    <p>
                        C'est 2 types de sous-requête sont utilisé dans certains cas spéciaux. En effet, certaines 
                        requêtes ne peuvent pas se faire avec les <IC>JOIN</IC>, mais peuvent se faire avec ces 2 types de 
                        sous-requêtes. 
                    </p>
                    <p>
                        Par exemple, avec une sous-requête dans un <IC>FROM</IC>, nous pouvons faire des 
                        agrégations d'agrégation, ce qui est impossible avec de <IC>JOIN</IC>. De la même façon, une 
                        sous-requête dans un <IC>JOIN</IC> nous permettra de sélectionner le maximum ou le minimum par 
                        groupe.
                    </p>
                </ColoredBox>
            </section>

            <section>
                <h2>Exemples</h2>
                <p>
                    La requête ci-dessous sélectionne le montant le plus élevé d'une commande.
                </p>
                <CodeBlock language="sql">{ exampleFrom }</CodeBlock>
                <p>
                    La requête ci-dessous sélectionne le id du produit qui a été vendu pour le plus d'argent pour 
                    chaque commande.
                </p>
                <CodeBlock language="sql">{ exampleJoin }</CodeBlock>
                <p>
                    Si la requête ci-dessus vous embrouille, ne vous inquiétez pas, ce n'est pas une requête facile à 
                    comprendre et Google a définitivement aidé le professeur à la compléter.
                </p>
            </section>
        </>;
    }
}
