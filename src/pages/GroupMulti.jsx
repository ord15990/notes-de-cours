import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const groupMulti = 
`SELECT marque, nom, COUNT(*) AS total
FROM vente_auto
GROUP BY marque, nom;`;

const groupNonGroup = 
`SELECT marque, nom, COUNT(*) AS total
FROM vente_auto
GROUP BY marque;`;

export default class GroupMulti extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe</h2>
                <p>
                    Il est possible de faire du groupage sur plusieurs colonnes à la fois. Voici un exemple:
                </p>
                <CodeBlock language="sql">{ groupMulti }</CodeBlock>
                <table>
                    <tr><th>marque</th><th>nom</th><th>total</th></tr>
                    <tr><td>Chevrolet</td><td>Cavalier</td><td>2</td></tr>
                    <tr><td>Chevrolet</td><td>Cobalt</td><td>1</td></tr>
                    <tr><td>Hyundai</td><td>Accent</td><td>1</td></tr>
                    <tr><td>Hyundai</td><td>Elantra</td><td>1</td></tr>
                    <tr><td>Kia</td><td>Rio</td><td>2</td></tr>
                    <tr><td>Toyota</td><td>Camry</td><td>1</td></tr>
                    <tr><td>Toyota</td><td>Yaris</td><td>2</td></tr>
                </table>
            </section>

            <section>
                <h2>Colonne hors des groupes</h2>
                <p>
                    Si dans votre requête SQL vous aller chercher une colonne qui n'est pas spécifiée dans une 
                    fonction d'agrégation ou dans la clause <IC>GROUP BY</IC>, vous aurez un comportement un peu 
                    inattendu: 
                </p>
                <p>
                    Seule la première valeur rencontré pour chaque groupe sera retourné.
                </p>
                <p>
                    Voici un exemple:
                </p>
                <CodeBlock language="sql">{ groupNonGroup }</CodeBlock>
                <table>
                    <tr><th>marque</th><th>nom</th><th>total</th></tr>
                    <tr><td>Chevrolet</td><td>Cavalier</td><td>3</td></tr>
                    <tr><td>Hyundai</td><td>Accent</td><td>2</td></tr>
                    <tr><td>Kia</td><td>Rio</td><td>2</td></tr>
                    <tr><td>Toyota</td><td>Yaris</td><td>3</td></tr>
                </table>
                <p>
                    Ce genre de requête peut vraiment porter à confusion. Si vous avez besoin du groupage, je vous 
                    suggère donc d'ajouter chaque colonne dans le <IC>SELECT</IC> à la clause <IC>GROUP BY</IC> ou 
                    dans une fonction d'agrégation.
                </p>
            </section>
        </>;
    }
}
