import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const syntax = 
`EXPLAIN
SELECT ...;

EXPLAIN
INSERT ...;

EXPLAIN
UPDATE ...;

EXPLAIN
DELETE ...;`;

export default class ExplainSyntax extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Faire des recherches dans une petite base de données ne pose généralement aucun problème.
                    Toutefois, dès que l'on commence à avoir beaucoup de données, notre situation se compliquera.
                    Souvent, les requêtes à la base de données commenceront à être plus lentes. Parfois, un gestionnaire
                    de base de données inexpérimenté aura tendance à mettre la base de données sur un serveur plus
                    performant, mais cela n'aide souvent pas beaucoup.
                </p>
                <p>
                    Comment pouvons-nous alors nous assurer que la base de données aura toujours de bonnes
                    performances? Ce sera en optimisant nos requêtes et notre base de données. Pour nous aider, nous
                    utiliserons la commande <IC>EXPLAIN</IC>.
                </p>
            </section>

            <section>
                <h2>Syntaxe</h2>
                <p>
                    Voici la syntaxe de la commande <IC>EXPLAIN</IC>:
                </p>
                <CodeBlock language="sql">{ syntax }</CodeBlock>
                <p>
                    Bien que la commande <IC>EXPLAIN</IC> peut être utilisée avec les <IC>INSERT</IC>, <IC>UPDATE</IC> et <IC>DELETE</IC>, nous
                    l'utiliserons majoritaire avec le SELECT puisque c'est souvent ces requêtes qui bénéficieront le plus
                    d'une optimisation.
                </p>
                <p>
                    Le résultat de la commande <IC>EXPLAIN</IC> est un peu difficile à comprendre. Dans la prochaine section,
                    nous étudierons les champs important à regarder, toutefois, si vous voulez trouver l'information
                    complète sur cette commande, vous pouvez vous rendre sur le site de la documentation MySQL à
                    l'adresse suivante:
                </p>
                <p>
                    <a href="https://dev.mysql.com/doc/refman/8.0/en/explain-output.html" target="_blank" rel="noopener noreferrer">
                        EXPLAIN Output Format
                    </a>
                </p>
            </section>
        </>;
    }
}
