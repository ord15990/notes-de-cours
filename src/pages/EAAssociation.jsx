import React from 'react';
import Video from '../component/Video';
import ColoredBox from '../component/ColoredBox';

import step2 from '../resources/ea-step2.png'

class EAAssociation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Définir les associations</h2>
                <p>
                    L’association est le deuxièment élément le plus important de notre diagramme. Une association est
                    essentiellement un lien entre deux entités. En général, les liens sont assez facile
                    à identifier dans la situation.
                </p>
                <p>
                    Dans notre situation, nous avons simplement deux associations:
                </p>
                <ul>
                    <li>Entre l’entité client et commande</li>
                    <li>Entre l’entité commande et produits</li>
                </ul>
                <ColoredBox heading="Attention:">
                    Il est facile de créer des associations de trop. Par exemple, dans notre situation, quelqu’un aurait pu
                    ajouter une association entre l’entité client et produit tel que «&nbsp;Un client achète un produit&nbsp;» et
                    «&nbsp;Un produit est acheté par un client&nbsp;». Toutefois, si nous lisons bien la situation, nous voyons bien
                    que le client n’achète pas directement des produits. Dans notre situation, le client fait des
                    commandes et les commandes contiennent des produits.
                </ColoredBox>
            </section>

            <section>
                <h2>Déssiner les associations</h2>
                <p>
                    Lorsqu'il est temps de dessiner les associations dans votre diagramme, vous devez tracer un lien entre
                    les entités pour chacune d'entre elle et de leur donner un nom pour bien comprendre le lien entre nos 
                    deux entités.
                </p>
                <img src={ step2 } alt="Déssiner les associations" />
                <p>
                    Vous constaterez que nous nommons les associations dans les deux sens. Dans le diagramme ci-dessus,
                    l’association entre le client et la commande se lit:
                </p>
                <ul>
                    <li>«&nbsp;Un client passe une commande&nbsp;»</li>
                    <li>«&nbsp;Une commande est passée par un client&nbsp;»</li>
                </ul>
            </section>

            <section>
                <Video title="Modèle entité-association - Associations" src="https://www.youtube.com/embed/Y89eh_7iRZs" />
            </section>
        </>;
    }
}

export default EAAssociation;