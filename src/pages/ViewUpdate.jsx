import React from 'react';
import IC from '../component/InlineCode';
import ColoredBox from '../component/ColoredBox';

export default class ViewUpdate extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Il est possible de d'insérer, de mettre à jour ou de supprimer des données directement à partir 
                    des vues, sans avoir besoin de passer par les tables originales. Il y a toutefois une restriction 
                    pour utiliser ces fonctionnalités: 
                </p>
                <p>
                    Chaque donnée de la vue doit avoir une relation 1 pour 1 avec les données de leur table d'origine.
                </p>
                <p>
                    Cette restriction est un peu complexe à comprendre et plusieurs facteurs peuvent entrer en cause. 
                    Parfois, seulement certaines colonnes de la vue sont modifiable et d'autres fois, la vue entière 
                    est complètement bloqué. Je vous suggère fortement de faire des tests avec votre vue pour voir ce 
                    qui est modifiable.
                </p>
                <p>
                    Voici quelques éléments dans vos requêtes qui peuvent nous empêcher de modifier directement une 
                    vue:
                </p>
                <ul>
                    <li>
                        Fonctions d'aggregation (<IC>SUM</IC>, <IC>MIN</IC>, <IC>MAX</IC>, <IC>COUNT</IC>)
                    </li>
                    <li><IC>DISTINCT</IC></li>
                    <li><IC>GROUP BY</IC></li>
                    <li><IC>HAVING</IC></li>
                    <li><IC>UNION</IC> ou <IC>UNION ALL</IC></li>
                    <li>
                        Utiliser une autre vue non modifiable dans le <IC>FROM</IC> de notre vue
                    </li>
                    <li>
                        Utiliser une valeur littérale (ex: <IC>3</IC>, <IC>'Allo'</IC>)
                    </li>
                </ul>
                <p>
                    Il existe de nombreux autres cas. Certains empêchent seulement la suppression, la mise à jour ou
                    encore l'insertion de données. Pour plus d'information sur ce genre de règles, vous pouvez vou référer
                    à la page de documentation suivante:
                </p>
                <p>
                    <a href="https://dev.mysql.com/doc/refman/8.0/en/view-updatability.html" target="_blank" rel="noopener noreferrer">
                        Updatable and Insertable Views
                    </a>
                </p>
                <ColoredBox heading="À noter">
                    Si vous n'êtes pas certai n de si votre vue permet certaines opération s , vous pouvez simplement
                    tester une requête sur celle ci. Si la vue ne permet pas l'opération, vous aurez une erreur. N'hésitez
                    pas à faire des tests pour voir que tout fonctionne comme voulu.
                </ColoredBox>
            </section>
        </>;
    }
}
