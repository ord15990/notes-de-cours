import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const syntax1 = 
`SELECT colonne1, ...
FROM table1
WHERE colonne1 = (
    SELECT colonne1
    FROM table2
    WHERE ...
)`;

const syntax2 = 
`SELECT colonne1, ... 
FROM table1
WHERE colonne1 IN (
    SELECT colonne1
    FROM table2
    WHERE ...
)`;

const example = 
`SELECT *
FROM orders
WHERE id IN (
    SELECT order_id
    FROM order_details
    WHERE product_id = 8
)`;

export default class SubCondition extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe</h2>
                <p>
                    C'est généralement dans le <IC>WHERE</IC> ou le <IC>HAVING</IC> que nous verrons les sous-requêtes. 
                    Il est important de savoir d'avance le nombre de résultats retournés par la sous requête d'avance. 
                    Vous avez 2 choix:
                     Si plusieurs valeurs peuvent être retournées, nous utiliserons l'opérateur " IN ".
                </p>
                <ul>
                    <li>
                        Si une seule valeur est toujours retournée par la sous-requête, nous utiliserons 
                        le <IC>=</IC> comme opérateur de comparaison.
                        <CodeBlock language="sql">{ syntax1 }</CodeBlock>
                    </li>
                    <li>
                        Si plusieurs valeurs peuvent être retournées par la sous-requête, nous utiliserons 
                        l'opérateur <IC>IN</IC> pour faire la comparaison.
                        <CodeBlock language="sql">{ syntax2 }</CodeBlock>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    La requête ci-dessous sélectionne les commandes contenant le produit ayant l'id <IC>8</IC>.
                </p>
                <CodeBlock language="sql">{ example }</CodeBlock>
                <p>
                    Le truc pour écrire cette requête est de la sous-divisé en plus petite requête. Dans ce cas-ci, 
                    la première chose à faire est de trouver le produit ayant l'id <IC>8</IC>. La deuxième étape 
                    consiste àTrouver les commandes qui utilisent ce produit.
                </p>
                <ColoredBox heading="Attention">
                    <p>
                        L'exemple ci dessus pourrait être fait à l'aide de jointures. Dans la majorité des cas, il est 
                        beaucoup plus efficace d'utiliser les jointures que les sous-requêtes. 
                    </p>
                    <p>
                        Vous devriez toujours utiliser les jointures si c'est possible et utiliser les sous requêtes 
                        uniquement lorsque les jointures ne permettent pas de faire la requête voulu.
                    </p>
                </ColoredBox>
            </section>

            <section>
                <h2>Autres conditions</h2>
                <p>
                    Dans l'exemple ci-dessus, nous utilisons les sous-requêtes dans le <IC>WHERE</IC> d'un <IC>SELECT</IC>.
                    Il est toutefois possible de mettre des sous-requêtes dans le <IC>WHERE</IC> d'un <IC>DELETE</IC> ou 
                    d'un <IC>UPDATE</IC>.
                </p>
                <p>
                    De la même façon, il est possible de mettre des sous-requêtes dans la clause <IC>HAVING</IC> de vos 
                    requêtes puisque c'est là-aussi une condition.
                </p>
            </section>
        </>;
    }
}
