import React from 'react';

export default class LaboratoireEA extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    Pour chacune des mises en situation ci-dessous, créer un 
                    modèle entité-association. Les mises en situation sont 
                    volontairement imprécises. Ceci veut dire que vous aurez 
                    probablement à inventer certains entités, association ou 
                    attribut.
                </p>
                <p>
                    Il n'y a pas de solution unique à ces exercices. N'hésitez 
                    pas à comparer vos résultats avec ceux des autres 
                    étudiants.
                </p>
            </section>

            <section>
                <h2>Mises en situation</h2>
                <ol>
                    <li>
                        Dans une école, les étudiants s’inscrivent à des cours. Les cours sont disponible seulement
                        durant certain semestre et ils sont enseigné par des enseignants. Les cours peuvent faire partie
                        de plusieurs programmes.
                    </li>
                    <li>
                        Dans un jeu vidéo (un RPG), votre personnage possède des objets de différents types. Il peut
                        avoir des armes, des armures et de la nourriture. Le personnage peut équiper les armures et
                        les armes.
                    </li>
                    <li>
                        Une petite librairie locale veut pouvoir louer ses livres au travers d’un portail Web. Elle veut
                        que ses clients puisse s’y connecter avec un compte et emprunter les livres qu’ils recherchent
                    </li>
                    <li>
                        Dans votre calendrier électronique, vous ajoutez des rendez-vous qui peuvent être récurrent.
                    </li>
                    <li>
                        Une pâtisserie a un système de commande en ligne pour ses clients. Les clients peuvent
                        commander des biscuits ou des gâteaux. Pour faire une commande les clients ont seulement
                        besoin d’entrer le type et le nombre de biscuit ou le type et la grosseur du gâteau. Le paiement
                        est nécessaire pour faire la commande.
                    </li>
                    <li>
                        Vous êtes un fan de pokémon. Vous travaillez sur un projet qui offre de l’information sur les
                        pokémons aux autres fans. Vous voulez que les fans puisse rechercher les pokémon au travers
                        de votre application et obtenir leurs évolutions et les attaques qu’ils peuvent apprendre.
                    </li>
                    <li>
                        Une compagnie d’import-export importe des produits de pays asiatiques pour les revendre en
                        Amérique. Ils gardent un stock de produits qu’ils peuvent revendre à des clients.
                    </li>
                    <li>
                        Dans un LAN (party), les participants peuvent s’inscrire à des compétitions de jeux au
                        travers d’un compte en ligne. Les résultats des compétitions sont sauvegardées à leur compte.
                    </li>
                </ol>
            </section>
        </>;
    }
}
