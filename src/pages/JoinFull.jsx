import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import fullJoin from '../resources/join-full.png';

const exemple = 
`SELECT membre.prenom, membre.nom, cours.nom
FROM cours 
FULL JOIN membre ON membre.id_cours = cours.id_cours`;

export default class JoinFull extends React.Component {
    render() {
        return <>
            <section>
                <h2>Description</h2>
                <p>
                    Le <IC>FULL JOIN</IC> permet de sélectionner toutes les données de la table de gauche et de 
                    droite, même si elles n’ont pas de données dans la table opposé. 
                </p>
                <p>
                    Nous pouvons représenter les données retournées par le <IC>FULL JOIN</IC> avec le diagramme 
                    suivant:
                </p>
                <img src={ fullJoin } alt="Full Join"/>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Voici un exemple de <IC>FULL JOIN</IC> avec notre situation d'exemple des cours du gym:
                </p>
                <CodeBlock language="sql">{ exemple }</CodeBlock>
                <p>
                    La requête ci-dessus produira le résultat suivant:
                </p>
                <table>
                    <tr><th>membre.prenom</th><th>membre.nom</th><th>cours.nom</th></tr>
                    <tr><td>Maxime</td><td>Tremblay</td><td>Yoga</td></tr>
                    <tr><td>Bob</td><td>Ross</td><td>Yoga</td></tr>
                    <tr><td>John</td><td>Cena</td><td>Kickboxing</td></tr>
                    <tr><td>Sasha</td><td>Ketchum</td><td><IC>NULL</IC></td></tr>
                    <tr><td><IC>NULL</IC></td><td><IC>NULL</IC></td><td>Pilates</td></tr>
                </table>
                <p>
                    Vous noterez que le membre Sasha Ketchum et le cours de pilates sont présent dans les résultats, 
                    mais qu'ils seront associés aux valeurs <IC>NULL</IC> puisqu'il n'ont respectivement pas de cours 
                    ou de membre associé à eux. 
                </p>
                <ColoredBox heading="À noter">
                    Bien que le <IC>FULL JOIN</IC> soit présent dans la plupart des bases de données relationnelles depuis leurs
                    débuts, MySQL n’a pas de <IC>FULL JOIN</IC>. L’exemple ci-dessus ne fonctionne donc pas avec MySQL. 
                    Le <IC>FULL JOIN</IC> n’a toutefois pas beaucoup d’utilité, cela ne nous cause donc pas vraiment de problèmes.
                </ColoredBox>
            </section>
        </>;
    }
}
