import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const syntax =
`DELIMITER //

CREATE TRIGGER nom_trigger
{ BEFORE | AFTER } { INSERT | UPDATE | DELETE }
ON nom_table FOR EACH ROW
BEGIN
    /* Code SQL à exécuter */
END//

DELIMITER ;`;

const drop = 
`DROP TRIGGER nom_trigger;`;

const delimiter =
`DELIMITER //
DELIMITER ;`;

export default class TriggerSyntax extends React.Component {
    render() {
        return <>
            <section>
                <h2>Créer et supprimer</h2>
                <p>
                    Voici la syntaxe de base pour créer un trigger:
                </p>
                <CodeBlock language="sql">{ syntax }</CodeBlock>
                <p>
                    Pour supprimer un déclencheur, on utilise simplement le mot-clé <IC>DROP</IC>
                </p>
                <CodeBlock language="sql">{ drop }</CodeBlock>
                <p>
                    Comme vous pouvez le voir, la création d'un déclencheur peut être assez complexe et utilise 
                    plusieurs nouveaux mot-clés. Nous verrons l'utilité de ces mot-clés dans le reste de cette page.
                </p>
            </section>

            <section>
                <h2>Détails de base</h2>
                <p>
                    Voici une liste des autres termes que vous pouvez voir dans la requête de création du déclencheur 
                    et leur explication:
                </p>
                <ul>
                    <li>
                        Le <IC>ON</IC> vous permet de spécifier la table sur laquelle vous voulez ajouter le 
                        déclencheur. Un déclencheur ne peut pas être utiliser sur plusieurs tables à la fois dans la 
                        même requête. Si vous voulez mettre le même déclencheur sur plusieurs tables, vous devez faire 
                        du copier/coller.
                    </li>
                    <li>
                        Le <IC>INSERT</IC>, <IC>UPDATE</IC> ou <IC>DELETE</IC> vous permet de spécifier sur quelle 
                        opération dans la table le déclencheur s'exécute. Par exemple, si vous 
                        indiquez <IC>INSERT</IC>, le code du déclencheur s'exécutera automatiquement 
                        lorsqu'un <IC>INSERT</IC> est fait sur la table.
                    </li>
                    <li>
                        Le <IC>BEGIN</IC> et le <IC>END</IC> définissent un bloc de code qui sera exécuté par le
                        déclencheur. Ce bloc de code est appelé le corps (le body) du déclencheur.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Délimiteur</h2>
                <p>
                    L'instruction <IC>DELIMITER</IC> au début et à la fin du code est obligatoire pour que ce genre 
                    de code SQL fonctionne. En effet, si nous ne changeons pas le délimiteur de fin de ligne (par 
                    défaut le point-virgule <IC>;</IC>), l'interpréteur de SQL pensera que le code du déclencheur se 
                    termine au premier point-virgule qu'il rencontre, ce qui risque fortement d'être une ligne de code 
                    entre les instructions <IC>BEGIN</IC> et <IC>END</IC>.
                </p>
                <p>
                    L'instruction <IC>DELIMITER</IC> permet donc de changer le délimiteur de fin de ligne. Ainsi, nous 
                    pourrons définir un déclencheur contenant plusieurs instructions sans problèmes. N'oubliez pas de 
                    remettre le délimiteur à sa valeur par défaut après la création du déclencheur.
                </p>
                <CodeBlock language="sql">{ delimiter }</CodeBlock>
            </section>

            <section>
                <h2>Avant ou après</h2>
                <p>
                    Les mots-clés <IC>BEFORE</IC> et <IC>AFTER</IC> permettent de spécifier quand le déclencheur va 
                    être exécuté. Si on mentionne <IC>BEFORE</IC>, on indique que le déclencheur devra être exécuté 
                    avant l'opération sur laquelle il est liée, donc avant le <IC>INSERT</IC>, le <IC>UPDATE</IC> ou 
                    le <IC>DELETE</IC>. Si on utilise <IC>AFTER</IC>, c'est simplement que le code est exécuté après 
                    l'opération.
                </p>
                <p>
                    Les déclencheurs ont aussi des comportements différents en cas d'erreur dépendant de si le 
                    mot-clé <IC>BEFORE</IC> ou <IC>AFTER</IC> est utilisé:
                </p>
                <ul>
                    <li>
                        Dans un déclencheur <IC>BEFORE</IC>, si le code à exécuter cause une erreur, l'opération liée 
                        au déclencheur n'est pas exécutée du tout.
                    </li>
                    <li>
                        Dans un déclencheur <IC>BEFORE</IC>, le code à exécuter est exécuté, même si l'opération liée 
                        au déclencheur cause une erreur.
                    </li>
                    <li>
                        Dans un déclencheur <IC>AFTER</IC>, le code à exécuter est exécuté seulement si l'opération a 
                        réussis sans causer d'erreur.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Pour chaque rangée</h2>
                <p>
                    Tous les déclencheurs dans MySQL sont exécutés pour chaque rangée. C'est-à-dire que si nous 
                    exécutons un code SQL de <IC>DELETE</IC> qui supprime 5 rangées, un déclencheur qui serait lié à 
                    cette opération serait exécuté 5 fois. C'est pour ça que MySQL utilise les 
                    mot-clés <IC>FOR EACH ROW</IC>.
                </p>
                <p>
                    Certains autres systèmes de base de données permettent de mettre des déclencheur sur l'opération
                    complète. Dans ce genre de cas, si nous prenons l'exemple précédant où nous supprimons 5 rangées, 
                    le déclencheur n'est exécuté qu'une seule fois.
                </p>
            </section>
        </>;
    }
}
