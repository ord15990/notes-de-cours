import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock'

const limit = 
`SELECT *
FROM commandes
WHERE date < '2020-01-01'
LIMIT 50`;

const distinct =
`SELECT DISTINCT adresse, code_postal
FROM client;`;

class DMLLimitDistinct extends React.Component {
    render() {
        return <>
            <section>
                <h2>Limite</h2>
                <p>
                    Il est possible de spécifier un nombre maximum de données à retourner pour nos recherches dans une 
                    base de données. En MySQL ou MariaDB, nous utiliserons le mot-clé <IC>LIMIT</IC>. Ce mot-clé 
                    permet de spécifier un maximum de rangés à retourner. Ainsi, si une recherche retourne
                    beaucoup de rangées, nous pouvons limiter le retour à une certaines quantités de rangées. Ce mot-clé
                    est différent d'une base de données à l'autre. Par exemple, dans les produits Microsoft, on 
                    utilise le mot-clé <IC>TOP</IC> et dans Oracle on utilise le mot-clé <IC>ROWNUM</IC>.
                </p>
                <CodeBlock language="sql">{ limit }</CodeBlock>
                <p>
                    Comme démontré par l'exemple ci-dessus, ce mot-clé va généralement se retrouver à la fin de votre 
                    requête SQL.
                </p>
            </section>

            <section>
                <h2>Valeurs distinctes</h2>
                <p>
                    Si nous voulons retirer les doublons lors de nos recherches, nous pouvons utiliser le 
                    mot-clé <IC>DISTINCT</IC>. Ainsi, même si plusieurs rangées ont les mêmes valeurs, seulement une 
                    seule rangée sera retournées pour toutes ces rangées.
                </p>
                <CodeBlock language="sql">{ distinct }</CodeBlock>
                <p>
                    La requête ci-dessus pourrait être pratique si nous désirons envoyer des cartes de Noël par la 
                    poste à nos clients. En effet, si plusieurs clients habitent à la même adresse, l'adresse sera 
                    retourné une seule fois, nous permettant d'économiser un peu sur le nombre de cartes à envoyer.
                </p>
            </section>
        </>;
    }
}

export default DMLLimitDistinct;