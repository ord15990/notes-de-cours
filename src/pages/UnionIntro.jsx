import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const unionSyntax = 
`SELECT nom_colonne FROM une_table
UNION
SELECT nom_colonne FROM autre_table;`;

const unionExample = 
`SELECT marque
FROM achat_auto
UNION
SELECT marque
FROM vente_auto;`;

export default class UnionIntro extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Il est parfois voulu de joindre le résultat de deux requêtes SQL ensembles. C'est généralement le 
                    cas lorsque l'on a besoin de sélectionner des données similaires dans plusieurs tables de notre 
                    base de données et que l'on ne veut pas le faire avec plusieurs requêtes. Voici quelques exemples:
                </p>
                <ul>
                    <li>
                        Chercher toutes les descriptions de produits et les description de services dans le but de les 
                        faire traduire.
                    </li>
                    <li>
                        Chercher toutes les adresses courriels des clients et des employés dans le but d'envoyer un 
                        courriel de masse.
                    </li>
                    <li>
                        Chercher tous les desserts dans un magasins vendant différents types de desserts, comme des 
                        gâteaux et des biscuits.
                    </li>
                </ul>
                <p>
                    La clause <IC>UNION</IC> du langage SQL nous permettra justement de combiner plusieurs requêtes 
                    SQL pour les retourner sous une seule réponse.
                </p>
            </section>

            <section>
                <h2>Syntaxe</h2>
                <p>
                    La syntaxe de base de la clause <IC>UNION</IC> est très simple. On écrit chacune de nos requêtes 
                    et on met le mot-clé <IC>UNION</IC> entre chacune d'elle:
                </p>
                <CodeBlock language="sql">{ unionSyntax }</CodeBlock>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Comme d'habitude, il est plus facile de comprendre avec un exemple. Pour nous aider, nous 
                    utiliserons la table <IC>achat_automobile</IC> suivante en plus de la 
                    table <IC>vente_automobile</IC> utilisé précédemment:
                </p>
                <table>
                    <tr><th>id_automobile</th><th>marque</th><th>nom</th><th>annee</th></tr>
                    <tr><td>1</td><td>Chevrolet</td><td>Cobalt</td><td>2010</td></tr>
                    <tr><td>2</td><td>Mercedes-Benz</td><td>E-Class Coupe</td><td>2016</td></tr>
                    <tr><td>3</td><td>Kia</td><td>Forte</td><td>2012</td></tr>
                    <tr><td>4</td><td>Tesla</td><td>Model 3</td><td>2021</td></tr>
                </table>
                <p>
                    Avec la clause <IC>UNION</IC>, nous pouvons joindre une partie des données de la 
                    table <IC>achat_automobile</IC> et <IC>vente_automobile</IC> ensemble. Voici un exemple qui 
                    recherche toutes les marques des voitures présentes dans ces 2 tables:
                </p>
                <CodeBlock language="sql">{ unionExample }</CodeBlock>
                <table>
                    <tr><th>marque</th></tr>
                    <tr><td>Chevrolet</td></tr>
                    <tr><td>Mercedes-Benz</td></tr>
                    <tr><td>Kia</td></tr>
                    <tr><td>Toyota</td></tr>
                    <tr><td>Hyundai</td></tr>
                    <tr><td>Tesla</td></tr>
                </table>
                <p>
                    Comme vous pouvez le remarquer, la clause <IC>UNION</IC> agit aussi comme une 
                    clause <IC>DISTINCT</IC>, c'est-à-dire qu'elle élémine automatiquement les doublons. Dans la 
                    majorité des cas, c'est tout à fait correct et nécessaire. Si vous ne voulez pas retirer les 
                    doublons, vous devrez utiliser une clause différente, mais similaire: la clause <IC>UNION ALL</IC>.
                </p>
            </section>
        </>;
    }
}
