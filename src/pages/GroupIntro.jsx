import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const groupSyntax = 
`SELECT colonne1, ...
FROM une_table
GROUP BY colonne1;`;

const groupClauseOrder = 
`SELECT a.colonne1, b.colonne2, ...
FROM une_table a
INNER JOIN autre_table b ON a.pk = b.fk
WHERE condition
GROUP BY colonne1
ORDER BY colonne1;`;

export default class GroupIntro extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Dans les pages précédante, nous avons vu les fonctions d'aggrégation (<IC>MIN</IC>, <IC>MAX</IC>, <IC>COUNT</IC>, <IC>AVG</IC>, <IC>SUM</IC>). 
                    Ces fonctions permettent d'exécuter des calculs sur l'ensemble des données retournées par une requête. 
                    Il pourrait toutefois être intéressant d'exécuter ces calculs sur des groupes dans nos données et 
                    de retourner les résultats dans une seule et même requête. Voici quelques exemples:
                </p>
                <ul>
                    <li>
                        Chercher le profit fait par produit dans un magasin.
                    </li>
                    <li>
                        Chercher le nombre de vente par produit dans un magasin.
                    </li>
                    <li>
                        Chercher le K/D/A de chaque joueur professionnel de League of Legends par saison.
                    </li>
                    <li>
                        Chercher la moyenne de coups de joueurs de golf par saison.
                    </li>
                    <li>
                        Chercher le nombre de réservations pour chaque livre d'une bibliothèque.
                    </li>
                </ul>
                <p>
                    Dans ce module, nous verrons comment utiliser les clauses <IC>GROUP BY</IC> et <IC>HAVING</IC> du 
                    langage SQL qui nous permettront justement de maximiser l'utilisation des fonctions d'agrégation 
                    et de les exécuter sur des groupes de données.
                </p>
            </section>

            <section>
                <h2>Syntaxe</h2>
                <p>
                    Pour utiliser les groupes, nous utiliserons les mot-clés <IC>GROUP BY</IC> du langage SQL. Ces 
                    mot-clés s'utilise immédiatement après la clause <IC>FROM</IC> ou <IC>WHERE</IC> (s'il y a lieu) de 
                    vos requêtes SQL. Voici la syntaxe de base des groupes:
                </p>
                <CodeBlock language="sql">{ groupSyntax }</CodeBlock>
                <p>
                    Dans le cas d'une requête SQL plus complexe, l'ordre de clauses de la requête serait:
                </p>
                <CodeBlock language="sql">{ groupClauseOrder }</CodeBlock>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Il est plus facile de comprendre les groupes avec des exemples. Pour le restant du module nous 
                    utiliserons une table <IC>vente_automobile</IC> avec les données suivantes:
                </p>
                <table>
                    <tr><th>id_automobile</th><th>marque</th><th>nom</th><th>annee</th><th>couleur</th><th>prix_vente</th></tr>
                    <tr><td>1</td><td>Chevrolet</td><td>Cavalier</td><td>2004</td><td>rouge</td><td>1324.00</td></tr>
                    <tr><td>2</td><td>Kia</td><td>Rio</td><td>2010</td><td>gris</td><td>9637.00</td></tr>
                    <tr><td>3</td><td>Toyota</td><td>Yaris</td><td>2011</td><td>noir</td><td>12111.00</td></tr>
                    <tr><td>4</td><td>Toyota</td><td>Yaris</td><td>2010</td><td>gris</td><td>11256.00</td></tr>
                    <tr><td>5</td><td>Hyundai</td><td>Accent</td><td>2005</td><td>rouge</td><td>1562.00</td></tr>
                    <tr><td>6</td><td>Chevrolet</td><td>Cobalt</td><td>2011</td><td>rouge</td><td>10459.00</td></tr>
                    <tr><td>7</td><td>Hyundai</td><td>Elantra</td><td>2009</td><td>noir</td><td>8678.00</td></tr>
                    <tr><td>8</td><td>Kia</td><td>Rio</td><td>2009</td><td>bleu</td><td>9652.00</td></tr>
                    <tr><td>9</td><td>Chevrolet</td><td>Cavalier</td><td>2005</td><td>gris</td><td>2068.00</td></tr>
                    <tr><td>10</td><td>Toyota</td><td>Camry</td><td>2010</td><td>rouge</td><td>10582.00</td></tr>
                </table>
            </section>
        </>;
    }
}
