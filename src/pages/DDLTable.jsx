import React from 'react';
import Video from '../component/Video';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const createTable = 
`CREATE TABLE nom_table (
    colonne1 type_donnee,
    colonne2 type_donnee,
    -- ...
);`;

const dropTable = 
`-- Supprime une table
DROP TABLE nom_table;

-- Supprime toutes les données d'une table
TRUNCATE TABLE nom_table;`;

const alterTable = 
`-- Ajouter une colonne
ALTER TABLE nom_table ADD nom_colonne type_donnee;

-- Supprimer une colonne
ALTER TABLE nom_table DROP nom_colonne;

-- Modifier le type de données d'une colonne
ALTER TABLE nom_table MODIFY nom_colonne nouveau_type_donnee;`;

const exemple = 
`-- Créer la base de données
DROP DATABASE IF EXISTS boutique_jouet;
CREATE DATABASE boutique_jouet 
CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

-- Utiliser la base de données pour le reste des opérations
USE boutique_jouet;

-- Créer la table client
DROP TABLE IF EXISTS client;
CREATE TABLE client(
    id_client INT,
    prenom VARCHAR(50),
    nom VARCHAR(50),
    adresse VARCHAR(100),
    ville VARCHAR(50),
    province VARCHAR(50),
    pays VARCHAR(50)
);

-- Créer la table commande
DROP TABLE IF EXISTS commande;
CREATE TABLE commande(
    id_commande INT,
    id_client INT,
    prix_total FLOAT,
    date DATE
);

-- Créer la table categorie
DROP TABLE IF EXISTS categorie;
CREATE TABLE categorie(
    id_categorie INT,
    nom_categorie VARCHAR(50)
);

-- Créer la table produit
DROP TABLE IF EXISTS produit;
CREATE TABLE produit(
    id_produit INT,
    id_categorie INT,
    nom VARCHAR(100),
    prix FLOAT
);

-- Créer la table commande_produit
DROP TABLE IF EXISTS commande_produit;
CREATE TABLE commande_produit(
    id_commande INT,
    id_produit INT,
    quantite INT,
    prix_total_produit FLOAT
);`;

class DDLTable extends React.Component {
    render() {
        return <>
            <section>
                <h2>Création</h2>
                <p>
                    Avant de commencer à manipuler les tables dans votre code SQL, assurez-vous de spécifier sur 
                    quelle base de données vous voulez travailler avec la commande <IC>USE</IC>.
                </p>
                <p>
                    Pour créer une table, nous utiliserons la commande SQL suivante:
                </p>
                <CodeBlock language="sql">{ createTable }</CodeBlock>
            </section>
                
            <section>
                <h2>Suppression</h2>
                <p>
                    Pour supprimer une table, ou pour supprimer les données d'une table, vous pouvez utiliser les 
                    commandes suivantes:
                </p>
                <CodeBlock language="sql">{ dropTable }</CodeBlock>
            </section>
                
            <section>
                <h2>Modification</h2>
                <p>
                    Bien que nous essayons de le faire rarement, il est possible de modifier une table. Nous le ferons 
                    généralement uniquement lorsque c'est nécessaire pour une base de données qui ne peut pas être 
                    recréé. C'est souvent le cas lorsque la base de données contient déjà beaucoup de données ou 
                    lorsque la base de données ne peut pas être mise hors ligne. Voici comment le faire:
                </p>
                <CodeBlock language="sql">{ alterTable }</CodeBlock>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Si nous produisons le SQL pour créer la base de données et les tables à partir du modèle physique 
                    de la situation de la boutique de jouet, nous allons avoir un SQL similaire à ceci:
                </p>
                <CodeBlock language="sql">{ exemple }</CodeBlock>
                <p>
                    Voici quelques informations sur ce script:
                </p>
                <ul>
                    <li>
                        La partie <IC>DROP TABLE IF EXISTS ...</IC> n'est théoriquement pas nécessaire puisque notre 
                        script écrase la base de données à chaque fois que vous l'exécutez. Cette partie est toutefois 
                        intéressante si vous désirez exécuter votre script partiellement et uniquement écraser une 
                        table.
                    </li>
                    <li>
                        L'ordre dans lequel vous mettez vos tables dans le script peut avoir une importance dépendant 
                        de la façon dont vous ajouterez les clés étrangères. Je vous recommande donc de les mettre 
                        dans un ordre qui s'assure qu'une table ayant une clé étrangère dans une autre table devra 
                        être exécuté en premier.
                    </li>
                </ul>
                <p>
                    La situation n'est pas complète. Il va nous rester à ajouter les contraintes ainsi que les clés. 
                    Il est toutefois possible de tester votre code à partir de cette étape en l'exécutant sur votre 
                    SGBD.
                </p>
            </section>

            <section>
                <Video title="SQL - DDL - Bases de données et tables" src="https://www.youtube.com/embed/I_k9lj_Zwvk" />
            </section>
        </>;
    }
}

export default DDLTable;