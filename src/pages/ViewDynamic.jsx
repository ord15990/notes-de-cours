import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const dynamicExample = 
`CREATE VIEW product_client AS
SELECT DISTINCT c.first_name, c.last_name, p.product_name
FROM customers c
INNER JOIN orders o ON c.id = o.customer_id
INNER JOIN order_details od ON o.id = od.order_id
INNER JOIN products p ON od.product_id = p.id;`;

const selectView =
`SELECT * 
FROM product_client
WHERE first_name = 'Elizabeth';`;

const updateView =
`UPDATE order_details
SET product_id = 86, unit_price = 15.99
WHERE id = 81;`;

const viewInView = 
`CREATE VIEW part_product_client AS
SELECT last_name, product_name
FROM product_client;`

export default class ViewDynamic extends React.Component {
    render() {
        return <>
            <section>
                <h2>Données dynamiques</h2>
                <p>
                    Contrairement à un <IC>SELECT</IC> dans un <IC>INSERT</IC> qui insère les données présentement 
                    dans une table dans une autre table, les views sont toujours mise à jour. C'est-à-dire que si vous 
                    modifiez les données dans une table, celles-ci seront automatiquement mise à jour dans les vues 
                    qui utilisent cette table. Les données des vues sont donc mise à jour dynamiquement. Nous pouvons 
                    toutefois utiliser ces vues comme si c'était une table.
                </p>
                <p>
                    Il est plus facile de comprendre avec un exemple. Supposons que nous ayons la base de données
                    Northwind et que nous avons la vue suivante:
                </p>
                <CodeBlock language="sql">{ dynamicExample }</CodeBlock>
                <p>
                    Nous pouvons accéder aux données de la vue de la même façon qu'avec une table:
                </p>
                <CodeBlock language="sql">{ selectView }</CodeBlock>
                <p>
                    Si nous ajoutons une commande dans la base de données pour un certain client (donc dans la table
                    orders et order_details), nous verrons alors que les données de la vue auront changé
                    automatiquement pour refléter le nouvel achat d'un client. De la même façon, si nous supprimons ou
                    modifions une commande ou une partie d'une commande, les données seront aussi automatiquement mise 
                    à jour. 
                </p>
                <p>
                    Par exemple, si vous modifiez la table <IC>order_details</IC> de la façon ci-dessous, les 
                    données dans la vue vont s'ajuster automatiquement.
                </p>
                <CodeBlock language="sql">{ updateView }</CodeBlock>
                <p>
                    Nous pouvons faire des requêtes sur les vues de la même manière que sur des tables. Nous pouvons 
                    aussi y faire des recherches avec des conditions, des jointures, des groupes ou des sous-requêtes.
                </p>
            </section>

            <section>
                <h2>Vues dans des vues</h2>
                <p>
                    Il est possible d'utiliser des requêtes pour créer une vue qui utilise une ou plusieurs autres vues. En
                    théorie, celles-ci fonctionneront sans problèmes et retourneront toujours des résultats corrects
                    lorsque l'on exécutera des requêtes.
                </p>
                <p>
                    Par exemple, si nous utilisons la vue précédemment créée, nous pourrions créer la vue suivante sans
                    problèmes:
                </p>
                <CodeBlock language="sql">{ viewInView }</CodeBlock>
            </section>
        </>;
    }
}
