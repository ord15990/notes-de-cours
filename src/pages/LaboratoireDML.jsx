import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import distribue from '../resources/laboratoire-6-distribué.sql'
import solution from '../resources/laboratoire-6-solution.sql'

class Laboratoire5 extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <ol>
                    <li>
                        Exécuter le script <IC>distribué.sql</IC> sur votre SGBD pour créer la base de 
                        données <IC>boutique_jouet</IC>. Cette base de donnée est une version un peu simplifié de 
                        la base de données de la boutique de jouet que nous avons créée dans les modules précédants.
                    </li>
                    <li>
                        Créer un fichier SQL qui réponds à chacun des exercices ci-dessous. N'hésitez pas à 
                        ajouter plus de données ou à réinitialiser la base de données pour bien tester vos 
                        requêtes SQL.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Exercices</h2>
                <ol>
                    <li>
                        Insérer les valeurs suivantes dans la table <IC>client</IC>:
                        <table>
                            <tr><th>Nom</th><th>Prénom</th><th>Date de naissance</th></tr>
                            <tr><td>Tremblay</td><td>Simon</td><td>2001-05-18</td></tr>
                            <tr><td>Dugal</td><td>Steven</td><td>1993-08-01</td></tr>
                            <tr><td>Boucher</td><td>Bertrand</td><td>1985-12-24</td></tr>
                            <tr><td>Gagnon</td><td>Mélanie</td><td>1996-08-24</td></tr>
                            <tr><td>Laporte</td><td>Éric</td><td>1991-02-09</td></tr>
                            <tr><td>Laporte</td><td>Éric</td><td>1995-03-25</td></tr>
                            <tr><td>Giroux</td><td>Claude</td><td></td></tr>
                            <tr><td>Wayne</td><td>Bruce</td><td></td></tr>
                        </table>
                    </li>
                    <li>
                        Insérer les valeurs suivantes dans la table <IC>produit</IC>:
                        <table>
                            <tr><th>Nom</th><th>Description</th><th>Prix</th></tr>
                            <tr><td>ordinateur</td><td>Un ordinateur</td><td>500.99</td></tr>
                            <tr><td>clavier</td><td>Un clavier</td><td>56.99</td></tr>
                            <tr><td>souris</td><td>Une souris</td><td>24.99</td></tr>
                            <tr><td>écran</td><td>Un écran</td><td>210.99</td></tr>
                        </table>
                    </li>
                    <li>
                        Supprimer les clients qui ont le nom <IC>Tremblay</IC> et le prénom <IC>Simon</IC>.
                    </li>
                    <li>
                        Changer le nom et le prénom du client qui a « 3 » comme ID. Le nouveau nom est <IC>Roger</IC> et
                        le nouveau prénom est <IC>Robert</IC>.
                    </li>
                    <li>
                        Supprimer le ou les clients ayant le nom <IC>Laporte</IC> et étant née avant 1993.
                    </li>
                    <li>
                        Mettre à jour les prix de tous les produits pour les réduire de moitié.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ distribue } name="distribue.sql"></DownloadBlock.File>
                    <DownloadBlock.File path={ solution } name="solution.sql"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}

export default Laboratoire5;