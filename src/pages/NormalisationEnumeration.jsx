import React from 'react';

import enumeration from '../resources/normalisation-enumaration.png';

class PhysiqueNormalisation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Liste dans une table</h2>
                <p>
                    Les tables d'énumération sont une façon de garder toutes les valeurs possible d'un champ dans une 
                    table. On les utilisera pour les champs qui ont un nombre défini de valeurs pas trop grand, mais qui 
                    pourrait changer éventuellement. Voici quelques exemples:
                </p>
                <ul>
                    <li>Liste de tous les pays</li>
                    <li>Liste de toutes les provinces d'un pays</li>
                    <li>Liste de catégories d'entité d'une autre table</li>
                </ul>
                <p>
                    Les tables d'énumération ne font pas vraiment partie de la normalisation, mais leur traitement est
                    similaire. Nous les verrons donc ici.
                </p>
            </section>

            <section>
                <h2>Table d'énumération dans la situation d'exemple</h2>
                <p>
                    Lorsque vous avez une colonne similaire aux exemples ci-dessus dans votre modèle, il y a de fort risque 
                    de répétition. La colonne du pays où habite vos clients dans notre situation d'exemple risque fortement 
                    de contenir des doublons puisqu'il est très probable que vous aillez plus d'un clients habitant dans le 
                    même pays.
                </p>
                <p>
                    Dans ce genre de cas, nous voulons tout simplement créer une table spécifiquement pour ce champ et 
                    utiliser une clé étrangère dans la table qui utilisait cette colonne.
                </p>
                <img src={ enumeration } alt="Normalisation d'un modèle" />
                <p>
                    Dans notre situation, la table de catégorie est elle aussi une table d'énumération, mais nous l'avions 
                    déjà identifié.
                </p>
            </section>
        </>;
    }
}

export default PhysiqueNormalisation;