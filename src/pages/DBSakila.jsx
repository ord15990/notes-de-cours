import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import sakila from '../resources/db-sakila-complet.sql'
import sakilaPhysique from '../resources/db-sakila-physique.png'

export default class DBSakila extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    La base de données Sakila est une base de données fictives permettant de tester des requêtes 
                    SQL. Elle a été créé par Oracle par les développeur de MySQL dans le but d'offrir une base de 
                    données de test.
                </p>
                <p>
                    Cette base de données simule un magasin qui fait de la location de DVD (comme Blockbuster ou les 
                    Superclub Vidéotro, etc.). Bien que ce style de magasin soit disparût aujourd'hui avec les 
                    nouvelles solutions numérique, nous utilisons tout de même ce concept ailleurs, comme dans les 
                    bibliothèques. Vous trouverez le schémas de la base de données dans les téléchargements.
                </p>
            </section>

            <section>
                <h2>Installation avec MySQL Workbench</h2>
                <p>
                    Voici les étapes pour installer la base de données Sakila sur MySQL ou MariaDB avec MySQL Workbench:
                </p>
                <ol>
                    <li>
                        Télécharger les fichiers de la base de données Sakila ci-dessous.
                    </li>
                    <li>
                        Ouvrir et exécuter le script SQL <IC>sakila-complet.sql</IC> dans MySQL Workbench.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Installation avec PHPMyAdmin</h2>
                <p>
                    Voici les étapes pour installer la base de données Sakila sur MySQL ou MariaDB avec PHPMyAdmin:
                </p>
                <ol>
                    <li>
                        Télécharger les fichiers de la base de données Sakila ci-dessous.
                    </li>
                    <li>
                        Ouvrir PHPMyAdmin et aller dans l'onglet <IC>Import</IC>.
                    </li>
                    <li>
                        Dans la section <IC>File to import</IC>, sélectionner le fichier <IC>sakila-complet.sql</IC>.
                    </li>
                    <li>
                        Décocher la case <IC>Enable foriegn key checks</IC>.
                    </li>
                    <li>
                        Cliquer sur <IC>Go</IC>
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargement</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ sakila } name="sakila-complet.sql"></DownloadBlock.File>
                    <DownloadBlock.File path={ sakilaPhysique } name="sakila-physique.png"></DownloadBlock.File>
                </DownloadBlock>
            </section>

            
        </>;
    }
}
