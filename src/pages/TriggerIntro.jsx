import React from 'react';
import IC from '../component/InlineCode';
import ColoredBox from '../component/ColoredBox';

export default class TriggerIntro extends React.Component {
    render() {
        return <>
            <section>
                <h2>Description</h2>
                <p>
                    Les déclencheurs, aussi appelé <IC>TRIGGER</IC> dans le langage SQL, sont un moyen d'exécuter du 
                    code SQL lorsque des actions sont exécuté sur une table. Plus précisément, les déclencheurs 
                    permettent d'exécuter du code lorsqu'un utilisateur fait un <IC>INSERT</IC>, un <IC>UPDATE</IC> ou 
                    un <IC>DELETE</IC>.
                </p>
                <p>
                    Bien que présent dans presque toutes les bases de données SQL, les triggers ne sont pas toujours
                    utilisé. On peut en effet utiliser un système similaire aux triggers dans une application au lieu 
                    de directement dans la base de données. C'est souvent une solution un peu plus flexible que les 
                    triggers et parfois plus facilement réutilisable.
                </p>
            </section>

            <section>
                <h2>Utilisations</h2>
                <p>
                    Voici quelques situations où il est pratique d'utiliser les triggers.
                </p>
                <ul>
                    <li>
                        Pour générer ou modifier la valeur de certaines colonnes qui ont des valeurs calculées.
                    </li>
                    <li>
                        Pour ajouter des journaux d'évènements (event logs) dans une autre table de la base de données.
                    </li>
                    <li>
                        Empêcher certaines opérations sur certaines données.
                    </li>
                    <li>
                        Maintenir à jour des tables qui sont répliquées.
                    </li>
                </ul>
                <ColoredBox heading="Attention">
                    <p>
                        Il arrive parfois de voir des bases de données avec des triggers très complexes qui servent à vérifier
                        des contraintes d'intégrités complexe ou encore à créer des droit de sécurités personnalisées. Il n'est
                        pas nécessairement mauvais d'utiliser les triggers pour exécuter ce genre d'opération complexe. Il est
                        toutefois important de bien documenter ces opérations. 
                    </p>
                    <p>
                        Par exemple, pour un nouvel employé, il peut être très difficile de comprendre pourquoi 
                        l'ajout d'une rangée dans une table ajoute des données dans plusieurs autres tables.
                    </p>
                </ColoredBox>
            </section>
        </>;
    }
}
