import React from 'react';
import DownloadBlock from '../component/DownloadBlock';

import exercice1 from '../resources/physique-exercice1.png'
import exercice2 from '../resources/physique-exercice2.png'
import solution from '../resources/laboratoire-3-solution.zip'

export default class LaboratoirePhysique extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    Pour chacun des modèles entité-association ci-dessous, convertissez-le en modèle physique.
                </p>
            </section>

            <section>
                <h2>Mises en situation</h2>
                <ol>
                    <li>
                        Base de données d'un collège
                        <img src={ exercice1 } alt="Modèle entité-association d'une base de données d'un collège" />
                    </li>
                    <li>
                        Base de données d'une bibliothèque
                        <img src={ exercice2 } alt="Modèle entité-association d'une base de données d'une bibliothèque" />
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}
