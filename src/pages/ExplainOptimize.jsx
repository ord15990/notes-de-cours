import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const all =
`EXPLAIN 
SELECT o.*
FROM products p
INNER JOIN order_details od ON od.product_id = p.id
INNER JOIN orders o ON o.id = od.order_id
WHERE product_name = 'Northwind Traders Chocolate';`;

const allIndex = 
`CREATE INDEX idx_product_name 
ON products(product_name);`;

const keyNull = 
`EXPLAIN
SELECT *
FROM orders
WHERE employee_id = (
    SELECT id 
    FROM employees 
    WHERE email_address = 'andrew@northwindtraders.com'
);`;

const keyNullUnique =
`ALTER TABLE employees
ADD CONSTRAINT uq_email_address
UNIQUE(email_address)`;

const highRows =
`EXPLAIN
SELECT o.*
FROM orders o
INNER JOIN order_details od ON o.id = od.order_id
GROUP BY o.id
HAVING SUM(od.quantity * od.unit_price) = (
    SELECT MAX(prix)
    FROM (
        SELECT SUM(od.quantity * od.unit_price) AS prix
        FROM orders o
        INNER JOIN order_details od ON o.id = od.order_id
        GROUP BY o.id
    ) p
);`;

const highRowsLess = 
`EXPLAIN
SELECT o.*
FROM orders o
INNER JOIN order_details od ON o.id = od.order_id
GROUP BY o.id
HAVING SUM(od.quantity * od.unit_price) = (
    SELECT SUM(od.quantity * od.unit_price) AS prix
	FROM order_details od
	GROUP BY od.order_id
	ORDER BY prix DESC
	LIMIT 1
);`;

const highRowsEvenLess = 
`EXPLAIN
SELECT o.*
FROM orders o
INNER JOIN order_details od ON o.id = od.order_id
GROUP BY o.id
ORDER BY SUM(od.quantity * od.unit_price) DESC
LIMIT 1;`;

export default class ExplainOptimize extends React.Component {
    render() {
        return <>
            <section>
                <h2>Cas de problèmes</h2>
                <p>
                    Il est important de comprendre les bases de l'instruction <IC>EXPLAIN</IC>. En effet, l'analyse de 
                    ses résultats nous permettra de détecter les problèmes de performance pour pouvoir les corrigés. 
                    Cette compétance est très pratique pour ceux d'entre vous qui sont des futurs gestionnaires de 
                    base de données.
                </p>
                <p>
                    En général, nous avons quelques indicateurs facile pouvant nous montrer que notre requête n'est pas
                    performante. Si un de ces cas vous arrive, vous pouvez surement optimiser vos requêtes ou ajouter
                    des index pour corriger ces problèmes de performance.
                </p>
                <p>
                    Cette page vous fera la démonstration de certains cas de problèmes typique facilement corrigeable.
                </p>
            </section>

            <section>
                <h2>Type de jointure <IC>ALL</IC></h2>
                <p>
                    Vous avez un type de jointure qui est <IC>all</IC> pour une certaine table. Ce qui veut dire que 
                    chaque rangée de la table doit être visité individuellement. On appelle aussi ce type de recherche 
                    un table scan dû à son comportement. Ce type de jointure est le pire et peut causé de longs temps 
                    d'attente si votre base de données contient beaucoup de données. Ça indique souvent qu'il manque 
                    un index pour accélérer la recherche.
                </p>
                <p>
                    Voici un exemple avec Northwind. Cette requête retourne toutes les commandes contenant le produit 
                    ayant pour nom <IC>Northwind Traders Chocolate</IC>:
                </p>
                <CodeBlock language="sql">{ all }</CodeBlock>
                <p>
                    Dans le résultat, nous voyons que la table employé est de type <IC>all</IC>. La valeur de 
                    la colonne <IC>rows</IC> est 58, soit le nombre de <IC>order_details</IC>. MySQL essait 
                    d'optimiser notre requête, ce qui explique pourquoi il boucle sur 
                    les <IC>order_details</IC> plutôt que sur les <IC>products</IC>. Si nous avions plusieurs 
                    millions de commandes ou de produits, cette requête serait désastreuse.
                </p>
                <p>
                    Dans ce cas-ci, le principal problème est le <IC>product_name</IC>. MySQL n'a aucune façon 
                    d'accélérer la recherche sur ce champ. L'ajout d'un index sur le <IC>product_name</IC> 
                    règle toutefois le problème.
                </p>
                <CodeBlock language="sql">{ allIndex }</CodeBlock>
                <p>
                    Si vous relancer le <IC>EXPLAIN</IC>, vous verrez que l'utilisation de l'index permet 
                    d'accélérer énormément votre requête. La valeur de la colonne <IC>rows</IC> devrait maintenant 
                    tomber à 1.
                </p>
            </section>

            <section>
                <h2>Aucune clé utilisé</h2>
                <p>
                    Vous avez une table qui n'utilise pas de clé dans sa requête, sa valeur de la colonne <IC>key</IC> est 
                    donc <IC>NULL</IC>. Cela indique souvent qu'il manque un index, une <IC>PRIMARY KEY</IC>, 
                    une <IC>FORIEGN KEY</IC> ou une contrainte <IC>UNIQUE</IC>.
                </p>
                <p>
                    Voici un exemple avec Northwind. Cette requête va chercher totues les commandes faites par 
                    l'employé ayant l'adresse courriel <IC>andrew@northwindtraders.com</IC>:
                </p>
                <CodeBlock language="sql">{ keyNull }</CodeBlock>
                <p>
                    Dans le résultat, nous voyons que la sous-requête n'utilise pas de clé, ce qui oblige un table
                    scan. Effectivement, ce problème sera souvent accompagné d'un type de jointure <IC>all</IC>. 
                </p>
                <p>
                    Dans cet exemple, l'ajout d'une contrainte <IC>UNIQUE</IC> ou d'un index sur la 
                    colonne <IC>email_address</IC> accélèrerait beaucoup la requête.
                </p>
                <CodeBlock language="sql">{ keyNullUnique }</CodeBlock>
                <p>
                    Si vous relancer le <IC>EXPLAIN</IC> après avoir ajouté la contrainte <IC>UNIQUE</IC>, vous verrez 
                    que l'utilisation de cette clé permet d'accélérer énormément votre requête. La valeur de la 
                    colonne <IC>rows</IC> pour la sous-requête devrait maintenant tomber à 1.
                </p>
            </section>

            <section>
                <h2>Visite de rangées élevée</h2>
                <p>
                    Le nombre total de <IC>rows</IC>, donc la multiplication des colonnes <IC>rows</IC> pour chaque 
                    table, est très élevé. Nous avons donc plusieurs valeurs de <IC>rows</IC> très élevé. Cela indique 
                    souvent que notre requête n'est pas écrit de manière performante ou qu'il nous manque plusieurs 
                    index, clés ou contraintes.
                </p>
                <p>
                    Voici un exemple avec Northwind. Cette requête sort les commandes qui ont coûté le plus cher:
                </p>
                <CodeBlock language="sql">{ highRows }</CodeBlock>
                <p>
                    Le total de rangées visitées approximatif est 
                    de <IC>58&nbsp;&times;&nbsp;1&nbsp;&times;&nbsp;58&nbsp;&times;&nbsp;58&nbsp;&times;&nbsp;1&nbsp;=&nbsp;195112</IC>. 
                    Ce genre de cas arrive souvent lorsqu'on utilise des sous-requêtes complexes. Parfois, nous 
                    n'avons pas d'autres solutions possibles et nous devons simplement essayé d'optimiser le plus 
                    chaque partie de la requête. 
                </p>
                <p>
                    Dans ce cas-ci, nous pouvons définitivement améliorer notre requête ou sinon en écrire une 
                    similaire beaucoup plus performante. Vous 2 réécritures plus performante:
                </p>
                <CodeBlock language="sql">{ highRowsLess }</CodeBlock>
                <CodeBlock language="sql">{ highRowsEvenLess }</CodeBlock>
                <p>
                    Le total de rangées visitées approximatif est de <IC>58&nbsp;&times;&nbsp;1&nbsp;=&nbsp;58</IC>. 
                    Comme vous pouvez le constater, une simple réécriture de la requête peut faire une très grande 
                    différence.
                </p>
                <p>
                    Malheureusement, dans les 2 cas ci-dessus, nous ne pouvons pas empêcher un table scan. Ceci est dû 
                    au triage sur une agrégation de calcul arithmétique sur 2 colonnes. Bref, 
                    le <IC>SUM(od.quantity * od.unit_price)</IC> n'aide pas notre situation. Dans ce cas-ci, un index 
                    ne serait malheureusement pas utile non plus. Il existe d'autres optimisations, mais elles sont 
                    trop avancé pour ce cours. Nous nous satisferons donc de ce résultat pour le moment.
                </p>
            </section>
        </>;
    }
}
