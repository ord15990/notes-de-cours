import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import resto from '../resources/db-resto.sql'
import restoPhysique from '../resources/db-resto-physique.png'

export default class DBResto extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    La base de données Resto est une base de données fictives permettant de tester des requêtes 
                    SQL. Cette base de données simule un restaurant où chaque commande est entrée dans un système 
                    informatique. Vous trouverez les schémas de la base de données dans les téléchargements.
                </p>
            </section>

            <section>
                <h2>Installation</h2>
                <p>
                    Voici les étapes pour installer la base de données Resto sur MySQL ou MariaDB:
                </p>
                <ol>
                    <li>
                        Télécharger le fichier de la base de données Resto ci-dessous.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>resto.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargement</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ resto } name="resto.sql"></DownloadBlock.File>
                    <DownloadBlock.File path={ restoPhysique } name="resto-physique.png"></DownloadBlock.File>
                </DownloadBlock>
            </section>

            
        </>;
    }
}
