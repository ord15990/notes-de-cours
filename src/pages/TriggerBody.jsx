import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const triggerInsert = 
`DELIMITER //

CREATE TRIGGER nom_trigger
AFTER INSERT ON nom_table FOR EACH ROW
BEGIN
    INSERT INTO nom_table_log (operation, colonne)
    VALUES ('INSERT', NEW.colonne);
END//

DELIMITER ;`;

const triggerDelete = 
`DELIMITER //

CREATE TRIGGER nom_trigger
AFTER DELETE ON nom_table FOR EACH ROW
BEGIN
    INSERT INTO nom_table_log (operation, colonne)
    VALUES ('DELETE', OLD.colonne);
END//

DELIMITER ;`;

const triggerUpdate =
`DELIMITER //

CREATE TRIGGER nom_trigger
AFTER UPDATE ON nom_table FOR EACH ROW
BEGIN
    INSERT INTO nom_table_log (operation, colonneAvant, colonneApres)
    VALUES ('UPDATE', OLD.colonne, NEW.colonne);
END//

DELIMITER ;`;

const set = 
`DELIMITER //

CREATE TRIGGER nom_trigger
BEFORE INSERT ON nom_table FOR EACH ROW
BEGIN
    SET NEW.calc = NEW.v1 + NEW.v2;
END//

DELIMITER ;`;

const preventDelete =
`DELIMITER //

CREATE TRIGGER prevenir_delete
BEFORE DELETE ON nom_table FOR EACH ROW
BEGIN
    IF OLD.id = 0 THEN
        SET @message_text = CONCAT(
            'La rangée ayant l''id ', 
            OLD.id, 
            ' ne peut être supprimée.'
        );
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = @message_text;
    END IF;
END//

DELIMITER ;`;

export default class TriggerBody extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Dans votre déclencheur, à l'intérieur du <IC>BEGIN</IC> et du <IC>END</IC> nous trouverons le code 
                    SQL à exécuter lorsque le déclencheur est lancé. Ce code SQL est différent du code SQL standard 
                    auquel nous sommes habitué. En effet, vous pouvez trouver ici des conditions <IC>IF</IC> et <IC>else</IC>,
                    des boucles <IC>WHILE</IC> et même la déclaration de variables.
                </p>
                <p>
                    Dans cette page, nous ne verrons pas comment faire ce genre d'opération. Je suppose que vous 
                    connaissez déjà les bases des langages de programmation et au besoin, vous êtes capable d'utiliser 
                    un engin de recherche pour vous aider.
                </p>
                <p>
                    Cette page traitera toutefois des fonctionnalités spéciales que vous pouvez faire à l'intérieur de 
                    ce SQL de programmation, soit l'utilisation des variables <IC>NEW</IC> et <IC>OLD</IC> ainsi que 
                    l'opérateur <IC>SET</IC>.
                </p>
            </section>

            <section>
                <h2>Variable <IC>NEW</IC> et <IC>OLD</IC></h2>
                <p>
                    Entre le <IC>BEGIN</IC> et le <IC>END</IC> (dans le code à exécuté du déclencheur), vous aurez accès à 
                    deux variables qui vous seront très utile: <IC>NEW</IC> et <IC>OLD</IC>. Ces deux variables seront 
                    utilisé de manière un peu différente dépendant de l'opération sur laquelle le déclencheur est 
                    liée.
                </p>
                <ul>
                    <li>
                        <IC>INSERT</IC>
                        <p>
                            Le mot-clé <IC>NEW</IC> nous permet d'aller chercher les valeurs de la rangée qui est ajouté.
                        </p>
                        <p>
                            Le mot-clé <IC>OLD</IC> ne sert à rien dans un trigger de <IC>INSERT</IC>.
                        </p>
                        <CodeBlock language="sql">{ triggerInsert }</CodeBlock>
                    </li>
                    <li>
                        <IC>DELETE</IC>
                        <p>
                            Le mot-clé <IC>NEW</IC> ne sert à rien dans un trigger de <IC>DELETE</IC>.
                        </p>
                        <p>
                            Le mot-clé <IC>OLD</IC> nous permet d'aller chercher les valeurs de la rangée qui est supprimée.
                        </p>
                        <CodeBlock language="sql">{ triggerDelete }</CodeBlock>
                    </li>
                    <li>
                        <IC>UPDATE</IC>
                        <p>
                            Le mot-clé <IC>NEW</IC> nous permet d'aller chercher les nouvelles valeurs de la rangée qui est modifié.
                        </p>
                        <p>
                            Le mot-clé <IC>OLD</IC> nous permet d'aller chercher les anciennes valeurs de la rangée qui est modifiée.
                        </p>
                        <CodeBlock language="sql">{ triggerUpdate }</CodeBlock>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Opérateur <IC>SET</IC></h2>
                <p>
                    Entre le <IC>BEGIN</IC> et le <IC>END</IC> d'un déclencheur sur le <IC>INSERT</IC> ou 
                    le <IC>UPDATE</IC>, vous aurez accès à l'opérateur <IC>SET</IC>. Cet opérateur sert à modifier les 
                    valeurs d'une rangée avant de l'insérer ou de la modifier. On utilise donc le <IC>SET</IC> avec la 
                    variable <IC>NEW</IC>. Il n'est pas possible d'utiliser <IC>SET</IC> avec la 
                    variable <IC>OLD</IC> puisque celle-ci est en lecture seulement.
                </p>
                <CodeBlock language="sql">{ set }</CodeBlock>
            </section>

            <section>
                <h2>Exemple complet</h2>
                <p>
                    Un des déclencheurs très pratique est celui qui empêche la suppression de certaines données dans 
                    une table. Il est pratique si vous voulez être certain de ne pas supprimer certaines valeurs ou 
                    simplement si vous voulez empêcher la suppression sur une table.
                </p>
                <CodeBlock language="sql">{ preventDelete }</CodeBlock>
                <p>
                    Ici, on prévient la suppression lorsqu'un utilisateur essai de supprimer la rangée ayant le id 0. 
                    En générant une erreur avec le mot-clé <IC>SIGNAL</IC>, le déclencheur arrête à cause de l'erreur, 
                    ce qui force l'opération liée au déclencheur, donc la suppression, à ne pas s'exécuter. 
                </p>
                <p>
                    Ce comportement est possible puisque le déclencheur est exécuté avant (<IC>BEFORE</IC>) la 
                    suppression.
                </p>
            </section>
        </>;
    }
}
