import React from 'react';
import Video from '../component/Video';
import ColoredBox from '../component/ColoredBox';

import step4 from '../resources/ea-step4.png'

class EAIdentifiant extends React.Component {
    render() {
        return <>
            <section>
                <h2>Définir les identifiants</h2>
                <p>
                    Cette étape est probablement la plus facile. L’identifiant est un attribut de l’entité qui la défini. Cet
                    identifiant devrait être unique pour chaque exemplaire de l’entité. Il faut toutefois parfois faire un peu
                    de recherche pour connaître l’identifiant d’une entité. Voici quelques exemples d’identifiants en
                    fonction de son entité:
                </p>
                <ul>
                    <li>Voiture : VIN (Vehicule Identification Number)</li>
                    <li>Livre : ISBN</li>
                </ul>
                <p>
                    Il arrive fréquement que des entités n’aient pas clairement d’identifiant. Lorsque cela arrive, nous lui
                    inventerons simplement un identifiant. Généralement, ce sera simplement un nombre entier. Dans
                    notre situation, toutes les entités n’ont pas d’identifiant clair. Nous créerons donc un identifiant pour
                    chacun d’entre eux.
                </p>
                <ColoredBox heading="À noter">
                    Dans certains cas, il est possible que l’identifiant d’une entité soit une combinaison d’attributs. Dans
                    ces cas, cela veut dire que l’entité est identifié par un ensemble d'attributs.
                </ColoredBox>
            </section>

            <section>
                <h2>Déssiner les identifiants</h2>
                <p>
                    Pour mettre les identifiants dans le modèle, nous écrirons simplement les identifiants sous le nom de l'entité 
                    qu'il représente et nous le <strong>soulignerons</strong>. Il est important de souligner le nom de l’identifiant 
                    puisque celui-ci se trouvera dans la même boîte que tous les autre attributs de l’entité. Ce soulignement indique 
                    donc que l’attribut est l'identifiant de l'entité.
                </p>
                <img src={ step4 } alt="Déssiner les identifiants" />
                <p>
                    Le nom donné à l’identifiant pourrait être différent, mais il doit être clair. Lorsque nous devons créer
                    un identifiant, je suggère généralement de simplement le nommer avec «&nbsp;ID&nbsp;» suivi du nom de l’entité.
                </p>
                <ColoredBox heading="À noter">
                    Si vous faites face à une situation qui demande une combinaison d’attribut comme identifiant, vous avez simplement 
                    à les mettre un en dessous de l'autre et à tous les souligner.
                </ColoredBox>
            </section>

            <section>
                <Video title="Modèle entité-association - Identifiants" src="https://www.youtube.com/embed/L09HpidGUTE" />
            </section>
        </>;
    }
}

export default EAIdentifiant;