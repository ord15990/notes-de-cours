import React from 'react';
import IC from '../component/InlineCode';

import subIntro from '../resources/sub-intro.png';

export default class SubIntro extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Les sous requêtes sont un moyen d'exécuter des requêtes SQL à l'intérieur d'une autre requête SQL.
                    Elles permettent d'exécuter certaines requêtes qui seraient normalement impossible seulement avec
                    les jointures et les groupes. Toutefois, les sous-requêtes sont généralement beaucoup plus lente à
                    exécuter.
                </p>
                <p>
                    Les sous-requêtes sont généralement placé dans:
                </p>
                <ul>
                    <li>Une condition comme un <IC>WHERE</IC> ou un <IC>HAVING</IC></li>
                    <li>Un <IC>INSERT</IC> comme données à ajouter</li>
                    <li>Un <IC>UPDATE</IC> comme données à modifier</li>
                    <li>Une table à joindre à la requête dans un <IC>FROM</IC> ou un <IC>JOIN</IC></li>
                </ul>
                <p>
                    Il est possible de voir des sous-requêtes à d'autres endroits, mais cela est plutôt rare.
                </p>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Dans ce module, nous utiliserons la base de données Northwind pour faire nos exemples. Vous
                    pouvez trouver les fichiers SQL et le schémas physique de cette base de données dans la section 
                    des bases de données de ce site Web.
                </p>
            </section>

            <section>
                <h2>Fonctionnement</h2>
                <p>
                    Les sous requêtes sont exécuté dans leur ordre de profondeur. Il est important de comprendre que
                    c'est la sous requête la plus profonde qui est exécuté en premier jusqu'à la requête principale. Une
                    façon facile de le voir est de se dire que les requêtes supérieures ont besoin des requêtes inférieures
                    pour s'exécuter.
                </p>
                <img src={ subIntro } alt="Fonctionnement des sous-requêtes"/>
                <p>
                    Dans la requête fictive ci-dessus, voici l'ordre d'exécution des requêtes
                </p>
                <ol>
                    <li>Requête imbriquée 3</li>
                    <li>Requête imbriquée 1</li>
                    <li>Requête imbriquée 2</li>
                    <li>Requête principale</li>
                </ol>
            </section>
        </>;
    }
}
