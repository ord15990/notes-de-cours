import React from 'react';
import Video from '../component/Video';
import IC from '../component/InlineCode';
import ColoredBox from '../component/ColoredBox';

import step1 from '../resources/physique-step1.png'

class PhysiqueTransformation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Terminologie</h2>
                <p>
                    Les étapes 1, 2 et 3 du processus de création du modèle physique sont très simple. Elle nécessite
                    seulement beaucoup de copier-coller ou de retranscription. Essentiellement, nous copions presque tel
                    quel toutes les entités, tous les attributs ainsi que tous les identifiants. Bien que les termes 
                    changent dans le diagramme physique, la base reste la même. Voici une brève comparaison des termes 
                    entre le modèle entité-association et le modèle physique:
                </p>
                <table>
                    <tr><th>Modèle entité-association</th><th>Modèle physique</th></tr>
                    <tr><td>entité</td><td>table</td></tr>
                    <tr><td>attribut</td><td>colonne</td></tr>
                    <tr><td>identifiant</td><td>clé primaire</td></tr>
                </table>
            </section>

            <section>
                <h2>Dénomination</h2>
                <p>
                    Puisque le modèle physique est notre plan final, nous voulons nous assurer que les noms utilisés ne
                    causeront pas de problèmes dans le SGBD. Par conséquant, nous changerons tous les noms d’entités et
                    d’attributs en <IC>snake_case</IC> minuscule.
                </p>
                <ColoredBox heading="Rappel">
                    Le <IC>snake_case</IC> minuscule est, comme son nom l'indique, complètement en minuscule. Les 
                    caractères accentués sont aussi retiré et changé pour leur version non accentuée. Finalement, tous 
                    les espaces seront remplacés par des barres de soulignement (<IC>_</IC>).
                </ColoredBox>
                <p>
                    Dans l’industrie, les standards pour les noms peuvent changer d’un milieu de travail à l’autre. De
                    plus, ce n’est pas tous les SGBD qui ont les mêmes standard ou limitation. Toutefois, bien que la
                    façon d’écrire les noms dans le modèle physique peut être variée dans l’industrie, dans le cadre de ce
                    cours, vous devrez absolument utiliser le <IC>snake_case</IC>.
                </p>
            </section>

            <section>
                <h2>Clé primaire</h2>
                <p>
                    Un autre élément important est la notation des clés primaires. Dans le modèle entité-association,
                    pour représenter un identifiant, nous soulignons simplement son nom. Dans le modèle physique, il y a
                    deux façon de représenté les clés primaires:
                </p>
                <ul>
                    <li>Le soulignement de son nom (comme dans le modèle entité-association)</li>
                    <li>Les lettres <IC>PK</IC> (Primary Key) à la gauche du nom de l'identifiant</li>
                </ul>
                <p>
                    Vous pouvez choisir selon vos préférences la méthode de représentation que vous voulez.
                </p>
            </section>

            <section>
                <h2>Déssiner les transformations</h2>
                <p>
                    Après toutes ces transformation, nous avons finalement le début de notre modèle physique. Dans
                    notre situation d’exemple, à la fin des étapes 1, 2 et 3, nous avons l’ébauche de modèle physique
                    suivant:
                </p>
                <img src={ step1 } alt="Étape 1, 2 et 3 de la conversion en modèle physique" />
                <p>
                    Comme vous pouvez le constater, à part les noms des entités et des attributs, rien n’a vraiment
                    changé.
                </p>
            </section>

            <section>
                <Video title="Modèle physique - Transformation des entités, identifiants et attributs" src="https://www.youtube.com/embed/4ZvdXOlE9p4" />
            </section>
        </>;
    }
}

export default PhysiqueTransformation;