import React from 'react';
import Video from '../component/Video';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'
import ColoredBox from '../component/ColoredBox'

import boucle from '../resources/ddl-boucle.png'
import referetialIntegrity from '../resources/ddl-referential-integrity.png'

const primaryKey = 
`CREATE TABLE compte (
    id_compte INT,

    -- Ajouter la contrainte PRIMARY KEY
    CONSTRAINT PRIMARY KEY(id_compte)
);`;

const primaryKeyCombine = 
`CREATE TABLE compte_jeu (
    id_compte INT,
    id_jeu INT,

    -- Ajouter la contrainte PRIMARY KEY
    CONSTRAINT PRIMARY KEY(id_compte, id_jeu)
);`;

const autoIncrement = 
`CREATE TABLE compte (
    id_compte INT AUTO_INCREMENT,

    -- Ajouter la contrainte PRIMARY KEY
    CONSTRAINT PRIMARY KEY(id_compte)
);`;

const foreignKey = 
`ALTER TABLE nom_table
    ADD CONSTRAINT fk_colonne1
    FOREIGN KEY(colonne1)
    REFERENCES table(colonne1);`;

const foreignKeyExample = 
`CREATE TABLE compte (
    id_compte INT AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY(id_compte)
    -- ...
);

CREATE TABLE post (
    id_post INT AUTO_INCREMENT,
    id_compte INT,
    CONSTRAINT PRIMARY KEY(id_post)
    -- ...
);

ALTER TABLE post
    ADD CONSTRAINT fk_post_compte
    FOREIGN KEY(id_compte)
    REFERENCES compte(id_compte);`;

const integrity = 
`ALTER TABLE post
    ADD CONSTRAINT fk_post_compte
    FOREIGN KEY(id_compte)
    REFERENCES compte(id_compte)
    ON DELETE SET NULL
    ON UPDATE CASCADE;`;

const exemple = 
`-- Créer la base de données
DROP DATABASE IF EXISTS boutique_jouet;
CREATE DATABASE boutique_jouet 
CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

-- Utiliser la base de données pour le reste des opérations
USE boutique_jouet;

-- Créer la table client
DROP TABLE IF EXISTS client;
CREATE TABLE client(
    id_client INT,
    prenom VARCHAR(50) NOT NULL,
    nom VARCHAR(50) NOT NULL,
    adresse VARCHAR(100) NOT NULL,
    ville VARCHAR(50) NOT NULL,
    province VARCHAR(50) NOT NULL,
    pays VARCHAR(50) NOT NULL,
    CONSTRAINT PRIMARY KEY(id_client)
);

-- Créer la table commande
DROP TABLE IF EXISTS commande;
CREATE TABLE commande(
    id_commande INT,
    id_client INT NOT NULL,
    prix_total FLOAT,
    date DATE,
    CONSTRAINT PRIMARY KEY(id_commande),
    CONSTRAINT prix_total CHECK(
        prix_total >= 0
    )
);

-- Créer la table categorie
DROP TABLE IF EXISTS categorie;
CREATE TABLE categorie(
    id_categorie INT,
    nom_categorie VARCHAR(50) NOT NULL,
    CONSTRAINT PRIMARY KEY(id_categorie)
);

-- Créer la table produit
DROP TABLE IF EXISTS produit;
CREATE TABLE produit(
    id_produit INT,
    id_categorie INT ,
    nom VARCHAR(100) NOT NULL,
    prix FLOAT,
    CONSTRAINT PRIMARY KEY(id_produit),
    CONSTRAINT ck_prix CHECK(
        prix >= 0
    )
);

-- Créer la table commande_produit
DROP TABLE IF EXISTS commande_produit;
CREATE TABLE commande_produit(
    id_commande INT,
    id_produit INT,
    quantite INT NOT NULL DEFAULT 1,
    prix_total_produit FLOAT,
    CONSTRAINT PRIMARY KEY(id_commande, id_produit),
    CONSTRAINT ck_prix_total_produit CHECK(
        prix_total_produit >= 0
    )
);

-- Ajouter les clés étrangères
ALTER TABLE commande
    ADD CONSTRAINT fk_commande_client
    FOREIGN KEY(id_client)
    REFERENCES client(id_client);
    
ALTER TABLE produit
    ADD CONSTRAINT fk_produit_categorie
    FOREIGN KEY(id_categorie)
    REFERENCES categorie(id_categorie);

ALTER TABLE commande_produit
    ADD CONSTRAINT fk_commande_produit_commande
    FOREIGN KEY(id_commande)
    REFERENCES commande(id_commande);

ALTER TABLE commande_produit
    ADD CONSTRAINT fk_commande_produit_produit
    FOREIGN KEY(id_produit)
    REFERENCES produit(id_produit);

-- Ajouter les index
CREATE INDEX idx_nom_produit ON produit(nom);`;

class DDLCle extends React.Component {
    render() {
        return <>
            <section>
                <h2>Clé primaire</h2>
                <p>
                    La clé primaire (primary key) est l'identifiant de notre table. Dans un SGBD relationnel, ce 
                    concept est implémenté sous la forme d'une contrainte. La contrainte de clé primaire s'ajoute de 
                    la même façon que les contraintes <IC>UNIQUE</IC> et <IC>CHECK</IC>:
                </p>
                <CodeBlock language="sql">{ primaryKey }</CodeBlock>
                <p>
                    Pour les tables de jonction, il est possible de créer une clé primaire combinée de la façon 
                    suivante:
                </p>
                <CodeBlock language="sql">{ primaryKeyCombine }</CodeBlock>
                <ColoredBox heading="Attention">
                    La contrainte <IC>PRIMARY KEY</IC> est une combinaison des contraintes <IC>UNIQUE</IC> et <IC>NOT NULL</IC>.
                    Vous n'avez donc pas à ajouter ces contraintes en plus de celle de votre clé primaire.
                </ColoredBox>
            </section>

            <section>
                <h2>Incrémentation automatique</h2>
                <p>
                    Si vous avez une clé primaire (identifiant) créé par vous-même, vous voulez utiliser 
                    l'auto-incrémentation. Cette fonctionnalité des SGBD permet de donner une valeur à une colonne 
                    automatiquement sans avoir besoin de la spécifié. Vous pouvez l'ajouter de la façon suivante:
                </p>
                <CodeBlock language="sql">{ autoIncrement }</CodeBlock>
                <ColoredBox heading="Attention">
                    Assurez-vous de mettre cette propriété uniquement sur les clés primaires qui ne sont pas aussi des 
                    clés étrangères. On ne doit donc pas ajouter cette propriété aux tables de jonction.
                </ColoredBox>
            </section>

            <section>
                <h2>Clé étrangère</h2>
                <p>
                    Les clés étrangères (foreign key) nous permet de définir un lien entre 2 tables. Dans un SGBD 
                    relationnel, ce concept est aussi implémenté sous la forme de contraintes. Nous pouvons ajouter 
                    cette contrainte de la même façon que la clé primaire, mais cela nous causera parfois des 
                    problèmes s'il y a des cycles dans notre modèle.
                </p>
                <img src={ boucle } alt="Boucle dans un modèle physique"/>
                <p>
                    Je vous recommande donc d'ajouter les clés étrangères après avoir créé toutes vos tables de la 
                    façon suivante:
                </p>
                <CodeBlock language="sql">{ foreignKey }</CodeBlock>
                <p>
                    Voici un exemple un peu plus complet:
                </p>
                <CodeBlock language="sql">{ foreignKeyExample }</CodeBlock>
            </section>

            <section>
                <h2>Intégrité référentielle</h2>
                <p>
                    La gestion des liens et des clés étrangères apporte un problème intéressant. Que ce passe-t-il si 
                    nous supprimons des données ou modifions l'identifiant de données qui ont un lien avec d'autres 
                    données dans une autre table? Cette gestion s'appelle l'intégrité référentielle.
                </p>
                <img src={ referetialIntegrity } alt="Intégrité référentielle"/>
                <p>
                    MySQL permet 4 options que l'on peut faire lors d'une situation comme expliqué ci-dessus:
                </p>
                <dl>
                    <dt><IC>RESTRICT</IC></dt>
                    <dd>
                        Empêche l’utilisateur de faire l’action (UPDATE ou DELETE) qui causait le problème. C’est ce qui est utilisé
                        par défaut si rien n’est spécifié.
                    </dd>
                    <dt><IC>SET NULL</IC></dt>
                    <dd>Met le champ de la clé étrangère à NULL.</dd>
                    <dt><IC>SET DEFAULT</IC></dt>
                    <dd>Met le champ de la clé étrangère à sa valeur par défaut</dd>
                    <dt><IC>CASCADE</IC></dt>
                    <dd>
                        Supprime ou mets à jour toutes les rangées affectées par le changement. Si les rangées 
                        supprimées ou modifiées ont eux-aussi des liens en <IC>CASCADE</IC>, cela peut procoquer 
                        beaucoup de changements dans votre base de données.
                    </dd>
                </dl>
                <p>
                    Vous pouvez spécifier une intégrité référentielle autre que celle par défaut lorsque vous ajoutez 
                    vos clés étrangères:
                </p>
                <CodeBlock language="sql">{ integrity }</CodeBlock>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Dans note situation d'exemple, il va falloir ajouter les clés primaires ainsi que les clés 
                    étrangères. Nous pouvons aussi spécifié que nous voulons faire cascade lors de la mise à jour pour 
                    l'intégrité référentielle. Notre code SQL final pour créer la base de données ressemblerait à ceci:
                </p>
                <CodeBlock language="sql">{ exemple }</CodeBlock>
            </section>

            <section>
                <Video title="SQL - DDL - Clés primaires et étrangères" src="https://www.youtube.com/embed/Pj63KMSOkQg" />
            </section>
        </>;
    }
}

export default DDLCle;