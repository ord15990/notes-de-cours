import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import solution from '../resources/laboratoire-9-solution.sql'

export default class LaboratoireGroup extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <ol>
                    <li>
                        Télécharger les fichiers de la base de données Northwind dans la section des bases de 
                        données de ce site Web.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind-data.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Créer un fichier SQL qui réponds à chacun des exercices ci-dessous.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Exercices</h2>
                <ol>
                    <li>
                        Sélectionner la date de la plus vieille commande (orders).
                    </li>
                    <li>
                        Sélectionner la date de la plus récente commande (orders) n'étant pas livré dans la ville de 
                        Salt Lake City.
                    </li>
                    <li>
                        Sélectionner le nombre de produit (products) ayant une valeur de <IC>quantity_per_unit</IC>.
                    </li>
                    <li>
                        Sélectionner le nombre de produit (products) n'ayant pas de valeur de <IC>minimum_reorder_quantity</IC>.
                    </li>
                    <li>
                        Sélectionner le coût standard moyen de tous les produits (products).
                    </li>
                    <li>
                        Sélectionner le nombre de commandes (orders) faite par le client ayant l'identifiant <IC>8</IC>
                    </li>
                    <li>
                        Sélectionner la somme des coûts standards de tous les produits (products) dont le prix est inférieur à 10.
                    </li>
                    <li>
                        Sélectionner la somme et la moyenne des coûts standards de tous les produits (products) avec un alias pour que les résultats
                        retournés soit plus beau.
                    </li>
                    <li>
                        Sélectionner le nombre de commandes (orders) qui ont été faite durant le mois d'avril 2006.
                    </li>
                    <li>
                        Sélectionner la somme des coûts standards des produits (products) ayant le mot <IC>mix</IC> dans son nom.
                    </li>
                    <li>
                        Sélectionner le coût standard du produit le moins cher parmi les produits dont le nom se termine par la 
                        lettre <IC>a</IC>.
                    </li>
                    <li>
                        Sélectionner le nombre total de produits vendus (regarder dans la table order_details).
                    </li>
                    <li>
                        Sélectionner le nombre de commande (orders) préparée par l'employé (employees) ayant
                        l'adresse courriel <IC>nancy@northwindtraders.com</IC>.
                    </li>
                    <li>
                        Sélectionner le nombre d'employés (employees) par ville.
                    </li>
                    <li>
                        Sélectionner le prénom, le nom ainsi que le nombre de commande (orders) faite par chaque employé (employees).
                    </li>
                    <li>
                        Sélectionner le prix de livraison le plus élevé qui a été payé pour chaque ville dans lesquelles
                        des commandes (orders) ont été livrées.
                    </li>
                    <li>
                        Sélectionner le nombre de produits (products) par catégorie de produits dont le coût standard
                        est inférieur ou égal à 20$.
                    </li>
                    <li>
                        Sélectionner le nombre de produits (products) vendu pour chaque commande (orders et
                        order_details) dont la quantité totale de produits est inférieur à 100.
                    </li>
                    <li>
                        Sélectionner le nom, prénom ainsi que le total de produits (products) acheté par chaque client
                        (customers) et ordonner le résultat en ordre du plus grand nombre de produits achetés vers le
                        plus petit nombre de produits achetés.
                    </li>
                    <li>
                        Sélectionner le prix total de chaque commande (orders et order_details) sans taxe et sans les
                        frais de livraison.
                    </li>
                    <li>
                        Sélectionner le nom et prénom d'un des employés (employees) qui a vendu la plus grosse
                        commande (order et order_details) en terme de prix sans taxe et sans frais de livraison.
                    </li>
                    <li>
                        Sélectionner le top 5 des meilleures commandes qui ont fait le plus de profit. On veut retourner
                        le nom et prénom de l'employé qui a fait la commande, le id de la commande ainsi que le
                        profit (prix total de la commande moins le prix de l'achat des produits) fait avec la commande.
                    </li>
                    <li>
                        Sélectionner tous les clients (customers) qui ont passé au moins de 4 commandes (orders).
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.sql"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}
