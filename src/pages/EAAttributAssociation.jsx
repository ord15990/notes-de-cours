import React from 'react';
import Video from '../component/Video';
import ColoredBox from '../component/ColoredBox';

import step6 from '../resources/ea-step6.png'

class EAAttributAssociation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Types d'associations</h2>
                <p>
                    Il arrive parfois que des associations ont elles aussi des attributs. Toutefois, ce ne sont pas toutes les
                    associations qui peuvent recevoir des attributs. Il est donc bien important de comprendre les trois
                    types d’associations possibles :
                </p>
                <table>
                    <tr>
                        <th>Type</th><th>Nom</th><th>Description</th>
                    </tr>
                    <tr>
                        <td>1 vers 1 (one to one)</td>
                        <td>Binaire</td>
                        <td>La cardinalité maximale sur chaque côté de la relation est 1.</td>
                    </tr>
                    <tr>
                        <td>1 vers plusieurs (one to many)</td>
                        <td>Fonctionnel</td>
                        <td>La cardinalité maximale d’un côté de la relation est 1 et l’autre côté est à n.</td>
                    </tr>
                    <tr>
                        <td>Plusieurs vers plusieurs (many to many)</td>
                        <td>Maillé</td>
                        <td>La cardinalité maximale sur chaque côté de la relation est n.</td>
                    </tr>
                </table>
                <p>
                    Pour définir le type d’une association, nous regardons sa cardinalité maximum de chaque côté de son
                    lien. Dans notre situation, dans l’association entre client et commande, la cardinalité maximum du
                    côté de l’entité client est «&nbsp;1&nbsp;» puisque nous avons «&nbsp;un et un seul&nbsp;». Du côté de l’entité commande, la
                    cardinalité maximum est «&nbsp;plusieurs&nbsp;» puisque nous avons «&nbsp;zéro à plusieur&nbsp;». En combinant les
                    cardinalités maximums, nous pouvons donc définir que le type de notre association est «&nbsp;1 vers
                    plusieurs&nbsp;».
                </p>
                <ColoredBox heading="Attention">
                    En général, nous verrons seulement des associations «&nbsp;un vers plusieurs&nbsp;» et «&nbsp;plusieurs vers
                    plusieurs&nbsp;». Si vous avez une associations «&nbsp;1 vers 1&nbsp;», c’est en général parce que vous avez divisé une
                    entité en deux morceaux, ce que nous ne voulons pas. Dans ces cas, essayé de reformer l'entité découpé pour retirer 
                    cette association.
                </ColoredBox>
            </section>

            <section>
                <h2>Définir les attributs d'association</h2>
                <p>
                    Revenons maintenant aux attributs pour les associations. Essentiellement, seulement les associations
                    «&nbsp;plusieurs vers plusieurs&nbsp;» peuvent avoir des attributs. Dans notre situation, l’association entre client
                    et commande ne peut donc pas avoir d’attribut. Toutefois, l’association entre commande et produit
                    peut en avoir. Il faut quand même comprendre que ce n’est pas obligatoire. Une association
                    «&nbsp;plusieurs vers plusieurs&nbsp;» ne contiendra pas nécessairement des attributs.
                </p>
                <p>
                    Dans notre situation, notre association entre commande et produit pourrait bénéficier de quelques
                    attributs:
                </p>
                <dl>
                    <dt>Quantité d'un produit</dt>
                    <dd>
                        La quantité d'un produit dans une commande. Sans cet attribut, une commande ne pourrait pas contenir plusieurs 
                        fois le même produit.
                    </dd>
                    <dt>Prix total d'un produit</dt>
                    <dd>
                        Le prix total d'un produit. On le calcule simplement en multipliant la quantité d'un produit avec son prix.
                    </dd>
                </dl>
            </section>

            <section>
                <h2>Dessiner les attributs d'association</h2>
                <p>
                    Pour indiquer les attributs à l’association, nous créons une entité que nous lierons d’un trait à notre
                    association. Il est important de donner un nom à cet entité-association puisque cela sera utile plus
                    tard lors de la création du modèle physique. Si vous ne trouvez pas de nom facilement, vous pouvez simplement
                    combiner le nom des 2 entités relié par l'association.
                </p>
                <img src={ step6 } alt="Dessiner les atrributs d'association" />
                <p>
                    Un fois cette étape passée, votre modèle entité-association est terminé. Nous pourrons donc passer à
                    la prochaine étape de création de la base de données, soit la création du modèle physique, qui sera vu
                    dans le prochain module.
                </p>
            </section>

            <section>
                <Video title="Modèle entité-association - Attributs d'association" src="https://www.youtube.com/embed/fWj_90Q2GZE" />
            </section>
        </>;
    }
}

export default EAAttributAssociation;