import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock'

const constants = 
`SELECT 
    42 AS 'Constante numérique', 
    '(╯°□°）╯︵ ┻━┻' AS 'Constante de texte';`;

const arithmetique = 
`-- Calcul matématique
SELECT (-25 + 50 * 3) / 5 - 12;;

-- Trouver l'aire et le périmètre d'un cercle à partir 
-- des données trouvées dans sa table
SELECT 
    (rayon * rayon * 3.14159) AS 'aire', 
    (2 * rayon * 3.14159) AS 'perimetre'
FROM cercle;`;

const increment = 
`UPDATE utilisateur
SET nb_acces = nb_acces + 1
WHERE id_utilisateur = 82;`;

class DMLMath extends React.Component {
    render() {
        return <>
            <section>
                <h2>Constantes</h2>
                <p>
                    Il est possible d'utiliser des constantes dans notre code SQL. Ces constantes peuvent être des 
                    nombres, des chaînes de caractères ou n'importe quel autre type de données disponible dans votre 
                    SGBD.
                </p>
                <CodeBlock language="sql">{ constants }</CodeBlock>
                <p>
                    Le code SQL ci-dessus vous retournerais le résultat suivant:
                </p>
                <table>
                    <tr>
                        <th>Constante numérique</th><th>Constante de texte</th>
                    </tr>
                    <tr>
                        <td>42</td><td>(╯°□°）╯︵ ┻━┻</td>
                    </tr>
                </table>
                <p>
                    Les constantes sont un peu inutile si on les utilisent toutes seules, mais elles sont très 
                    pratique si on les utilisent conjointement avec les opérateurs mathématiques ou les fonctions.
                </p>
            </section>

            <section>
                <h2>Opérateurs mathématiques</h2>
                <p>
                    Il est possible d'effectuer des opérations mathématique directement dans une recherche. Pour ce 
                    faire, nous utiliserons les opérateurs arithmétiques de base offert, par notre base de données:
                </p>
                <ul>
                    <li>Addition (+)</li>
                    <li>Soustraction (-)</li>
                    <li>Multiplication (*)</li>
                    <li>Division (/)</li>
                </ul>
                <CodeBlock language="sql">{ arithmetique }</CodeBlock>
                <p>
                    Il est même possible d'utiliser les constantes et les équations mathématique dans d'autres types 
                    de requêtes que les <IC>SELECT</IC>. Par exemple, on pourrait l'utiliser dans une mise à jour pour 
                    incrémenter la valeur d'un champ:
                </p>
                <CodeBlock language="sql">{ increment }</CodeBlock>
            </section>
        </>;
    }
}

export default DMLMath;