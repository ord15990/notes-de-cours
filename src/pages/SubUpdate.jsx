import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const syntax = 
`UPDATE table1
SET colonne = (
    SELECT colonne
    FROM table2
    WHERE ...
)
WHERE ...`;

const example = 
`UPDATE orders
SET employee_id = (
    SELECT id
    FROM employees
    WHERE email_address = 'steven@northwindtraders.com'
)
WHERE id = 42`;

export default class SubUpdate extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe</h2>
                <p>
                    Dans un <IC>UPDATE</IC>, il est possible de spécifier la valeur d'une colonne dans la 
                    clause <IC>SET</IC> à l'aide d'une sous-requête. Assurez-vous que votre sous-requête retourne une 
                    seule valeur puisque sinon, votre requête principale ne fonctionnera pas.
                </p>
                <CodeBlock language="sql">{ syntax }</CodeBlock>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    La requête ci-dessous modifie la commande ayant l'id <IC>42</IC> pour y changer l'employé qui l'a
                    fait pour l'employée ayant l'adresse courriel <IC>steven@northwindtraders.com</IC>.
                </p>
                <CodeBlock language="sql">{ example }</CodeBlock><ColoredBox heading="Attention">
                    <p>
                        Comme pour beaucoup de sous-requêtes, l'exemple ci dessus pourrait être fait à l'aide de 
                        jointures. Vous devriez toujours utiliser les jointures si c'est possible et utiliser les 
                        sous-requêtes uniquement lorsque les jointures ne permettent pas de faire la requête voulu.
                    </p>
                </ColoredBox>
            </section>
        </>;
    }
}
