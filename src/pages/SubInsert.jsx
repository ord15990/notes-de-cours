import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const syntax = 
`INSERT INTO table1 (colonne, ...)
SELECT autre_colonne AS colonne, ...
FROM table2
WHERE ...;`;

const example = 
`INSERT INTO orders (employee_id, customer_id)
SELECT id AS employee_id, 4 AS customer_id
FROM employees
WHERE email_address = 'steven@northwindtraders.com';`;

export default class SubInsert extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe</h2>
                <p>
                    Ce type de <IC>INSERT</IC> nous permet d'insérer des données à partir de données déjà présente dans la base
                    de données, à partir de calcul mathématique ou encore à partir de manipulations sur des chaînes de caractères. 
                    Il est important de bien nommer les colonnes avec des alias dans le <IC>SELECT</IC> de la requête imbriquée 
                    pour que les colonnes retournées correspondent aux colonnes à insérer dans la table.
                </p>
                <CodeBlock language="sql">{ syntax }</CodeBlock>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    La requête ci-dessous insère une commande faite par l'employé ayant l'adresse 
                    courriel <IC>steven@northwindtraders.com</IC> et acheté par le client ayant l'id <IC>4</IC>.
                </p>
                <CodeBlock language="sql">{ example }</CodeBlock>
            </section>
        </>;
    }
}
