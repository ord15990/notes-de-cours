import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import chinook from '../resources/db-chinook.zip'
import chinookPhysique from '../resources/db-chinook-physique.png'

export default class DBChinook extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    La base de données Chinook est une base de données fictives permettant de tester des requêtes 
                    SQL. Elle a été développé par Luis Rocha pour offrir une base de données fictive alternative à 
                    Northwind de Microsoft. C'est d'ailleurs pourquoi elle s'appelle Chinook. Le Chinook étant un 
                    nom de grand vent connu, comme Northwind.
                </p>
                <p>
                    Cette base de données simule un magasin de musique qui vend les musiques à l'unité. Vous 
                    trouverez le schémas de la base de données dans les téléchargements.
                </p>
            </section>

            <section>
                <h2>Installation</h2>
                <p>
                    Voici les étapes pour installer la base de données Chinook sur MySQL ou MariaDB:
                </p>
                <ol>
                    <li>
                        Télécharger les fichiers de la base de données Chinook ci-dessous.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>Chinook_Optimized.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargement</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ chinook } name="chinook.zip"></DownloadBlock.File>
                    <DownloadBlock.File path={ chinookPhysique } name="chinook-physique.png"></DownloadBlock.File>
                </DownloadBlock>
            </section>

            
        </>;
    }
}
