import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import solution from '../resources/laboratoire-8-solution.sql'

export default class LaboratoireJoin extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <ol>
                    <li>
                        Télécharger les fichiers de la base de données Northwind dans la section des bases de 
                        données de ce site Web.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind-data.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Créer un fichier SQL qui réponds à chacun des exercices ci-dessous.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Exercices</h2>
                <ol>
                    <li>
                        Sélectionnner toutes les commandes (orders) avec le nom et prénom de l'employé
                        (employees) qui l'a vendu.
                    </li>
                    <li>
                        Sélectionner toutes les commandes (orders) qui ont été fait pour les clients (customers) ayant
                        le nom de famille "Lee".
                    </li>
                    <li>
                        Sélectionner l'adresse de livraison complète (adresse, ville, province, pays, code postal) de
                        toutes les commandes (orders et order_details) qui contiennent le produit (products) ayant le
                        nom "Northwind Traders Curry Sauce".
                    </li>
                    <li>
                        Sélectionner tous les produits (products) étant vendus dans la commande (order et
                        order_details) ayant le id "42".
                    </li>
                    <li>
                        Sélectionner le nom de tous les produits (products) qui ont été commandé auprès des fournisseurs
                        (suppliers) "Supplier A" et "Supplier C". Assurez-vous de passer par la table 
                        "purchase_order_details" lors de vos jointures puisque la table "inventory_transactions" est 
                        incomplète.
                    </li>
                    <li>
                        Sélectionner le prix le plus élevé de livraison que le client (custommers) ayant pour prénom
                        "Francisco" a du payer pour une commande (orders).
                    </li>
                    <li>
                        Sélectionner le nom complet des clients (customers) qui ont fait une commande durant le mois
                        de février en 2006.
                    </li>
                    <li>
                        Sélectionner tous les produits (products) qui ont été vendu par les employés (employees) dont
                        le prénom débute par la lettre "N".
                    </li>
                    <li>
                        Sélectionner tous les produits (products) qui n'ont jamais été commandés (orders et
                        order_details).
                    </li>
                    <li>
                        Sélectionner le nom et prénom de tous les employés (employees) qui ont un prénom pareil à celui 
                        d'un des clients (customers).
                    </li>
                    <li>
                        Sélectionner une des commandes complètes (orders) ayant la plus grande quantité d'un seul produit
                        (order_details et products).
                    </li>
                    <li>
                        Sélectionner tous les employés (employees) qui n'ont jamais fait de commande (orders).
                    </li>
                    <li>
                        Sélectionner tous les employés (employees) ayant vendu un produit (products) dont le nom
                        contient le mot "mix".
                    </li>
                    <li>
                        Sélectionner distinctivement tous les produits (products) ayant été acheté par les clients
                        (customers) ayant le prénom "Michael", "Karen", "George" ou "Elizabeth".
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.sql"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}
