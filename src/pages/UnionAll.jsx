import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const unionAllExemple = 
`SELECT marque
FROM achat_auto
UNION ALL
SELECT marque
FROM vente_auto;`;

export default class UnionAll extends React.Component {
    render() {
        return <>
            <section>
                <h2>Accepter les doublons</h2>
                <p>
                    Parfois, nous ne voulons simplement pas que le doublons soient écrasés comme avec la 
                    clause <IC>UNION</IC>. Quand c'est le cas, nous utiliserons la clause <IC>UNION ALL</IC>. Voici un 
                    exemple utilisant les tables et données utilisé dans ce module jusqu'à présent:
                </p>
                <CodeBlock language="sql">{ unionAllExemple }</CodeBlock>
                <table>
                    <tr><th>marque</th></tr>
                    <tr><td>Chevrolet</td></tr>
                    <tr><td>Mercedes-Benz</td></tr>
                    <tr><td>Kia</td></tr>
                    <tr><td>Tesla</td></tr>
                    <tr><td>Chevrolet</td></tr>
                    <tr><td>Kia</td></tr>
                    <tr><td>Toyota</td></tr>
                    <tr><td>Toyota</td></tr>
                    <tr><td>Hyundai</td></tr>
                    <tr><td>Chevrolet</td></tr>
                    <tr><td>Hyundai</td></tr>
                    <tr><td>Kia</td></tr>
                    <tr><td>Chevrolet</td></tr>
                    <tr><td>Toyota</td></tr>
                </table>
                <p>
                    Dans ce cas-ci, la requête ne sort définitivement pas des données qui sont très utile. Il y a 
                    toutefois des cas ou la conservation des doublons est nécessaire. N'hésitez pas à utiliser la 
                    clause <IC>UNION ALL</IC> si c'est le cas.
                </p>
            </section>
        </>;
    }
}
