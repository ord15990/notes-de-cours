import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const create = 
`CREATE VIEW nom_vue AS
SELECT colonne, ...
FROM nom_table
WHERE ... ;`;

const replace = 
`CREATE OR REPLACE VIEW nom_vue AS
SELECT colonne, ...
FROM nom_table
WHERE ... ;`;

const drop = 
`DROP VIEW nom_vue;`;

export default class ViewIntro extends React.Component {
    render() {
        return <>
            <section>
                <h2>Description</h2>
                <p>
                    Les vues, aussi nommées <IC>VIEW</IC> en SQL, sont un moyen de créer des tables virtuelles basé sur des
                    requêtes SQL. Ces tables virtuelles sont presque identiques aux tables normales d'une base de données, 
                    mais le fait qu'elles sont basées sur des requêtes leur permettent de contenir des colonnes de plusieurs 
                    tables et de gérer ses données dynamiquement.
                </p>
                <p>
                    En bref, les vues sont une façon de créer une table à partir du résultat d'une requête SQL.
                </p>
            </section>

            <section>
                <h2>Syntaxe</h2>
                <p>
                    Pour créer une vue, nous utiliserons le mot-clé <IC>CREATE</IC> du langage SQL DDL de la façon 
                    suivante:
                </p>
                <CodeBlock language="sql">{ create }</CodeBlock>
                <p>
                    Pour remplacer une vue déjà existante ou la créer si elle n'existe pas, nous utiliserons plutôt la 
                    syntaxe suivante:
                </p>
                <CodeBlock language="sql">{ replace }</CodeBlock>
                <p>
                    Pour supprimer une vue, nous utiliserons simplement le mot-clé <IC>DROP</IC>:
                </p>
                <CodeBlock language="sql">{ drop }</CodeBlock>
            </section>
        </>;
    }
}
