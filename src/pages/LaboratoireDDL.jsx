import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import physique from '../resources/ddl-exercice-1.png'
import solution from '../resources/laboratoire-5-solution.sql'

export default class LaboratoireDDL extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    À partir du modèle physique suivant, produisez un fichier SQL permettant de créer la base de 
                    données qu'il représente.
                </p>
                <img src={ physique } alt="Collège - Modèle physique"/>
                <p>
                    Assurez-vous que votre base de données respecte les contraintes suivantes:
                </p>
                <ul>
                    <li>
                        Mettre une contrainte <IC>CHECK</IC> sur la <IC>note</IC> dans <IC>classe_etudiant</IC> et 
                        la <IC>note_moyenne</IC> dans <IC>etudiant</IC> qui doivent être entre 0 et 100 inclusivement.
                    </li>
                    <li>
                        Les <IC>nom</IC> et <IC>prenom</IC> des <IC>etudiant</IC> et <IC>professeur</IC> doivent être <IC>NOT NULL</IC>.
                    </li>
                    <li>
                        Ajouter un index sur le <IC>nom</IC> des <IC>cours</IC> et ajouter un index composé sur 
                        le <IC>prenom</IC> et le <IC>nom</IC> de <IC>etudiant</IC>.
                    </li>
                    <li>
                        Sur le <IC>DELETE</IC>, toutes les clés étrangères doivent utiliser la valeur d'intégrité 
                        référentielle <IC>RESCTRICT</IC>.
                    </li>
                    <li>
                        Sur le <IC>UPDATE</IC> toutes les clés étrangères doivent utiliser la valeur d'intégrité 
                        référentielle <IC>CASCADE</IC>.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.sql"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}
