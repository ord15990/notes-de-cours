import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const deleteSQL = 
`DELETE FROM nom_table
WHERE condition;`;

class DMLDelete extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe</h2>
                <CodeBlock language="sql">{ deleteSQL }</CodeBlock>
                <p>
                    La commande <IC>DELETE FROM</IC> permet de supprimer des données dans la base de données. Avec
                    cette commande, nous pouvons supprimer une ou plusieurs rangées dans une table. Les rangées 
                    supprimées sont celle qui respectent la condition dans le <IC>WHERE</IC>. Pour plus d'information 
                    sur les conditions, vous pouvez vous rendre à la page sur les conditions de ce module.
                </p>
                <p>
                    Voici quelques informations additionnelles sur la suppression de données:
                </p>
                <ul>
                    <li>
                        Si on ne spécifie aucune condition, toutes les données de la table seront supprimées, comme
                        pour la commande <IC>UPDATE</IC>. Ce genre d'opération peut donc être très dangereuse si on ne 
                        fait pas attention. Il est donc important de bien vérifier que la condition est présente et 
                        qu’elle est bonne.
                    </li>
                    <li>
                        Pour supprimer un élément spécifique de la table, on utilisera souvent comme condition son
                        ID (ex:&nbsp;<IC>id_commande&nbsp;=&nbsp;16</IC>).
                    </li>
                </ul>
            </section>
        </>;
    }
}

export default DMLDelete;