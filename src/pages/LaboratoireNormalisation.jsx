import React from 'react';
import DownloadBlock from '../component/DownloadBlock';

import exercice1 from '../resources/normalisation-exercice-1.png'
import solution from '../resources/laboratoire-4-solution.zip'

export default class LaboratoireNormalisation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    Pour chaque modèle physique suivant, utiliser les règles de la normalisation pour le transformer 
                    en un modèle physique respectant les 3 premières formes normales et utilisant des tables 
                    d'énumération là où il en est nécessaire.
                </p>
            </section>

            <section>
                <h2>Mises en situation</h2>
                <ol>
                    <li>
                        Base de données d'un collège
                        <img src={ exercice1 } alt="Normalisation d'un modèle" />
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}
