import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const groupHaving = 
`SELECT marque, COUNT(*) AS total
FROM vente_auto
GROUP BY marque
HAVING COUNT(*) > 2;`;

const groupHavingWhere = 
`SELECT marque, COUNT(*) AS total
FROM vente_auto
WHERE couleur <> 'noir'
GROUP BY marque
HAVING COUNT(*) > 2`;

export default class GroupCondition extends React.Component {
    render() {
        return <>
            <section>
                <h2>Clause <IC>HAVING</IC></h2>
                <p>
                    La clause <IC>HAVING</IC> est une clause similaire à la clause <IC>WHERE</IC>. Elle permet de 
                    spécifier des conditions comme la clause <IC>WHERE</IC>, mais après l'exécution du groupage.
                </p>
                <p>
                    Si nous voulions, par exemple, avoir toutes les marques de voitures dans notre base de données qui
                    ont plus de 2 véhicules, nous utiliserions le code SQL suivant:
                </p>
                <CodeBlock language="sql">{ groupHaving }</CodeBlock>
                <table>
                    <tr><th>marque</th><th>total</th></tr>
                    <tr><td>Chevrolet</td><td>3</td></tr>
                    <tr><td>Toyota</td><td>3</td></tr>
                </table>
                <p>
                    Il est important de savoir que seules les colonnes présente dans la clause <IC>GROUP BY</IC> ou 
                    les fonctions d'agrégation (ainsi que leur alias) peuvent être présente dans la 
                    clause <IC>HAVING</IC>.
                </p>
                <ColoredBox heading="À noter">
                    Dans l'exemple ci-dessus, il est possible de remplacer le <IC>COUNT(*)</IC> dans la 
                    clause <IC>HAVING</IC> par l'alias <IC>total</IC> pour simplifier le code SQL.
                </ColoredBox>
            </section>

            <section>
                <h2>Différence entre le <IC>WHERE</IC> et le <IC>HAVING</IC></h2>
                <p>
                    Il est très important de comprendre la différence entre la clause <IC>WHERE</IC> et la 
                    clause <IC>HAVING</IC> puisqu'il est possible d'avoir les 2 dans une même requête. Par exemple, 
                    dans la requête suivante, nous sélectionnons toutes les marques ayant plus de 2 véhicules qui ne 
                    sont pas de la couleur noir.
                </p>
                <CodeBlock language="sql">{ groupHavingWhere }</CodeBlock>
                <table>
                    <tr><th>marque</th><th>total</th></tr>
                    <tr><td>Chevrolet</td><td>3</td></tr>
                </table>
                <p>
                    Il est vraiment important de comprendre l'ordre d'exécution des différentes clauses. Dans 
                    l'exemple ci-dessus, les clauses sont exécutés dans l'ordre suivant:
                </p>
                <ol>
                    <li>
                        La clause <IC>WHERE</IC> filtre les voitures pour retirer les voitures noires.
                    </li>
                    <li>
                        La clause <IC>GROUP BY</IC> groupe les voitures filtrées par marque et exécute 
                        le <IC>COUNT(*)</IC> pour chaque groupe.
                    </li>
                    <li>
                        La clause <IC>HAVING</IC> filtre les groupes pour garder uniquement les marques ayant un total 
                        de voitures filtrées plus grand que 2.
                    </li>
                </ol>
            </section>
        </>;
    }
}
