import React from 'react';
import Video from '../component/Video';

import step7 from '../resources/physique-step7.png'

class PhysiqueTypes extends React.Component {
    render() {
        return <>
            <section>
                <h2>Types de base d'un SGBD</h2>
                <p>
                    La dernière étape de la création de modèle physique consiste à marquer le type de chaque attribut à
                    la droite de son nom. En général, nous utiliserons directement les types que nous offre notre SGBD.
                    Les SGBD utilisent tous des types de données similaires. Puisque nous travaillerons avec MySQL, voici
                    un tableau des types de données de base de MySQL:
                </p>
                <table>
                    <tr><th>Type de données</th><th>Description</th></tr>
                    <tr><td>TINYINT</td><td>Nombre entier entre -128 et 127</td></tr>
                    <tr><td>INT</td><td>Nombre entier entre -2147483648 et 2147483647</td></tr>
                    <tr><td>BIGINT</td><td>Nombre entier entre -9223372036854775808 et 9223372036854775807</td></tr>
                    <tr><td>FLOAT</td><td>Un nombre à virgule flottante petit</td></tr>
                    <tr><td>DOUBLE</td><td>Un nombre à virgule flottante grand</td></tr>
                    <tr><td>DATE</td><td>Une date</td></tr>
                    <tr><td>TIME</td><td>Un temps</td></tr>
                    <tr><td>DATETIME</td><td>Une date avec un temps</td></tr>
                    <tr><td>CHAR(size)</td><td>Une chaîne de caractère avec taille fixe</td></tr>
                    <tr><td>VARCHAR(max size)</td><td>Une chaîne de caractère avec taille variable</td></tr>
                    <tr><td>TEXT</td><td>Une longue chaîne de caractère de maximum 65535 caractères</td></tr>
                    <tr><td>BLOB</td><td>Binary Large OBject, Objet binaire de maximum 65535 octets</td></tr>
                </table>
                <p>
                    Il existe d’autres types, mais ils sont presque tous des variantes des types ci-dessus. Pour plus
                    d’information, vous pouvez aller voir cette page Web:
                </p>
                <p>
                    <a href="https://www.w3schools.com/sql/sql_datatypes.asp" target="blank" rel="norefferer noopenner">SQL Data Types for MySQL, SQL Server, and MS Access</a>
                </p>
            </section>

            <section>
                <h2>Ajouter les types à notre exemple</h2>
                <p>
                    Après l’ajout des types de données, nous avons le modèle physique final suivant:
                </p>
                <img src={ step7 } alt="Ajout des types de données" />
                <p>
                    La type pour plupart des colonnes est assez facile à déterminer. Bien entendu, le but est généralement
                    d’économiser le plus de mémoire possible, c’est pourquoi nous limitons la taille de certains champs.
                    Un cas particulier est celui des clés primaires qui nous viennent d’identifiant que nous avons créé. En
                    général, nous donnerons simplement un type de nombre entier à ces colonnes.
                </p>
                <p>
                    Après cette étape, votre modèle physique st terminé et prêt à être créer dans votre SGDB. Nous ferons
                    ce travail dans le prochain module à l’aide du langage SQL.
                </p>
            </section>

            <section>
                <Video title="Modèle physique - Types de données" src="https://www.youtube.com/embed/VMH08YO1za0" />
            </section>
        </>;
    }
}

export default PhysiqueTypes;