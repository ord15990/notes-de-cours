import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import solution from '../resources/laboratoire-trigger-vue.sql'

export default class LaboratoireViewTrigger extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <ol>
                    <li>
                        Télécharger les fichiers de la base de données Northwind dans la section des bases de 
                        données de ce site Web.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind-data.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Créer un fichier SQL qui réponds à chacun des exercices ci-dessous.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Exercices</h2>
                <ol>
                    <li>
                        Créer un trigger qui empêchera la suppression des fournisseurs (suppliers) ayant l'id 1, 4, 5 et 9.
                    </li>
                    <li>
                        <p>
                            Ajouter une colonne <IC>subtotal</IC> de type <IC>DECIMAL(15, 2)</IC> avec une valeur par 
                            défaut de zéro à la table <IC>orders</IC>.
                        </p>
                        <p>
                            Créer un déclencheur sur le <IC>INSERT</IC>, <IC>UPDATE</IC> et <IC>DELETE</IC> dans la 
                            table <IC>order_details</IC> qui calculera le total avant taxe et sans les frais de 
                            livraison de la commande et qui l'insèrera dans la colonne <IC>subtotal</IC> de la 
                            commande.
                        </p>
                    </li>
                    <li>
                        Créer un déclencheur qui ajoute automatiquement le prix d'un produit vendu dans la 
                        table <IC>order_details</IC> à partir du <IC>product_id</IC> spécifié dans le <IC>INSERT</IC>.
                    </li>
                    <li>
                        <p>
                            Créer une table <IC>log_order_details</IC> qui contiendra les champs suivant:
                        </p>
                        <table>
                            <tr><th>Colonne</th><th>Type</th><th>Contrainte</th></tr>
                            <tr><td>id</td><td><IC>INT</IC></td><td><IC>NOT NULL</IC>, <IC>AUTO_INCREMENT</IC></td></tr>
                            <tr><td>order_details_id</td><td><IC>INT</IC></td><td><IC>NOT NULL</IC></td></tr>
                            <tr><td>operation</td><td><IC>CHAR(6)</IC></td><td><IC>NOT NULL</IC>, (Valeurs acceptés: 'INSERT', 'UPDATE' ou 'DELETE')</td></tr>
                            <tr><td>date</td><td><IC>DATETIME</IC></td><td><IC>NOT NULL</IC></td></tr>
                        </table>
                        <p>
                            Créer un déclencheur sur le <IC>INSERT</IC>, <IC>UPDATE</IC> et <IC>DELETE</IC> dans la 
                            table <IC>order_details</IC> qui ajoute une entrée dans la table <IC>log_order_details</IC> à 
                            chaque fois qu'une opération est faite.
                        </p>
                    </li>
                    <li>
                        Créer une vue ayant pour nom <IC>produits_non_vendu</IC> contenant toutes l'information des
                        produits n'ayant jamais été vendu.
                    </li>
                    <li>
                        Créer une vue ayant pour nom <IC>paires_employee_client</IC> contenant les prénoms et noms
                        combiné en un seul champ des employés (employees) et des clients (customers) pour chaque
                        commande (orders). Vous devez mettre l'alias <IC>Employee</IC> pour le nom de l'employée et l'alias
                        <IC>Customer</IC> pour le nom du client.
                    </li>
                    <li>
                        Essayer d'insérer des données directement dans la vue <IC>produits_non_vendu</IC>. Est-il possible
                        d'ajouter des données dans cette vue?
                    </li>
                    <li>
                        Essayer de modifier le champ <IC>description</IC> directement dans la vue <IC>produits_non_vendu</IC>.
                        Est-il possible de modifier des données dans cette vue?
                    </li>
                    <li>
                        Essayer de supprimer des données directement dans la vue <IC>produits_non_vendu</IC>. Est-il
                        possible de supprimer des données dans cette vue?
                    </li>
                    <li>
                        Créer une vue ayant pour nom <IC>certaines_commandes</IC> contenant toutes les informations de
                        toutes les commandes (orders) qui n'ont rien coûté à envoyer (shipping_fee).
                    </li>
                    <li>
                        Essayer d'insérer des données directement dans la vue <IC>certaines_commandes</IC>. Est-il possible
                        d'insérer des données dans cette vue?
                    </li>
                    <li>
                        Essayer de supprimer des données directement dans la vue <IC>certaines_commandes</IC>. Est-il
                        possible de supprimer des données dans cette vue?
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.sql"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}
