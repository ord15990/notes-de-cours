import React from 'react';
import CodeBlock from '../component/CodeBlock'

const example = 
`SELECT o.*
FROM orders o
INNER JOIN order_details od ON o.id = od.order_id
GROUP BY o.id
HAVING SUM(od.quantity * od.unit_price) = (
    SELECT MAX(montant) AS montant
    FROM (
        SELECT od.order_id, SUM(od.quantity * od.unit_price) AS montant
        FROM order_details od
        GROUP BY od.order_id
    ) AS montant
);`;

export default class SubInSub extends React.Component {
    render() {
        return <>
            <section>
                <h2>Exemple</h2>
                <p>
                    Il est possible de mettre des sous-requêtes dans d'autres sous requêtes. Cela complexifie beaucoup
                    les requêtes. Utilisez les donc seulement si Google vous le suggère.
                </p>
                <p>
                    Dans l'exemple ci-dessous, on sélectionne les commandes qui ont été vendu le plus cher. En effet, 
                    si 2 commandes ont la même valeur de vente la plus cher, ces 2 commandes seront retournées.
                </p>
                <CodeBlock language="sql">{ example }</CodeBlock>
            </section>
        </>;
    }
}
