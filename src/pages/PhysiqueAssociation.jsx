import React from 'react';
import Video from '../component/Video';
import IC from '../component/InlineCode';

import step2 from '../resources/physique-step2.png'
import step3 from '../resources/physique-step3.png'
import step4 from '../resources/physique-step4.png'
import step5 from '../resources/physique-step5.png'

class PhysiqueAssociation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Types d'associations</h2>
                <p>
                    Le traitement des associations dans le modèle physique est l'étape la plus longue de la conversion 
                    du modèle entité-association vers le modèle physique. Ceci est dû au fait qu'il y a différents cas
                    pour le traitement des associations. En fait, les cas sont en fonction du type de l’association
                    dans le modèle entité-association. Il y a trois type d’association, donc nous avons trois cas lors du
                    traitement des associations dans le modèle physique.
                </p>
                <p>
                    Voici un petit rappel des types d'associations
                </p>
                <table>
                    <tr>
                        <th>Type</th><th>Nom</th><th>Description</th>
                    </tr>
                    <tr>
                        <td>1 vers 1 (one to one)</td>
                        <td>Binaire</td>
                        <td>La cardinalité maximale sur chaque côté de la relation est 1.</td>
                    </tr>
                    <tr>
                        <td>1 vers plusieurs (one to many)</td>
                        <td>Fonctionnel</td>
                        <td>La cardinalité maximale d’un côté de la relation est 1 et l’autre côté est à n.</td>
                    </tr>
                    <tr>
                        <td>Plusieurs vers plusieurs (many to many)</td>
                        <td>Maillé</td>
                        <td>La cardinalité maximale sur chaque côté de la relation est n.</td>
                    </tr>
                </table>
            </section>

            <section>
                <h2>Un vers un</h2>
                <p>
                    Lorsque vous avez une association 1 vers 1, le traitement est simple: vous fusionnez les tables.
                    Il est toutefois important de ne garder qu’une seule clé primaire. En général, nous utiliserons la clé
                    primaire la plus significative. Pour le nom de la table, nous utiliserons aussi le nom le plus significatif.
                    Dans notre situation, nous avons une seule association 1 vers 1. C’est l’association entre
                    commande et dateCommande. Nous allons donc fusionner les deux tables.
                </p>
                <img src={ step2 } alt="Traitement des associations 1 vers 1" />
                <p>
                    Vous constaterez que nous avons gardé le nom <IC>commande</IC> pour la table et <IC>id_commande</IC> pour
                    la clé primaire. Toutes les autres colonnes ont été fusionnées dans la même table.
                </p>
            </section>

            <section>
                <h2>Un vers plusieurs</h2>
                <p>
                    Avant de voir le traitement pour les associations 1 vers plusieurs, nous devons introduire un
                    nouveau terme, soit les clés étrangères (foreign key). Une clé étrangère est une colonne dans une table qui contient
                    la valeur de la clé primaire d’une autre table. Il y a deux façon de représenter les clés étrangères:
                </p>
                <ul>
                    <li>Le symbole <IC>#</IC> devant son nom</li>
                    <li>Les lettres <IC>FK</IC> (Foreign Key) à la gauche de son nom</li>
                </ul>
                <p>
                    Il sera plus facile de comprendre comment fonctionne une clé étrangère en utilisant notre situation
                    d’exemple. Pour le moment, revenons à notre association 1 vers plusieurs. Lorsque nous avons
                    cette association, nous prenons la clé primaire de la table du côté «&nbsp;1&nbsp;» de l’association et nous la
                    copierons comme colonne dans la table du côté &nbsp;plusieurs&nbsp;» de l’association. Cette clé primaire copié
                    est appelé une clé étrangère.
                </p>
                <p>
                    Dans notre sitation d’exemple, nous avons deux associations 1 vers plusieurs. Il y a celle entre
                    client et commande et celle entre produit et categorie. Voici notre modèle résultant après avoir traité
                    ces associations:
                </p>
                <img src={ step3 } alt="Traitement des associations 1 vers plusieurs" />
                <p>
                    Nous avons donc copier la clé primaire de la table <IC>client</IC> dans la table <IC>commande</IC> comme clé
                    étrangère. Nous avons fait de même pour la clé primaire de <IC>categorie</IC> dans <IC>produit</IC>. Ces clés
                    étrangères nous permettent de faire les liens entre les tables. Dans notre situation, puisque chaque commande 
                    doit avoir un seul client, la clé étrangère dans la table commande nous indiquera quel client à passé cette 
                    commande puisqu’elle contient l’identifiant du client.
                </p>
            </section>

            <section>
                <h2>Plusieurs vers plusieurs</h2>
                <p>
                    L’association plusieurs vers plusieurs est la plus difficile à gérer. Lorsque nous avons ce type
                    d’association, nous devrons créer une nouvelle table entre les 2 tables de cette association. Cette nouvelle table 
                    aura comme nom celui des deux tables de l'association originale séparé par un <IC>_</IC>. Si l'association contenait
                    déjà des attributs d'association vous pouvez aussi utiliser le nom de l’entité d’association, à condition de le 
                    transformer en <IC>snake_case</IC>. 
                </p>
                <p>
                    Dans cette table, nous ajouterons les clés primaires des deux tables de l'association comme clés étrangères et comme 
                    clé primaire. En effet, cette combinaison de clés étrangère sera aussi notre clé primaire pour cette table. 
                </p>
                <p>
                    Finalement, si vous avez des attributs d'association sur cette association, ajoutez-les à cette nouvelle table en 
                    vous assurant de les mettre en <IC>snake_case</IC>. 
                </p>
                <p>
                    Il est beaucoup plus facile de comprendre avec un exemple. Dans notre situation, nous avons une
                    seule association plusieurs vers plusieurs. C’est l’association entre commande et produit. Après
                    avoir traité cette association, nous avons le modèle physique suivant:
                </p>
                <img src={ step4 } alt="Traitement des associations plusieurs vers plusieurs" />
                <p>
                    Comme vous pouvez le voir, nous avons créé une table nommée <IC>commande_produit</IC>. Cette table
                    contient une clé primaire composé des clés étrangère <IC>id_commande</IC> et <IC>id_produit</IC>. Cela veut
                    dire que l’identifiant de notre table est la combinaison des deux attributs. Puisque cette association
                    avait une entité d’association, nous y avons ajouté les attributs <IC>quantite</IC> et <IC>prix_total_produit</IC>.
                </p>
            </section>

            <section>
                <h2>Ajouter les liens</h2>
                <p>
                    Une fois toutes les associations traitées, il nous reste simplement qu'à tracer les liens entre les tables. Nous
                    faisons cette étape principalement pour que le diagramme physique soit plus facile à lire. À cette
                    étape, nous devons simplement relier les clés étrangères vers leur table d’origine.
                </p>
                <p>
                    Si nous ajoutons tous les liens dans notre situation, nous avons le modèle physique suivant:
                </p>
                <img src={ step5 } alt="Traitement des associations plusieurs vers plusieurs" />
            </section>

            <section>
                <Video title="Modèle physique - Traitement des associations" src="https://www.youtube.com/embed/lkE3ZsOdREE" />
            </section>
        </>;
    }
}

export default PhysiqueAssociation;