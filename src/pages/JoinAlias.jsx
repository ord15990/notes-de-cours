import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock';
import Situation from '../component/Situation';
import ColoredBox from '../component/ColoredBox';

const aliasLong = 
`SELECT 
    membre_du_gym.prenom, 
    membre_du_gym.nom, 
    cours_activite_physique.nom, 
    membre_du_gym_cours_activite_physique.date_inscription
FROM membre_du_gym 
INNER JOIN membre_du_gym_cours_activite_physique 
    ON membre_du_gym_cours_activite_physique.id_membre = membre_du_gym.id_membre
INNER JOIN cours_activite_physique 
    ON membre_du_gym_cours_activite_physique.id_cours = cours_activite_physique.id_cours`;

const aliasCourt = 
`SELECT m.prenom, m.nom, c.nom, mc.date_inscription
FROM membre_du_gym m 
INNER JOIN membre_du_gym_cours_activite_physique mc
    ON mc.id_membre = m.id_membre
INNER JOIN cours_activite_physique c
    ON mc.id_cours = c.id_cours`;

const allColumns = 
`SELECT membre.*, cours.nom
FROM membre 
INNER JOIN cours ON membre.id_cours = cours.id_cours`;

export default class JoinAlias extends React.Component {
    render() {
        return <>
            <section>
                <h2>Raccourcir le nom des tables</h2>
                <p>
                    Voici une mise en situation qui arrive beaucoup trop souvent:
                </p>
                <Situation>
                    Vous avez une table <IC>membre_du_gym</IC> et une table <IC>cours_activite_physique</IC> qui sont 
                    liées ensemble par une table de jonction <IC>membre_du_gym_cours_activite_physique</IC>. Vous 
                    faites une requête pour aller chercher les noms des membres, le nom du cours ainsi que la date 
                    d'inscription du membre à ce cours.
                </Situation>
                <p>
                    À partir de cette situation, vous pourriez en arriver à la requête suivante:
                </p>
                <CodeBlock language="sql">{ aliasLong }</CodeBlock>
                <p>
                    Comme vous pouvez le constater, la requête est très volumineuse, principalement dû aux longs noms 
                    de ses tables. Il y a toutefois possibilité de donner un alias aux tables pour réduire leur taille
                    et facilité la lecture de celles-ci:
                </p>
                <CodeBlock language="sql">{ aliasCourt }</CodeBlock>
                <p>
                    Vous noterez l'utilisation d'un alias après le nom de la table dans le <IC>FROM</IC> et dans 
                    les <IC>JOIN</IC>. Une fois ces alias défini, la requête permettra d'utiliser ces noms raccourcis.
                </p>
                <ColoredBox heading="Attention">
                    Si vous définissez un alias pour une table dans une requête, il ne sera plus possible d'utiliser 
                    son nom original pour le restant de la requête. Seul le nom raccourci sera disponible.
                </ColoredBox>
            </section>

            <section>
                <h2>Chercher toutes les colonnes</h2>
                <p>
                    Si vous désirez chercher toutes les colonnes d'une table dans une requête avec des jointures, vous 
                    pourrez utiliser l'opérateur <IC>*</IC>, de façon similaire aux requêtes sans jointures. Voici un 
                    exemple:
                </p>
                <CodeBlock language="sql">{ allColumns }</CodeBlock>
            </section>
        </>;
    }
}
