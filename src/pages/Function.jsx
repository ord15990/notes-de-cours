import React from 'react';

export default class Function extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Il existe dans chaque système de base de données des fonctions déjà prédéfini qui peuvent nous être
                    utile dans nos requêtes SQL. Il en existe une très grande quantité dans MySQL. Dans le document
                    suivant, nous survelerons seulement certaines fonctions plus populaire.
                </p>
                <p>
                    Pour voir l'étendu des fonctions disponibles ou savoir comment utiliser les fonctions dans ce 
                    document, vous pouvez vous rendre sur la page de documentation suivante:
                </p>
                <p>
                    <a href="https://dev.mysql.com/doc/refman/8.0/en/sql-function-reference.html" target="_blank" rel="noopener noreferrer">
                        SQL Function and Operator Reference
                    </a>
                </p>
                <p>
                    <a href="https://www.w3schools.com/mysql/mysql_ref_functions.asp" target="_blank" rel="noopener noreferrer">
                        MySQL Functions
                    </a>
                </p>
                
            </section>

            <section>
                <h2>Fonctions sur les nombres</h2>
                <table>
                    <tr><th>Fonction</th><th>Description</th></tr>
                    <tr><td>ABS()</td><td>Valeur absolue</td></tr>
                    <tr><td>CEIL()</td><td>Retourne l'entier supérieur</td></tr>
                    <tr><td>FLOOR()</td><td>Retourne l'entier inférieur</td></tr>
                    <tr><td>PI()</td><td>Retourne la valeur de Pi</td></tr>
                    <tr><td>SIN()</td><td>Sinus</td></tr>
                    <tr><td>COS()</td><td>Cosinus</td></tr>
                    <tr>
                        <td>SIGN()</td>
                        <td>
                            Retourne 1 si le nombre est positif, -1 si le nombre est négatif ou 0 si le nombre est zéro
                        </td>
                    </tr>
                    <tr><td>POW()</td><td>Exposant</td></tr>
                    <tr><td>SQRT()</td><td>Racine carré</td></tr>
                    <tr><td>LOG()</td><td>Logarithme</td></tr>
                    <tr>
                        <td>+ - * / %</td>
                        <td>
                            Techniquement, ce ne sont pas des fonctions, mais des opérateurs. On peut les
                            utiliser pour faire des opérations arithmétique sur les colonnes de type numérique
                        </td>
                    </tr>
                </table>
            </section>

            <section>
                <h2>Fonctions sur les chaînes de caractères</h2>
                <table>
                    <tr><th>Fonction</th><th>Description</th></tr>
                    <tr><td>LENGTH()</td><td>Retourne la longueur d'une chaîne.</td></tr>
                    <tr><td>INSERT()</td><td>Insère une chaîne dans une autre chaîne à une position spécifique.</td></tr>
                    <tr><td>CHAR()</td><td>Retourne le caractère à une position spécifique dans la chaine.</td></tr>
                    <tr><td>FORMAT()</td><td>Formattage d'un nombre avec un certain nombre de décimale.</td></tr>
                    <tr><td>REPLACE()</td><td>Remplace toutes les occurences d'une chaîne dans une autre chaîne.</td></tr>
                    <tr><td>SUBSTR()</td><td>Retourne la sous-chaîne spécifiée.</td></tr>
                    <tr><td>TRIM()</td><td>Enlève les caractères blanc (espace, tabulation, etc.) avant et après la chaîne.</td></tr>
                    <tr><td>UPPER()</td><td>Convertit en majuscule.</td></tr>
                    <tr><td>LOWER()</td><td>Convertit en minuscule.</td></tr>
                    <tr><td>CONCAT()</td><td>Retourne la concaténation de chaînes de caractères.</td></tr>
                </table>
            </section>

            <section>
                <h2>Fonctions sur les dates et heures</h2>
                <table>
                    <tr><th>Fonction</th><th>Description</th></tr>
                    <tr><td>NOW()</td><td>Retourne la date et l'heure courante.</td></tr>
                    <tr><td>SECOND()</td><td>Permet de retourner la valeur à la position des secondes d'un temps.</td></tr>
                    <tr><td>MINUTE()</td><td>Permet de retourner la valeur à la position des minutes d'un temps.</td></tr>
                    <tr><td>HOUR()</td><td>Permet de retourner la valeur à la position de l'heure d'un temps.</td></tr>
                    <tr><td>DAY()</td><td>Permet de retourner la valeur à la position des jours d'une date.</td></tr>
                    <tr><td>MONTH()</td><td>Permet de retourner la valeur à la position des mois d'une date.</td></tr>
                    <tr><td>YEAR()</td><td>Permet de retourner la valeur à la position des l'année d'une date.</td></tr>
                    <tr><td>ADDDATE()</td><td>Ajoute un interval à une date.</td></tr>
                    <tr><td>ADDTIME()</td><td>Ajoute un interval à un temps.</td></tr>
                    <tr><td>SUBDATE()</td><td>Soustrait un interval à une date.</td></tr>
                    <tr><td>SUBTIME()</td><td>Soustrait un interval à un temps.</td></tr>
                    <tr><td>UNIX_TIMESTAMP()</td><td>Retourne le temps UNIX (EPOCH) d'une date et heure.</td></tr>
                </table>
            </section>
        </>;
    }
}
