import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const insert = 
`INSERT INTO nom_table (nom_colonne1, nom_colonne2, nom_colonne3, ...)
VALUES (valeur1, valeur2, valeur3, ...);`;

class DMLInsert extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe</h2>
                <CodeBlock language="sql">{ insert }</CodeBlock>
                <p>
                    La commande <IC>INSERT INTO</IC> permet d’ajouter des données dans la base de données. Cette
                    commande insère une rangée dans la base de données. 
                </p>
                <p>
                    Voici quelques points important à se souvenir avec l’insertion des données:
                </p>
                <ul>
                    <li>
                        Si on omet une colonne (son nom et sa valeur), la commande met la valeur <IC>NULL</IC> ou la 
                        valeur par défaut de la colonne. Si la colonne est <IC>NOT NULL</IC> et qu'elle n'a pas de 
                        valeur par défaut, l’insertion ne réussira pas.
                    </li>
                    <li>
                        On a pas besoin de spécifier une colonne (son nom et sa valeur) si elle est 
                        en <IC>AUTO_INCREMENT</IC>.
                    </li>
                </ul>
            </section>
        </>;
    }
}

export default DMLInsert;