import React from 'react';
import Video from '../component/Video';

class Introduction extends React.Component {
    render() {
        return <>
            <section>
                <h2>Base de données</h2>
                <p>
                    Les données sont un des plus importants éléments de l’informatique. Les processus informatiques
                    traitent des données. Sans données, il n’y aurait pas beaucoup d’actions sur nos ordinateur. Dans ce
                    cours, nous nous intéresserons surtout au stockage des données. Comme leur nom l’indique, les bases
                    de données servent principalement à conserver et stocker des données. Les buts premiers d’une base
                    de données sont :
                </p>
                <ul>
                    <li>Séparer les données des programmes</li>
                    <li>Permettre de stocker plus d’information</li>
                    <li>Stocker les données sur un média permanent</li>
                    <li>Conserver les données même en cas d’erreur ou de pannes</li>
                </ul>
                <p>
                    Une base de données peut être très simple. Un fichier texte ou une feuille de calcul pour tableur
                    peuvent tout à fait être utilisé comme base de données. Ces méthodes ont toutefois beaucoup de
                    contraintes et de nombreuses limites. C’est pourquoi nous utiliserons généralement des <strong>systèmes de
                    gestion de bases de données</strong>.
                </p>
            </section>

            <section>
                <h2>Les systèmes de gestion de bases de données (SGBD)</h2>
                <p>
                    Un SGBD est un logiciel permettant de gérer des bases de données. Il offre en général une gamme
                    d’outils permettant de simplifier certaines tâches sur les bases de données. Comme mentionné ci-dessus,
                    si notre but est simplement de stocker des données, l’utilisation d’un fichier texte ou d’une
                    feuille de calcul dans un tableur est tout à fait suffisant. Les SGDB ont toutefois deux avantages
                    majeurs :
                </p>
                <ul>
                    <li>La recherche de données</li>
                    <li>La manipulation de données</li>
                </ul>
                <p>
                    En effet, les SGBD sont construit de façon à permettre des recherches très efficaces parmi une très grande 
                    quantité de données et une manipulation facile des données, donc de facilement pouvoir ajouter, modifier ou
                    supprimer des données.
                </p>
                <p>
                    Il y a d’autres avantages, mais ils ne sont pas nécessairement typiques de tous les SGBD ou ils ne sont
                    pas exclusif aux SGBD. En voici quelques-uns :
                </p>
                <ul>
                    <li>La distribution des données (séparer les données sur plusieurs ordinateurs)</li>
                    <li>La liaison des données (faire des liens entre les données)</li>
                    <li>La souplesse pour la quantité des données</li>
                </ul>
            </section>

            <section>
                <h2>Base de données VS Système de gestion de bases de données</h2>
                <p>
                    En théorie, une base de données et un SGBD sont différents. Une base de données est le conteneur contenant les 
                    données. Ceci est généralement un ou plusieurs fichiers sur un ordinateur. Un SGBD est le logiciel permettant 
                    de manipuler les bases de données. Donc, c'est le logiciel permettant de modifier ou de rechercher dans les 
                    fichiers de base de données.
                </p>
                <p>
                    Pratiquement, les termes « système de gestion de base de données » (SGBD) et « base de données » (BD) sont
                    souvent interchangés. En général, quand quelqu’un parle d’une « base de données », il parle en fait d’un 
                    « système de gestion de base de données ».
                </p>
            </section>

            <section>
                <h2>Exemples de SGBD</h2>
                <p>
                    Voici quelques exemples de SGBD connus :
                </p>
                <ul>
                    <li>
                        Gratuit
                        <ul>
                            <li>
                                <a href="https://www.mysql.com/" target="_blank" rel="noopener noreferrer">MySQL</a>
                            </li>
                            <li>
                                <a href="https://mariadb.org/" target="_blank" rel="noopener noreferrer">MariaDB</a>
                            </li>
                            <li>
                                <a href="https://www.postgresql.org/" target="_blank" rel="noopener noreferrer">PostgreSQL</a>
                            </li>
                            <li>
                                <a href="https://www.sqlite.org/" target="_blank" rel="noopener noreferrer">SQLite</a>
                            </li>
                            <li>
                                <a href="https://www.mongodb.com/" target="_blank" rel="noopener noreferrer">MongoDB</a>
                            </li>
                            <li>
                                <a href="https://redis.io/" target="_blank" rel="noopener noreferrer">Redis</a>
                            </li>
                            <li>
                                <a href="https://cassandra.apache.org/" target="_blank" rel="noopener noreferrer">Apache Cassandra</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Payant
                        <ul>
                            <li>
                                <a href="https://www.microsoft.com/en-ca/sql-server/sql-server-2019" target="_blank" rel="noopener noreferrer">Microsoft SQL Server</a>
                            </li>
                            <li>
                                <a href="https://www.oracle.com/ca-en/database/technologies/" target="_blank" rel="noopener noreferrer">Oracle Database</a>
                            </li>
                            <li>
                                <a href="https://www.ibm.com/ca-en/products/db2-database" target="_blank" rel="noopener noreferrer">IBM DB2</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </section>

            <section>
                <Video title="Vidéo d'introduction" src="https://www.youtube.com/embed/ivBqHRUeVC4" />
            </section>
        </>;
    }
}

export default Introduction;