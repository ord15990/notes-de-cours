import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock'

const orderBy = 
`SELECT * 
FROM client
WHERE nom LIKE 'C%'
ORDER BY prenom DESC`;

const orderByLimit = 
`-- Retourne les données de la partie ayant le plus haut pointage
SELECT *
FROM partie
ORDER BY pointage DESC
LIMIT 1`;

const orderByMultiple = 
`SELECT *
FROM employe
ORDER BY prenom ASC, nom DESC`;

class DMLOrder extends React.Component {
    render() {
        return <>
            <section>
                <h2>Trier les recherches</h2>
                <p>
                    Si vous voulez trier les résultats de vos recherches, c'est le mot-clé <IC>ORDER BY</IC> qui vous 
                    sera utile. Nous utiliserons aussi les mot-clés <IC>ASC</IC> et <IC>DESC</IC> pour indiquer le 
                    sens du triage. Si aucun sens n’est spécifié, les sens <IC>ASC</IC> est utilisé par défaut.
                </p>
                <CodeBlock language="sql">{ orderBy }</CodeBlock>
            </section>

            <section>
                <h2>Utiliser avec limites</h2>
                <p>
                    Il peut être pratique d'utiliser le triage conjointement avec les limites pour retourner la rangée 
                    ayant la valeur la plus petite ou la plus grande.
                </p>
                <CodeBlock language="sql">{ orderByLimit }</CodeBlock>
            </section>

            <section>
                <h2>Trier sur plusieurs colonnes</h2>
                <p>
                    Nous pouvons ordonner les données par plusieurs colonnes. Ainsi, si deux rangées ont la même 
                    valeur pour une colonne lors de l’ordonnancement, une deuxième colonne peut être utilisé pour les 
                    trier.
                </p>
                <CodeBlock language="sql">{ orderByMultiple }</CodeBlock>
            </section>
        </>;
    }
}

export default DMLOrder;