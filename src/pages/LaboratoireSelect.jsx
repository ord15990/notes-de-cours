import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import distribue from '../resources/laboratoire-7-distribué.sql'
import solution from '../resources/laboratoire-7-solution.sql'

export default class LaboratoireSelect extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <ol>
                    <li>
                        Exécuter le script <IC>distribué.sql</IC> sur votre SGBD pour créer la base de 
                        données <IC>boutique_jouet</IC>. Cette base de donnée est une version un peu simplifié de 
                        la base de données de la boutique de jouet utilisé dans les modules précédants.
                    </li>
                    <li>
                        Créer un fichier SQL qui réponds à chacun des exercices ci-dessous. N'hésitez pas à 
                        ajouter plus de données ou à réinitialiser la base de données pour bien tester vos 
                        requêtes SQL.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Exercices</h2>
                <ol>
                    <li>
                        Sélectionner toutes l’information de tous les clients.
                    </li>
                    <li>
                        Sélectionner uniquement le nom, prénom et date de naissance de tous les clients.
                    </li>
                    <li>
                        Sélectionner le nom, prénom et date de naissance de tous les clients qui sont née en 1992 ou après.
                    </li>
                    <li>
                        Sélectionner le nom et le prénom de tous les clients qui ont le nom <IC>Laporte</IC>
                        et le prénom <IC>Éric</IC>.
                    </li>
                    <li>
                        Sélectionner tous les clients qui sont née avant 1992.
                    </li>
                    <li>
                        Sélectionner le nom et le prénom de tous les clients avec l’alias <IC>NomFamille</IC> pour le nom et
                        l’alias <IC>Prenom</IC> pour le prénom.
                    </li>
                    <li>
                        Sélectionner tous les clients qui ont le nom <IC>Laporte</IC> ou <IC>Roger</IC>.
                    </li>
                    <li>
                        Sélectionner tous les clients qui n’ont pas le nom <IC>Laporte</IC>.
                    </li>
                    <li>
                        Sélectionner tous les clients qui n’ont pas de date de naissance.
                    </li>
                    <li>
                        Sélectionner tous les clients qui ont une date de naissance.
                    </li>
                    <li>
                        Sélectionner tous les clients qui sont née entre 1992 et 1997.
                    </li>
                    <li>
                        Sélectionner tous les clients dont le nom est parmi <IC>Laporte</IC>, <IC>Gagnon</IC> et <IC>Dugal</IC>.
                    </li>
                    <li>
                        Sélectionner tous les clients dont le nom n'est pas parmi <IC>Tremblay</IC>, <IC>Gagnon</IC> et <IC>Wayne</IC>.
                    </li>
                    <li>
                        Sélectionner tous les clients dont le nom commence par la lettre <IC>G</IC>.
                    </li>
                    <li>
                        Sélectionner tous les clients dont le nom commence par la lettre <IC>G</IC> et qui se termine par la
                        lettre <IC>x</IC>.
                    </li>
                    <li>
                        Sélectionner tous les clients dont le nom se termine par la lettre <IC>y</IC>.
                    </li>
                    <li>
                        Sélectionner tous les clients dont le nom commence par la lettre <IC>G</IC> et qui ont une longueur
                        de 6 lettres.
                    </li>
                    <li>
                        Sélectionner tous les clients dont le nom commence par la lettre <IC>G</IC> et qui ont une longueur
                        d’au moins 2 lettres.
                    </li>
                    <li>
                        Sélectionner tous les produits par ordre alphabétique de nom.
                    </li>
                    <li>
                        Sélectionner tous les clients par ordre alphabétique de prénom et nom.
                    </li>
                    <li>
                        Sélectionner les 3 clients les plus jeunes qui ont une date de naissance qui n'est pas nulle.
                    </li>
                    <li>
                        Sélectionner le top 5 des produits les plus chers.
                    </li>
                    <li>
                        Sélectionner le top 3 des produits les moins chers.
                    </li>
                    <li>
                        Sélectionner les 2 plus vieilles commandes.
                    </li>
                    <li>
                        Sélectionner tous les clients qui ont une date de naissance en ordre alphabétique décroissant 
                        de nom de famille.
                    </li>
                    <li>
                        Mettre à jour la date de naissance de tous les clients n'ayant pas de date de naissance dans 
                        le système pour y mettre la valeur <IC>1970-01-01</IC> par défaut.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ distribue } name="distribue.sql"></DownloadBlock.File>
                    <DownloadBlock.File path={ solution } name="solution.sql"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}
