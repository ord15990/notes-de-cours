import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import solution from '../resources/laboratoire-11-solution.sql';

const category =
`SELECT product_code, product_name, list_price
FROM products
WHERE category IN ('Beverages', 'Condiments');`;

const produit = 
`SELECT p.product_code, p.product_name
FROM products p
WHERE p.id = (
    SELECT a.product_id
    FROM (
        SELECT od.product_id, od.unit_price * od.quantity AS price
        FROM order_details od
    ) a
    GROUP BY a.product_id
    HAVING SUM(a.price) = (
        SELECT MAX(b.price)
        FROM (
            SELECT a.product_id, SUM(a.price) AS price
            FROM (
                SELECT od.product_id, od.unit_price * od.quantity AS price
                FROM order_details od
            ) a
            GROUP BY a.product_id
        ) b
    )
);`;

export default class LaboratoireExplain extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <ol>
                    <li>
                        Télécharger les fichiers de la base de données Northwind dans la section des bases de 
                        données de ce site Web.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind-data.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Créer un fichier SQL qui réponds à chacun des exercices ci-dessous.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Exercices</h2>
                <ol>
                    <li>
                        Sélectionner les adresses des employés (employees) et des clients (customers).
                    </li>
                    <li>
                        Changer le nom de tous les produits pour remplacer "Northwind Traders" par "Awesome Dealer".
                    </li>
                    <li>
                        Sélectionner tous les employés (employees) dont la longueur de leur nom complet (nom et
                        prénom) est plus petite que la moyenne de la longueur des noms complets des employés.
                    </li>
                    <li>
                        Changer l'adresse courriel de tous les employés (employees) pour qu'ils suivent le standard
                        "prenom.nom@awesomedealers.ca". Assurez-vous que tout est en minuscule.
                    </li>
                    <li>
                        Sélectionner le id et le nombre de jours entre la commande et la livraison pour chaque
                        commandes (orders) qui ont été livré.
                    </li>
                    <li>
                        Écriver la requête SQL qui recherche tous les noms et prénoms de tous les clients (customers)
                        ainsi que le nom des produits qu’ils ont achetés distinctivement (pas de doublons). Vous devez
                        écrire cette requête uniquement avec des <IC>JOIN</IC>. Vous ne pouvez pas utiliser de 
                        sous-requêtes. Vos résultats devrait avoir le format suivant:
                        <table>
                            <thead><tr><th>first_name</th><th>last_name</th><th>product_name</th></tr></thead>
                            <tbody><tr><td>...</td><td>...</td><td>...</td></tr></tbody>
                        </table>
                    </li>
                    <li>
                        Écriver la même requête SQL que le numéro précédant, mais cette fois-ci, vous ne pouvez pas
                        utiliser de <IC>JOIN</IC>. Vous devez utiliser des sous-requêtes. Assurez-vous que cette nouvelle
                        requête renvois exactement les mêmes données. Pour vous aider à comparer les résultats,
                        vous pouvez ordonner les données des deux requêtes. Vos résultats devrait avoir le même format 
                        que la requête précédante.
                    </li>
                    <li>
                        Exécuter un <IC>EXPLAIN</IC> des deux requêtes précédantes. Laquelle des deux requêtes est
                        exécutées le plus efficacement sur votre SGBD et pourquoi? Les réponses ici peuvent variées
                        dépendant des requêtes que vous avez écrites et de votre version de MySQL.
                    </li>
                    <li>
                        Vous trouvez que la requête ci-dessous n’est pas efficace. Trouver un moyen de rendre cette 
                        requête plus efficace, soit en changeant son code SQL ou en faisant des modifications dans la 
                        base de données. Prouver avec un <IC>EXPLAIN</IC> que la requête est plus efficace qu’avant.
                        <CodeBlock language="sql">{ category }</CodeBlock>
                    </li>
                    <li>
                        La requête ci-dessous retourne le code et le nom du produit qui a fait le plus d’argent. Vous 
                        trouvez que la requête n’est pas efficace. Trouvez un moyen de rendre cette requête plus 
                        efficace, soit en changeant son code SQL ou en faisant des modifications dans la base de 
                        données. Prouvez avec un explain que la requête est plus efficace qu’avant.
                        <CodeBlock language="sql">{ produit }</CodeBlock>
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.sql"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}
