import React from 'react';
import Video from '../component/Video';
import ColoredBox from '../component/ColoredBox';

import step5 from '../resources/ea-step5.png'

class EAAttribut extends React.Component {
    render() {
        return <>
            <section>
                <h2>Définir les attributs</h2>
                <p>
                    Les attributs sont les informations sur l’entité que nous voulons stocker dans notre base de données.
                    L’identifiant est un attribut. Il est toutefois très rare de vouloir uniquement stocker l’identifiant. Les
                    attributs peuvent être très variés. Par exemple, une entité « élève » pourrait avoir un attribut « nom »
                    indiquant le nom de l’élève.
                </p>
                <p>
                    En général, je vais m'arranger pour que les attributs soient assez clair. Dans notre situation, il n’y a 
                    toutefois pas beaucoup d’informations. Nous mettrons donc simplement des attributs de base en fonction 
                    de la situation qui nous est donnée.
                </p>
                <ColoredBox heading="Attention">
                    Dans un vrai contexte, il serait important que vous demandiez des précisions au client pour savoir ce
                    que vous voulez stocker comme données. S’il vous manque des données, cela peut être problématique
                    pour un projet et si vous avez plus de données que nécessaire, vous utiliserez plus de mémoire, ce
                    qui coûte généralement plus cher.
                </ColoredBox>
            </section>

            <section>
                <h2>Déssiner les attributs</h2>
                <p>
                    Dans le modèle entité-association, les attributs vont se placer sous les identifiants dans chacune des
                    entités. Dans notre situation d'exemple, une fois les attributs identifiés et ajouté à notre modèle, nous 
                    pourrions avoir le résultat suivant:
                </p>
                <img src={ step5 } alt="Déssiner les attributs" />
            </section>

            <section>
                <Video title="Modèle entité-association - Attributs" src="https://www.youtube.com/embed/GCM3LJqED38" />
            </section>
        </>;
    }
}

export default EAAttribut;