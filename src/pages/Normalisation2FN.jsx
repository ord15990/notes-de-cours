import React from 'react';
import IC from '../component/InlineCode';

import step6 from '../resources/physique-step6.png'

class PhysiqueNormalisation extends React.Component {
    render() {
        return <>
            <section>
                <h2>2<sup>ème</sup> forme normale (2FN)</h2>
                <p>
                    Cette forme normale est applicable uniquement lorsque vous avez des clés primaires composées, ce qui devrait 
                    arriver très rarement. Les clés primaires composées crées par les associations plusieurs vers 
                    plusieurs (dans les tables de jonction) respecteront toujours la 2<sup>ème</sup> forme normale. 
                </p>
                <p>
                    Pour les autres clés primaires composées, si vous en avez, vous devez simplement vous demander si certaines
                    colonnes de votre table dépendent uniquement d'une partie de la clé primaire. Si oui, vous devez ajouter une autre 
                    table.
                </p>
                <p>
                    Avec l'utilisation des identifiants/clé primaire inventé (toutes ces clés primaire qui commence par <IC>id_</IC>)
                    vous ne devriez pas avoir à faire des changements pour respecter la 2<sup>ème</sup> forme normale. 
                </p>
            </section>

            <section>
                <h2>Normaliser la situation d'exemple</h2>
                <p>
                    Dans notre situation d’exemple, notre structure de données respecte déjà la 2<sup>ème</sup> forme normale.
                    Nous n'avons donc pas besoin de modifier le modèle.
                </p>
                <img src={ step6 } alt="Normalisation d'un modèle" />
            </section>
        </>;
    }
}

export default PhysiqueNormalisation;