import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock'

const boolean = 
`SELECT nom_colonne1, nom_colonne2, nom_colonne3
FROM nom_table
WHERE NOT nom_colonne1 = 5 AND nom_colonne2 = 4 OR nom_colonne3 = 3;`;

const comparaison =
`SELECT nom_colonne1, nom_colonne2, nom_colonne3
FROM nom_table
WHERE nom_colonne1 <> 'test' AND nom_colonne2 > 50 AND nom_colonne3 >= 5;`;

export default class DMLConditionSimple extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Les conditions en SQL peuvent prendre plusieurs formes. À la base, elles sont simplement un 
                    ensemble d'opérations booléenne exécuté sur des colonnes pour nous indiquer si notre requête doit 
                    s'exécuter ou non sur certaines rangées.
                </p>
                <p>
                    Les conditions se retrouveront toujours dans la clause <IC>WHERE</IC> de vos requêtes. Dans les 
                    modules subséquant, vous verrez qu'il est possible de les mettre ailleurs, comme dans la 
                    clause <IC>HAVING</IC>.
                </p>
            </section>

            <section>
                <h2>Opérateurs booléen</h2>
                <p>
                    Les opérateurs booléen en SQL sont les mêmes que dans les langages de programmation. Nous avons
                    la négation, le ET et le OU. Nous pouvons aussi utiliser les paranthèses pour grouper et prioriser 
                    nos expressions booléennes.
                </p>
                <table>
                    <tr>
                        <th>Opérateur SQL</th><th>Opérateur Java</th><th>Description</th>
                    </tr>
                    <tr>
                        <td>NOT</td><td>!</td><td>Négation booléen. Inversion de la valeur.</td>
                    </tr>
                    <tr>
                        <td>AND</td><td>&amp;&amp;</td><td>ET booléen. Vrai si les 2 valeurs sont vrai, sinon faux.</td>
                    </tr>
                    <tr>
                        <td>OR</td><td>||</td><td>OU booléen. Vrai si au moins une des 2 valeurs est vrai, sinon faux</td>
                    </tr>
                </table>
                <CodeBlock language="sql">{ boolean }</CodeBlock>
            </section>

            <section>
                <h2>Opérateurs de comparaison de base</h2>
                <p>
                    Les opérateurs de comparaison en SQL sont similaire à ceux que vous utilisez dans les langages de 
                    programmation.
                </p>
                <table>
                    <tr>
                        <th>Opérateur de comparaison SQL</th><th>Description</th>
                    </tr>
                    <tr><td>=</td><td>Est égal à</td></tr>
                    <tr><td>&lt;&gt;</td><td>N'est pas égal à (est différent de)</td></tr>
                    <tr><td>&lt;</td><td>Plus petit que</td></tr>
                    <tr><td>&lt;=</td><td>Plus petit ou égal à</td></tr>
                    <tr><td>&gt;</td><td>Plus grand que</td></tr>
                    <tr><td>&gt;=</td><td>Plus grand ou égal à</td></tr>
                </table>
                <CodeBlock language="sql">{ comparaison }</CodeBlock>
            </section>
        </>;
    }
}
