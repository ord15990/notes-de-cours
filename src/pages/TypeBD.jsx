import React from 'react';
import Video from '../component/Video'

class TypeDB extends React.Component {
    render() {
        return <>
            <section>
                <h2>Classification</h2>
                <p>
                    Les SGBD peuvent gérer différents types de base de données. Historiquement, nous divisions les bases de 
                    données en quatre type. Cette division était faite selon la structure des données sauvegardées. Toutefois, 
                    dans les dernières années, l’arrivée du « Big Data » et de l'Internet ont beaucoup changé les choses.
                    Plusieurs nouveaux types de bases de données ont été créé et nous les regroupons généralement sous une 5
                    <sup>e</sup> catégorie.
                </p>
            </section>

            <section>
                <h2>Base de données hierarchique</h2>
                <p>
                    Un des premiers types de base de données. Ce type de base de données fut (très probablement) créé
                    par IBM. Les bases de données hierarchique stockent leurs données sous la forme d’un 
                    <strong> arbre</strong>. Ce type de base de données tel qu’il était dans le passé est aujourd’hui 
                    désuet puisqu’il était très peu flexible. Le type de structure de données sous forme d’arbre est 
                    toutefois encore très utilisé (en XML ou JSON par exemple).
                </p>
                <p>
                    Voici quelques exemples de bases de données hierarchiques :
                </p>
                <ul>
                    <li>IBM IMS</li>
                    <li>Windows Registry</li>
                </ul>
            </section>

            <section>
                <h2>Base de données réseau</h2>
                <p>
                    Ce type de base de données à été créé pour essayer de répondre aux problèmes de flexibilité des
                    bases de données hierarchiques. Les bases de données réseaux stockent leurs données sous la forme
                    d’un <strong>graphe</strong>. Ce type de base de données tel qu’il était dans le passé est aujourd’hui très 
                    peu utilisé puisqu’il est très difficile à utiliser et à implémenter. Les performances de ce type de bases 
                    de données sont encore toutefois très bonne.
                </p>
                <p>
                    Voici quelques exemples de bases de données réseau :
                </p>
                <ul>
                    <li>IDS</li>
                    <li>IBM IDMS</li>
                </ul>
            </section>

            <section>
                <h2>Base de données relationnelles</h2>
                <p>
                    Ce type de base de données est aujourd’hui l’un des plus utilisé au monde. Ce type de base de
                    données est assez simple et est très flexible. C’est le type de base de données que l’on étudiera dans
                    ce cours. Les bases de données relationnelles stockent leurs données sous la forme de <strong>table</strong>. 
                    Un langage a été créer pour manipuler ces bases de données. On appele ce langage le SQL. Nous étudierons ce 
                    langage dans le cours.
                </p>
                <p>
                    Voici quelques exemples de bases de données relationnelles :
                </p>
                <ul>
                    <li>MySQL</li>
                    <li>MariaDB</li>
                    <li>Microsoft SQL Server</li>
                    <li>IBM DB2</li>
                    <li>PostgreSQL</li>
                    <li>Oracle Database</li>
                </ul>
            </section>

            <section>
                <h2>Base de données objets</h2>
                <p>
                    Ce type de base de données a été créer en complément à la base de données relationnelles. On
                    voulait pouvoir faciliter le transfert entre les objets venant de classes dans les langages de
                    programmation et la base de données. Les bases de données objets stockent leurs données sous la
                    forme... <strong>d’objet</strong>. Plusieurs SGBD supportant des bases de données relationnelles 
                    supporte aussi les bases de données objets. Malheureusement, ce type de base de données n’est pas 
                    très utilisé, principalement parce qu’il est plus difficile à utiliser que les bases de données 
                    relationnelles et qu'il apporte plus souvent des problèmes que des solutions.
                </p>
                <p>
                    Voici quelques exemples de bases de données objets:
                </p>
                <ul>
                    <li>PostgreSQL</li>
                    <li>Oracle Database</li>
                </ul>
            </section>

            <section>
                <h2>Base de données NoSQL</h2>
                <p>
                    Ces bases de données sont appelé ainsi pour « non SQL » ou « not only SQL ». Ce sont généralement des bases de
                    données qui ont été créé pour un besoin très précis et qui remplisse ce besoin de manière très
                    efficace. On a vu le nombre de bases de données NoSQL explosé entre autre avec l’arrivé du
                    « Big Data », où l’on devait stocker des quantité phénoménale de données, surtout pour le Web
                    (Facebook, Twitter, Amazon, Google). Les bases de données NoSQL ne stocke pas leurs données sous
                    une forme particulière. La structure des données est donc sous une forme <strong>variée</strong>, 
                    dépendant de la base de données utilisé et de ses besoins.
                </p>
                <p>
                    Voici quelques exemples de bases de données NoSQL:
                </p>
                <ul>
                    <li>MongoDB</li>
                    <li>Cassandra</li>
                    <li>Redis</li>
                </ul>
            </section>

            <section>
                <Video title="Vidéo sur la classification des bases de données" src="https://www.youtube.com/embed/rM3zheq5VnM" />
            </section>
        </>;
    }
}

export default TypeDB;