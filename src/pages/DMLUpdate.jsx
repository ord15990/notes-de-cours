import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const update = 
`UPDATE nom_table
SET nom_colonne1 = valeur1, nom_colonne2 = valeur2, ...
WHERE condition;`;

class DMLUpdate extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe</h2>
                <CodeBlock language="sql">{ update }</CodeBlock>
                <p>
                    La commande <IC>UPDATE</IC> permet de mettre à jour des données dans la base de données. Avec cette
                    commande, nous pouvons modifier une ou plusieurs rangées à la fois. Seules les rangées respectant la
                    condition <IC>WHERE</IC> seront modifiées. Pour plus d'information sur les conditions, vous pouvez 
                    vous rendre à la page sur les conditions de ce module.
                </p>
                <p>
                    Voici quelques points important pour la mise à jour des données:
                </p>
                <ul>
                    <li>
                        Si on ne spécifie aucune condition, toutes les données de la table seront modifiées, comme
                        pour la commande <IC>DELETE</IC>. Ce genre d'opération peut donc être très dangereuse si on ne 
                        fait pas attention. Il est donc important de bien vérifier que la condition est présente et 
                        qu’elle est bonne.
                    </li>
                    <li>
                        Pour mettre à jour un élément spécifique de la table, on utilisera souvent comme condition son
                        ID (ex:&nbsp;<IC>id_client&nbsp;=&nbsp;5</IC>).
                    </li>
                </ul>
            </section>
        </>;
    }
}

export default DMLUpdate;