import React from 'react';
import Video from '../component/Video';
import IC from '../component/InlineCode';
import Situation from '../component/Situation';

import step1 from '../resources/ea-step1.png'

class EAEntite extends React.Component {
    render() {
        return <>
            <section>
                <h2>Types d'entités</h2>
                <p>
                    Pour exécuter la première étape, il est important de comprendre ce qu’est une entité puisque
                    c’est l’élément le plus important de notre diagramme entité-association. Une entité
                    peut généralement être catégorisée parmi 4 types d'entité:
                </p>
                <dl>
                    <dt>Rôle / Personne</dt>
                    <dd>Exemple: Employé, Étudiant, Animal, Équipe</dd>
                    <dt>Objet physique</dt>
                    <dd>Exemple: Livre, Produit, Véhicule</dd>
                    <dt>Évènement</dt>
                    <dd>Exemple: Semestre, Paiement, Vol d’avion, Examen</dd>
                    <dt>Lieu</dt>
                    <dd>Exemple: Ville, Campus, Local, Route</dd>
                </dl>
            </section>

            <section>
                <h2>Définir les entités</h2>
                <p>
                    Lorsqu'il est temps de définir les entités à partir d'une situation, je vous recommande de noter tout 
                    ce qui semble être une entité. Dans la situation présenté dans ce module, nous pourrions obtenir quelque
                    chose de similaire à ceci:
                </p>
                <Situation>
                    Vous travaillez pour les ressources informatique d’une <IC>compagnie</IC> de <IC>jouet</IC>. Présentement, la
                    compagnie vend ses produits uniquement en <IC>magasin</IC>, mais elle voudrait que ses <IC>produits</IC> puissent
                    être aussi vendu en ligne sur son <IC>site&nbsp;Web</IC>. Elle veut donc que ses <IC>clients</IC> puissent faire 
                    des <IC>commandes</IC> de produits en ligne. Vous voulez créer la base de données pour conserver les
                    données des <IC>achats</IC> fait en ligne.
                </Situation>
                <ul>
                    <li>Compagnie (Rôle / Lieu)</li>
                    <li>Jouet (Objet physique)</li>
                    <li>Magasin (Lieu)</li>
                    <li>Produit (Objet physique)</li>
                    <li>Site Web (Objet physique / Lieu)</li>
                    <li>Client (Rôle)</li>
                    <li>Commande (Évènement)</li>
                    <li>Achat (Évènement)</li>
                </ul>
            </section>

            <section>
                <h2>Filtrage des entités</h2>
                <p>
                    Nous devons par la suite filtrer ces entités en nous posant 3 questions:
                </p>
                <ol>
                    <li>Est-ce que l'entité est la même qu'une autre, mais sous un autre nom?</li>
                    <li>Est-ce que nous avons besoin de sauvegarder des données sur cette entité?</li>
                    <li>Est-ce que nous allons uniquement sauvegarder une seule donnée de cette entité?</li>
                </ol>
                <p>
                    Après filtrage, voici les entité restante de notre situation:
                </p>
                <ul>
                    <li>Produit</li>
                    <li>Client</li>
                    <li>Commande</li>
                    <li><del>Compagnie</del> (Il y a une seule compagnie)</li>
                    <li><del>Magasin</del> (C'est la même chose que "Compagnie")</li>
                    <li><del>Jouet</del> (C'est la même chose que "Produit")</li>
                    <li><del>Site Web</del> (Il y a un seul site Web)</li>
                    <li><del>Achat</del> (C'est la même chose que "Commande")</li>
                </ul>
                <p>
                    Il ne nous reste donc que 3 entités, soit Produit, Client et Commande.
                </p>
            </section>

            <section>
                <h2>Déssiner les entités</h2>
                <p>
                    Lorsqu'il est temps de dessiner votre diagramme, vous devez mettre chacune des entités dans sa 
                    propre boîte. Le nom de l'entité est écrit dans le haut de la boîte et est d'une bordure horizontale.
                </p>
                <p>
                    Dans notre situation, le dessin de notre modèle jusqu'à présent ressemblerait à ceci:
                </p>
                <img src={ step1 } alt="Déssiner les entités" />
            </section>

            <section>
                <Video title="Modèle entité-association - Entités" src="https://www.youtube.com/embed/ORLmpgVOkck" />
            </section>
        </>;
    }
}

export default EAEntite;