import React from 'react';
import Video from '../component/Video';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const createDB = 
`CREATE DATABASE nom_base_donnees 
CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;`;

const dropDB = `DROP DATABASE nom_base_donnees;`;

const startDDL = 
`-- Supprimer la base de données si elle existe
DROP DATABASE IF EXISTS nom_base_donnees;

-- Créer la base de données
CREATE DATABASE nom_base_donnees 
CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

-- Utiliser la base de données pour le reste des opérations
USE nom_base_donnees;`;

export default class DDLDB extends React.Component {
    render() {
        return <>
            <section>
                <h2>Création</h2>
                <p>
                    La création de la base de données vide est facile. Vous n'avez qu'à utiliser le code SQL suivant:
                </p>
                <CodeBlock language="sql">{ createDB }</CodeBlock>
                <p>
                    Dans les vieilles version de MySQL, si l'on crée une base de données, celle-ci sera encodé 
                    en <IC>latin1_swedish_ci</IC> par défaut, ce qui cause beaucoup de problèmes surtout si nous voulons 
                    utiliser cette base de données dans une application moderne. En effet, la plupart des applications 
                    aujourd'hui préfère l'encodage standard <IC>utf-8</IC> de Unicode. C'est pour cette raison que nous utilisons 
                    le <IC>CHARACTER SET</IC> <IC>utf8mb4</IC> dans l'instruction ci-dessus. Dans les versions plus récente 
                    de MySQL, vous n'avez pas besoin de le mettre puisque c'est la valeur par défaut.
                </p>
                <p>
                    Le <IC>COLLATE</IC> sert à indiquer le mode de <IC>COLLATION</IC>. Le <IC>COLLATION</IC> n'est pas un snack que 
                    l'on manque entre nos repas, mais plutôt le mode de comparaison des chaînes de caractère dans la base de données. 
                    Dans notre exemple, on utilise le <IC>utf8mb4_unicode_ci</IC> qui indique que nous comparons les caractères en 
                    unicode. De plus, le <IC>ci</IC> à la fin indique que les chaîne sont insensible à la casse (case insensitive).
                    Bref, les chaîne <IC>AlLo</IC> et <IC>allo</IC> sont considéré comme équivalente lors de la comparaison dans la 
                    base de données. 
                </p>
                <p>
                    Dans la version courante de MySQL dans XAMPP, nous avons peu de choix pour le <IC>COLLATE</IC>, mais les versions 
                    futures proposeront d'autres options intéressantes, comme celle d'être insensible aux accents et d'avoir de 
                    meilleures performances lors des comparaisons de chaînes de caratères.
                </p>
            </section>
                
            <section>
                <h2>Supression</h2>
                <p>
                    Pour supprimer une base de données, la commande SQL est très simple. Faite toutefois attention, la 
                    suppression d'une base de données est irréversible. Voici comment faire:
                </p>
                <CodeBlock language="sql">{ dropDB }</CodeBlock>
            </section>
                
            <section>
                <h2>Exemple</h2>
                <p>
                    Je vous recommande de toujours commencer vos scripts de création de base de données de la façon 
                    suivante:
                </p>
                <CodeBlock language="sql">{ startDDL }</CodeBlock>
                <p>
                    Quelques détails sur le code ci-dessus:
                </p>
                <ul>
                    <li>
                        <IC>IF EXISTS</IC> permet de supprimer la base de données si celle-ci existe déjà et d'éviter 
                        de causer des erreur si elle n'existe pas
                    </li>
                    <li>
                        <IC>USE</IC> permet d'indiquer que nous utiliserons cette base de données pour le reste du 
                        fichier
                    </li>
                    <li>
                        L'avantage d'écrire le code de cette façon est que nous pouvons toujours réexécuter le script 
                        pour réinitialiser la base de données sans causer d'erreurs
                    </li>
                </ul>
            </section>

            <section>
                <Video title="SQL - DDL - Bases de données et tables" src="https://www.youtube.com/embed/I_k9lj_Zwvk" />
            </section>
        </>;
    }
}
