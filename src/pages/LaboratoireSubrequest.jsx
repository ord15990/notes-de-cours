import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import solution from '../resources/laboratoire-10-solution.sql'

export default class LaboratoireSubrequest extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <ol>
                    <li>
                        Télécharger les fichiers de la base de données Northwind dans la section des bases de 
                        données de ce site Web.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind-data.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Créer un fichier SQL qui réponds à chacun des exercices ci-dessous <strong>en utilisant les 
                        sous-requêtes et en évitant d'utiliser les jointures.</strong>.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Exercices</h2>
                <ol>
                    <li>
                        Sélectionner tous les produits (products) étant vendus dans la commande (order et
                        order_details) ayant le id "63".
                    </li>
                    <li>
                        Sélectionner tous les produits (products) qui n'ont jamais été commandés (orders et
                        order_details).
                    </li>
                    <li>
                        Sélectionner tous les employés (employees) ayant vendu un produit (products) dont le nom
                        contient le mot "pea".
                    </li>
                    <li>
                        Sélectionner tous les produits (products) dont le prix standard est plus grand que la moyenne
                        des prix standard de tous les produits.
                    </li>
                    <li>
                        Sélectionner toutes les commandes (orders) qui ont été livré dans la même ville (ship_city) que
                        celle de l'employé (employees) qui l'a vendu.
                    </li>
                    <li>
                        Sélectionner la moyenne de la quantité de produits vendus des commandes (orders).
                    </li>
                    <li>
                        Sélectionner toutes les commandes (orders) dont la quantité de produits est supérieur à la
                        moyenne de la quantité de produits vendus des commandes.
                    </li>
                    <li>
                        Sélectionner le id de toutes les commandes (orders) qui vendent le produit faisant le plus de
                        profit par vente (list_cost – standard_price = profit)
                    </li>
                    <li>
                        Sélectionner le ou les clients (customers) qui ont acheté la ou les commandes (orders) les plus cher
                        sans taxe ni frais de livraison.
                    </li>
                    <li>
                        Sélectionner le ou les clients (customers) qui ont acheté pour le plus gros montant en
                        commandes (orders) sans taxe ni frais de livraison. Il faut faire ici la somme des commandes pour 
                        chaque client. On veut donc le ou les clients qui ont le plus dépensé.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargements</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ solution } name="solution.sql"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}
