import React from 'react';

export default class LaboratoireInstallation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <ol>
                    <li>Installer XAMPP ou une des ses alternatives (WAMP, MAMP, LAMP)</li>
                    <li>Installer MySQL Workbench</li>
                    <li>Installer un éditeur de code</li>
                </ol>
            </section>
        </>;
    }
}
