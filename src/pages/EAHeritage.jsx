import React from 'react';
import Video from '../component/Video';
import Situation from '../component/Situation';

import heritage from '../resources/ea-heritage.png'

class EAHeritage extends React.Component {
    render() {
        return <>
            <section>
                <h2>Définir l'héritage</h2>
                <p>
                    Lors de l’étape 1 du processus de création du modèle entité-association, il arrive parfois que nous
                    ayons des entités qui sont «&nbsp;une sorte&nbsp;» d’un autre entité. C’est un cas typique d’héritage. Ces cas
                    n’arrive pas souvent, mais lorsqu’ils arrivent, nous voudrions être capable de les repérer et de les représenter
                    dans notre diagramme entité-association. 
                </p>
                <p>
                    Voici un exemple pour vous aider:
                </p>
                <Situation>
                    Vous travaillez pour un magasin de dessert. Vous voulez faire une base de données des desserts
                    pour pouvoir mettre leurs informations sur le site Web du magasin. Le magasin vend deux types de
                    desserts: des biscuits et des gateaux. Pour les biscuits, sur le site Web, on veut afficher sa forme et
                    sa taille. Pour les gateaux, on veut afficher son nombre de portion, son type de glaçage et son type
                    de gateau. Pour tous les desserts, on veut afficher leur nom et leur quantité de calories.
                </Situation>
                <p>
                    Dans cette situation, nous avons 3 entités: des biscuits, des gateaux et des desserts. Toutefefois, les biscuits 
                    et les gateaux sont tous deux un sorte de dessert. Nous avons donc une forme d’héritage.
                </p>
            </section>

            <section>
                <h2>Déssiner l'héritage</h2>
                <p>
                    Lorsque nous avons un cas d'héritage, nous mettrons les entités une dans l'autre de la façon suivante:
                </p>
                <img src={ heritage } alt="Déssiner les associations" />
                <p>
                    Dans le modèle entité-association ci-dessus, puisque biscuit et gateau sont dans la boîte dessert, cela
                    indique que les biscuits et les gateaux sont des desserts. Les biscuits et les gateaux ont donc eux aussi
                    les attributs nom et calorie des desserts.
                </p>
            </section>

            <section>
                <Video title="Modèle entité-association - Héritage" src="https://www.youtube.com/embed/YFoCZnxtHDM" />
            </section>
        </>;
    }
}

export default EAHeritage;