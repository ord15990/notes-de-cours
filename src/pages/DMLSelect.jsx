import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock'

const select = 
`SELECT nom_colonne1, nom_colonne2, ...
FROM nom_table
WHERE condition;`;

const selectAll =
`SELECT * FROM nom_table;`;

const selectAlias =
`SELECT 
    nas AS 'numero_assurance_social',
    ddn AS 'date_de_naissance'
FROM nom_table;`;

class DMLSelect extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe</h2>
                <CodeBlock language="sql">{ select }</CodeBlock>
                <p>
                    La commande <IC>SELECT</IC> permet de faire une recherche de données dans la base de données. Avec
                    cette commande, nous pouvons rechercher une ou plusieurs rangées à la fois dans une table. IL est 
                    aussi possible de simplement regarder si des rangées existes. Les rangées retournées sont celles 
                    qui respectent la condition <IC>WHERE</IC>. Pour plus d'information sur les conditions, vous pouvez 
                    vous rendre à la page sur les conditions de ce module.
                </p>
                <p>
                    Voici quelques points important pour la recherche des données:
                </p>
                <ul>
                    <li>
                        Si on ne spécifie aucune condition, toutes les données de la table seront retourné dans votre 
                        recherche. Ce genre d'opération est souvent voulu sur de petites tables, mais peut être très 
                        long sur une table qui contient beaucoup de données.
                    </li>
                    <li>
                        Seules les colonnes spécifiées sont retournées pour la recherche. Cela vous permet de faire 
                        des recherches qui retournent uniquement les données que vous recherchez, et non les rangées 
                        au complet.
                    </li>
                    <li>
                        Il est possible de faire une recherche sur toutes les colonnes sans avoir à spécifier chaque 
                        colonne dans votre requête en utilisant le symbole de l'astérisque (<IC>*</IC>)
                        <CodeBlock language="sql">{ selectAll }</CodeBlock>
                    </li>
                    <li>
                        Il est possible de donner des alias aux colonnes retournées par une recherche à l'aide du 
                        mot-clé <IC>AS</IC>. Cela nous permet entre autres de renommer des noms de colonnes pour 
                        qu'ils soient plus facile à comprendre pour ceux qui vont lire les données de vos recherches.
                        <CodeBlock language="sql">{ selectAlias }</CodeBlock>
                    </li>
                </ul>
            </section>
        </>;
    }
}

export default DMLSelect;