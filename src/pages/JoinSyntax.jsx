import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const syntax = 
`SELECT table_origine.champ, table_jointe.champ, ...
FROM table_origine
[TYPE_JOINTURE] JOIN table_jointe ON table_origine.table_jointe_id = table_jointe.id;`;

export default class JoinSyntax extends React.Component {
    render() {
        return <>
            <section>
                <h2>Syntaxe de base</h2>
                <p>
                    Pour utiliser les jointures, nous utiliserons le mot-clé <IC>JOIN</IC> du langage SQL. Ce mot-clé 
                    s'utilise immédiatement après la clause <IC>FROM</IC> de vos requêtes SQL. Voici la syntaxe de 
                    base des jointures:
                </p>
                <CodeBlock language="sql">{ syntax }</CodeBlock>
                <p>
                    Voici quelques informations sur ce type de requêtes:
                </p>
                <ul>
                    <li>
                        Lorsque nous avons un <IC>JOIN</IC>, il peut y avoir des conflits de noms entre les champs des 
                        différentes tables. Nous accèderons donc aux champs des différentes tables en les précédant du 
                        nom de la table pour éviter tout conflit et faciliter la lecture de la requête.
                    </li>
                    <li>
                        Nous remplacerons <IC>[TYPE_JOINTURE]</IC> par le mot-clé associé à la jointure que nous 
                        voulons faire. Nous verrons les 3 types de jointure dans les pages suivantes de ce module.
                    </li>
                    <li>
                        Dans la section <IC>ON</IC> de la clause <IC>JOIN</IC>, nous spécifions sur quel champ les 
                        tables sont liées. Nous utiliserons généralement les 2 champs utilisé dans la clé étrangère 
                        entre les 2 tables ici. L’ordre dans lequel nous mettons les champs (avant ou après 
                        le <IC>=</IC>) ne change rien.
                    </li>
                </ul>
            </section>
        </>;
    }
}
