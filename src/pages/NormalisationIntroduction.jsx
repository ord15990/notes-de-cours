import React from 'react';
import Video from '../component/Video';

import restrictive from '../resources/normalisation-restrictive.png';

class PhysiqueNormalisation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Formes normales</h2>
                <p>
                    Dans le but de créer notre modèle physique, nous voulons que notre modèle respecte ce qu’on
                    appelle les formes normales. Les formes normales sont différents niveaux d’organisation des données
                    qui nous amènerons vers une structure de base de donnée optimale. Il existe au de nombreuses formes
                    normales, mais nous appliquerons seulement les trois premières puisque les autres ne sont pas
                    vraiment nécessaire. 
                </p>
                <p>
                    La normalisation a quelques buts important:
                </p>
                <ul>
                    <li>Éviter la redondance (répétition) des données</li>
                    <li>Avoir de meilleur performance</li>
                    <li>Diminuer le poids de la base de données</li>
                    <li>Avoir une meilleur intégrité (cohérence) des données</li>
                </ul>
                <p>
                    Les formes normales sont en ordre de restriction. Ainsi, la 2<sup>ème</sup> forme normale est plus 
                    restrictive que la 1<sup>ère</sup>. De même, la 3<sup>ème</sup> forme normale est plus restrictive 
                    que les 2 précédante.
                </p>
                <p>
                    De ce fait, pour respecter une forme normale, il faut aussi respecter les formes normales précédentes. 
                    Bref, si vous voulez respecter la 3<sup>ème</sup> forme normale, vous devez respecter la 
                    1<sup>ère</sup> et la 2<sup>ème</sup> forme normale.
                </p>
                <img src={ restrictive } alt="Ordre de restriction des formes normales"/>
            </section>

            <section>
                <Video title="Modèle physique - Normalisation" src="https://www.youtube.com/embed/nmvM_iGj3HA" />
            </section>
        </>;
    }
}

export default PhysiqueNormalisation;