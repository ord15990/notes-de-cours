import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const groupCar =
`SELECT marque, prix_vente
FROM vente_auto
GROUP BY marque;`;

const groupAggregate = 
`SELECT marque, SUM(prix_vente) AS total
FROM vente_auto
GROUP BY marque;`;

export default class GroupAggregate extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    La force de la clause <IC>GROUP BY</IC> est avec les fonctions d'agrégation. En effet, si nous 
                    exécutons la requête suivante, qui n'utilise pas de fonction d'agrégation, sur notre ensemble de 
                    données d'exemple, nous aurons un résultat plutôt moche.
                </p>
                <CodeBlock language="sql">{ groupCar }</CodeBlock>
                <table>
                    <tr><th>marque</th><th>prix_vente</th></tr>
                    <tr><td>Chevrolet</td><td>1324.00</td></tr>
                    <tr><td>Hyundai</td><td>1562.00</td></tr>
                    <tr><td>Kia</td><td>9637.00</td></tr>
                    <tr><td>Toyota</td><td>12111.00</td></tr>
                </table>
                <p>
                    Comme vous pouvez le voir, cette requête n'est pas très pratique puisqu'elle prends seulement la 
                    première valeur de la colonne <IC>prix_vente</IC> pour chaque marque. C'est ici que les fonctions 
                    d'agrégation seront pratique.
                </p>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Utilisons maintenant la clause <IC>GROUP BY</IC> avec une fonction d'agrégation:
                </p>
                <CodeBlock language="sql">{ groupAggregate }</CodeBlock>
                <table>
                    <tr><th>marque</th><th>total</th></tr>
                    <tr><td>Chevrolet</td><td>13851.00</td></tr>
                    <tr><td>Hyundai</td><td>10240.00</td></tr>
                    <tr><td>Kia</td><td>19289.00</td></tr>
                    <tr><td>Toyota</td><td>33949.00</td></tr>
                </table>
                <p>
                    Nous voyons ici que la sommes de la colonne prix_vente s'est faite pour chaque marque. Nous
                    pourrions le faire aussi avec les autres fonctions d'agrégation:
                </p>
                <ul>
                    <li>
                        <IC>MAX</IC>: Retourne le <IC>prix_vente</IC> maximum pour chaque marque.
                    </li>
                    <li>
                        <IC>MIN</IC>: Retourne le <IC>prix_vente</IC> minimum pour chaque marque.
                    </li>
                    <li>
                        <IC>COUNT</IC>: Retourne le nombre d'automobile pour chaque marque.
                    </li>
                    <li>
                        <IC>AVG</IC>: Retourne la moyenne des <IC>prix_vente</IC> pour chaque marque.
                    </li>
                </ul>
            </section>
        </>;
    }
}
