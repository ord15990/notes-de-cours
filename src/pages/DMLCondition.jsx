import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock'

const isnull = 
`SELECT nom_colonne1, nom_colonne2
FROM nom_table
WHERE nom_colonne1 IS NULL AND nom_colonne2 IS NOT NULL;`;

const between = 
`DELETE FROM nom_table
WHERE nom_colonne1 BETWEEN '2020-01-01' AND '2020-12-31';`;

const betweenSlow = 
`-- Ceci est généralement plus lent
DELETE FROM nom_table
WHERE nom_colonne1 >= '2020-01-01' AND nom_colonne1 <= '2020-12-31';`;

const inSql =
`UPDATE nom_table
SET nom_colonne1 = nouvelle_valeur
WHERE nom_colonne1 IN ('valeur1', 'valeur2', 'valeur3');`;

const like0N =
`-- Sélectionne toutes les villes dont le nom se 
-- termine par "ville":
-- ville, Victoriaville, Drummundville, Boucherville, etc.
SELECT * FROM ville
WHERE nom_ville LIKE '%ville';`;

const like11 =
`-- Sélectionne toutes les personnes dont le nom se 
-- termine par "ay" et qui débute par une lettre quelconque:
-- Jay, Say, May, Ray, etc.
SELECT * FROM personne
WHERE prenom LIKE '_ay';`;

const likeCombine =
`-- Valide: Jonathan, Jaunathan, Anath
-- Invalide: Nathan (Nécessite au moins un caractère avant le "nath")
SELECT * FROM personne
WHERE prenom LIKE '_%nath%';`;

const likeNot = 
`-- Sélectionne toutes les personnes dont le prénom
-- ne contient pas la lettre "c" minuscule
SELECT * FROM personne
WHERE prenom NOT LIKE '%c%';`;

export default class DMLCondition extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Bien qu'à la base, les conditions en SQL sont simplement un ensemble d'opérations booléennes 
                    exécutées sur des colonnes, elles peuvent être légèrement plus complexe. Nous verrons ici comment 
                    utiliser certains mot-clé du langage SQL pour préciser nos conditions.
                </p>
                <p>
                    Les conditions se retrouveront toujours dans la clause <IC>WHERE</IC> de vos requêtes. Dans les 
                    modules subséquant, vous verrez qu'il est possible de les mettre ailleurs, comme dans la 
                    clause <IC>HAVING</IC>.
                </p>
            </section>

            <section>
                <h2>Is Null</h2>
                <p>
                    En SQL, il n’est pas possible de comparer la valeur d’une colonne avec <IC>NULL</IC> à l’aide des 
                    opérateur de comparaison de base. Nous utilisons donc les opérateurs <IC>IS NULL</IC> ou <IC>IS NOT NULL</IC>.
                </p>
                <CodeBlock language="sql">{ isnull }</CodeBlock>
            </section>

            <section>
                <h2>Between</h2>
                <p>
                    Pour indiquer qu'une colonne devrait être entre deux valeurs inclusivement, il est préférable 
                    d'utiliser le mot-clé <IC>BETWEEN</IC>. Ce mot-clé est inclusif, ce qui veut dire que la valeur de 
                    départ et de fin sont inclus dans la condition.
                </p>
                <CodeBlock language="sql">{ between }</CodeBlock>
                <p>
                    Il est possible de faire une comparaison similaire avec deux conditions, mais cela est
                    généralement plus lent que d’utiliser le mot-clé <IC>BETWEEN</IC> dépendemment du SGBD que vous 
                    utilisez.
                </p>
                <CodeBlock language="sql">{ betweenSlow }</CodeBlock>
            </section>

            <section>
                <h2>In</h2>
                <p>
                    Le mot-clé <IC>IN</IC> indique qu’une colonne peut prendre plusieurs valeurs. Ainsi, dans l’exemple
                    ci-dessous, nous faisons une mise à jour sur toutes les rangées dont la valeur de la 
                    colonne <IC>nom_colonne1</IC> est soit <IC>valeur1</IC>, <IC>valeur2</IC> ou <IC>valeur3</IC>.
                </p>
                <CodeBlock language="sql">{ inSql }</CodeBlock>
            </section>

            <section>
                <h2>Like</h2>
                <p>
                    Le mot-clé <IC>LIKE</IC> permet d’indiquer que la valeur de la colonne doit suivre un certain 
                    patron. Ce mot-clé fonctionne comme un <IC>=</IC>, mais avec l'option de lui ajouter des 
                    wildcards. En SQL, il existe 2 wildcards:
                </p>
                <dl>
                    <dt><IC>%</IC></dt>
                    <dd>
                        Indique zéro, un ou plusieurs caractères quelconque.
                        <CodeBlock language="sql">{ like0N }</CodeBlock>
                    </dd>

                    <dt><IC>_</IC></dt>
                    <dd>
                        Indique un seul caractère quelconque.
                        <CodeBlock language="sql">{ like11 }</CodeBlock>
                    </dd>
                </dl>
                <p>
                    Il est possible de combiner les deux wildcards pour produire des patrons plus complexe. Dans
                    l’exemple suivant, on sélectionne toutes les personnes dont les prénoms commence par un ou
                    plusieurs caractère quelconque, suivi de <IC>nath</IC> et finissant par zéro ou plusieurs caractère
                    quelconque.
                    Valide : Jonathan, Jaunathan, Anath
                    Invalide : Nathan (on doit avoir au moins un caractère avant le « nath »)
                </p>
                <CodeBlock language="sql">{ likeCombine }</CodeBlock>
                <p>
                    Il est possible d’utiliser le mot clé <IC>NOT</IC> devant le <IC>LIKE</IC> pour trouver toutes les rangées 
                    ne respectant pas le patron.
                </p>
                <CodeBlock language="sql">{ likeNot }</CodeBlock>
            </section>
        </>;
    }
}
