import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

import innerJoin from '../resources/join-inner.png';

const exemple = 
`SELECT membre.prenom, membre.nom, cours.nom
FROM membre 
INNER JOIN cours ON membre.id_cours = cours.id_cours`;

export default class JoinInner extends React.Component {
    render() {
        return <>
            <section>
                <h2>Description</h2>
                <p>
                    Le <IC>INNER JOIN</IC> permet de spécifier que nous sélectionnons seulement les données des deux 
                    tables qui sont en relation directe avec une donnée dans l’autre table et vice-versa. 
                </p>
                <p>
                    Nous pouvons représenter les données retournées par le <IC>INNER JOIN</IC> avec le diagramme 
                    suivant:
                </p>
                <img src={ innerJoin } alt="Inner Join"/>
                <ColoredBox heading="À noter">
                    Dans 99% de vos requêtes, c'est ce type de jointure que vous voulez utiliser. Le autres types de 
                    jointures seront utiles seulement dans certains cas.
                </ColoredBox>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Voici un exemple de <IC>INNER JOIN</IC> avec notre situation d'exemple des cours du gym:
                </p>
                <CodeBlock language="sql">{ exemple }</CodeBlock>
                <p>
                    La requête ci-dessus produira le résultat suivant:
                </p>
                <table>
                    <tr><th>membre.prenom</th><th>membre.nom</th><th>cours.nom</th></tr>
                    <tr><td>Maxime</td><td>Tremblay</td><td>Yoga</td></tr>
                    <tr><td>Bob</td><td>Ross</td><td>Yoga</td></tr>
                    <tr><td>John</td><td>Cena</td><td>Kickboxing</td></tr>
                </table>
                <p>
                    Vous noterez que le membre Sasha Ketchum ne se retrouve pas dans les résultats de cette recherche.
                    De la même façon, le cours de pilates n'est pas dans les résultats. Ceci est dû 
                    au <IC>INNER JOIN</IC> qui retournent uniquement les éléments qui ont une relation avec l'autre 
                    table.
                </p>
                <p>
                    Dans ce cas-ci, Sasha Ketchum ne prends aucun cours puisque <IC>id_cours = NULL</IC>, il 
                    n'est donc pas dans les résultats de la recherche. De la même façon, le cours de pilates n'est 
                    pris par aucun membre, il est donc exclu des résultats de la recherche.
                </p>
            </section>
        </>;
    }
}
