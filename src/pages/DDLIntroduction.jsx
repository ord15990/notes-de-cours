import React from 'react';
import Video from '../component/Video';
import IC from '../component/InlineCode'

import typesSql from '../resources/ddl-types-sql.png'
import workbench from '../resources/ddl-workbench.png'
import phpmyadmin from '../resources/ddl-phpmyadmin.png'
import physique from '../resources/ddl-exemple.png'

class PhysiqueIntroduction extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Vous avez maintenant le modèle final de votre base de données. Votre modèle physique sera votre 
                    plan pour construire la base de données dans un SGBD. Toutefois, cette création nécessite 
                    l'utilisation d'un langage particulier: le SQL.
                </p>
                <ol>
                    <li>Création du modèle entité-association</li>
                    <li>Création du modèle physique</li>
                    <li><strong>Génération du code SQL DDL</strong></li>
                </ol>
                <p>
                    Le but de ce module sera de créer un fichier de type <IC>.sql</IC> qui, une fois exécuté, créera 
                    votre base de données dans votre SGBD.
                </p>
            </section>

            <section>
                <h2>Types de SQL</h2>
                <p>
                    Le code SQL est un langage pour gérer, maintenir et rechercher dans les bases de données 
                    relationnelle. Ce langage est divisé en 4 types de SQL, chacun ayant son propre rôle:
                </p>
                <dl>
                    <dt>Data Definition Language (DDL)</dt>
                    <dd>
                        On utilise ces instructions pour créer, modifier et supprimer les bases de données et leurs
                        tables. Le but de ces instructions est de définir la structure des bases de données.
                    </dd>
                    
                    <dt>Data Manipulation Language (DML)</dt>
                    <dd>
                        On utilise ces instructions pour ajouter, modifier, supprimer ou rechercher des données dans
                        une base de données.
                    </dd>

                    <dt>Data Control Language (DCL)</dt>
                    <dd>
                        On utilise ces instructions pour donner ou retirer des droits d’accès aux utilisateurs de la base
                        de données. Le but de ces instructions est de sécuriser la base de données.
                    </dd>

                    <dt>Transaction Control Language (TCL)</dt>
                    <dd>
                        On utilise ces instructions pour créer des transactions complexes sur la base de données.
                    </dd>
                </dl>
                <img src={ typesSql } alt="Types de SQL"/>
                <p>
                    Ce module traitera du DDL, mais nous verrons aussi le DML dans les modules suivants. Le DCL et le TCL 
                    seront abordé dans un autre cours.
                </p>
            </section>

            <section>
                <h2>Écrire et exécuter du SQL</h2>
                <p>
                    Pour créer une base de données, nous utiliserons du code code SQL. Nous allons donc créer un 
                    fichier de code <IC>.sql</IC> pour contenir notre code de création de base de données. Pour ce 
                    faire, vos pouvez utiliser les outils suivants:
                </p>
                <ul>
                    <li>MySQL Workbench</li>
                    <li>Un éditeur de code</li>
                </ul>
                <p>
                    Une fois votre SQL prêt, vous pourrez l'exécuter de la façon suivante:
                </p>
                <ul>
                    <li>
                        En ouvrant votre fichier SQL dans MySQL Workbench, puis en cliquant sur le bouton d'exécution 
                        du script (<span class="material-icons md-18">flash_on</span>). Assurez-vous de ne pas 
                        sélectionner de texte dans le fichier SQL pour que le fichier en entier soit exécuté.
                        <img src={ workbench } alt="Exécuter du SQL dans MySQL Workbench"/>
                    </li>
                    <li>
                        En copiant le contenu de votre fichier dans l'onglet SQL de phpMyAdmin puis en cliquant sur 
                        le bouton Go. Assurez-vous d'être à la racine du serveur, et non pas dans une autre base de 
                        données lorsque vous exécutez le script.
                        <img src={ phpmyadmin } alt="Exécuter du SQL dans phpMyAdmin"/>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Exemple</h2>
                <p>
                    Dans ce module, nous continuerons notre exemple de la base de données de la boutique de jouet. 
                    Nous utiliserons donc le modèle physique bâtit au module précédent:
                </p>
                <img src={ physique } alt="Types de SQL"/>
            </section>

            <section>
                <Video title="SQL - DDL - Introduction" src="https://www.youtube.com/embed/qnUuOSKw3RA" />
            </section>
        </>;
    }
}

export default PhysiqueIntroduction;