import React from 'react';
import CodeBlock from '../component/CodeBlock';

const checkUser = 
`SELECT * 
FROM users 
WHERE name = 'Username venant de la page de connexion';`;

const username = 
`test'; DROP TABLE users; --`;

const inject =
`SELECT * 
FROM users 
WHERE name = 'test'; DROP TABLE users; --';`

const escapeStatementJs = 
`let connection = await connectionPromise;
connection.query(
    \`INSERT INTO products(
        product_code, 
        product_name, 
        description, 
        standard_cost, 
        list_price)
        VALUES(?, ?, ?, ?, ?)\`,
    [productCode, productName, description, standardCost, listPrice]
);`;

const escapeStatementCSharp = 
`using (SqlConnection connection = new SqlConnection(connectionString))
{
    connection.Open();
    SqlCommand command = new SqlCommand(null, connection);

    // Create and prepare an SQL statement.
    command.CommandText =
        "INSERT INTO Region (RegionID, RegionDescription) " +
        "VALUES (@id, @desc)";
    SqlParameter idParam = new SqlParameter("@id", SqlDbType.Int, 0);
    SqlParameter descParam = new SqlParameter("@desc", SqlDbType.Text, 100);
    command.Parameters.Add(idParam);
    command.Parameters.Add(descParam);
    command.Prepare();

    command.Parameters[0].Value = 7;
    command.Parameters[1].Value = "Outaouais";
    command.ExecuteNonQuery();
}`;

export default class SecurityInject extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Lorsque vous branchez votre base de données à une application, vous devez prendre plusieurs mises 
                    en garde pour empêcher les utilisateurs malicieux d'accéder ou de corrompre vos données. En effet, 
                    une bonne partie du travail d'un gestionnaire de base de données consiste à sécuriser la base de 
                    données en donnant des droits d'accès spécifique à certains comptes, et en s'assurant que chaque 
                    utilisateur ait un mot de passe sécuritaire.
                </p>
                <p>
                    Bien que ce genre de gestion sorte complètement du cadre de ce cours, il y a un autre problème de 
                    sécurité que nous, les développeurs, pouvont aider à corriger. Ce problème est l'un des plus connu 
                    pour les bases de données relationnelle:
                </p>
                <p>
                    L'injection de SQL.
                </p>
            </section>

            <section>
                <h2>Comment injecter du SQL</h2>
                <p>
                    Supposons que vous programmez une page de connexion pour un site Web. Dans cette page de 
                    connexion, vous envoyez une requête SQL pour voir si le nom de l'utilisateur existe dans la base 
                    de données.
                </p>
                <CodeBlock language="sql">{ checkUser }</CodeBlock>
                <p>
                    À première vu, tout semble correct. Mais un utilisateur malicieux pourrait entrer un nom 
                    d'utilisateur problématique, comme le suivant:
                </p>
                <CodeBlock language="txt">{ username }</CodeBlock>
                <p>
                    Si vous entrez cette valeur dans votre requête, vous aurez donc la requête suivante:
                </p>
                <CodeBlock language="sql">{ inject }</CodeBlock>
                <p>
                    Un seul nom d'utilisateur malicieux et vous perdez l'ensemble des données de vos utilisateurs. Un 
                    pirate informatique peut donc supprimer complètement vos tables sans aucun problème. Il est
                    donc nécessaire de sécuriser les requêtes qui utilisent des paramètres.
                </p>
                <img src="https://imgs.xkcd.com/comics/exploits_of_a_mom.png" alt="Exploits of a Mom"/>
            </section>

            <section>
                <h2>Prévenir les injections SQL</h2>
                <p>
                    Les prepared statements sont une façon simple de se protéger contre les injections SQL. Presque 
                    chaque librairie de code permettant la connexion à une base de données permet d'utiliser ce 
                    concept ou un concept similaire pour empêcher ce problème de sécurité majeur. Essentiellement, la 
                    librairie de code vous offrira un moyen de paramétrer vos requêtes et s'assurera, à votre place, 
                    que tous les caractères spéciaux seront dans des séquences d'échappement pour empêcher leur 
                    exécution littérale.
                </p>
                <p>
                    Voici quelques exemples avec différents langages de programmation:
                </p>
                <CodeBlock language="js">{ escapeStatementJs }</CodeBlock>
                <CodeBlock language="csharp">{ escapeStatementCSharp }</CodeBlock>
            </section>
        </>;
    }
}
