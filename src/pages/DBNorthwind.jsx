import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import northwind from '../resources/db-northwind.zip'
import northwindPhysique from '../resources/db-northwind-physique.png'

export default class DBNorthwind extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    La base de données Northwind est une base de données fictives permettant de tester des requêtes 
                    SQL. Elle a été développé par Microsoft pour leurs propres systèmes de gestion de bases de 
                    données, soit Microsoft Access et SQL Server. Nous utiliserons toutefois une version de cette base 
                    de données pour MySQL ou MariaDB.
                </p>
                <p>
                    Cette base de données simule un magasin qui vend ses produits en ligne. Vous trouverez les schémas 
                    de la base de données dans les téléchargements.
                </p>
            </section>

            <section>
                <h2>Installation</h2>
                <p>
                    Voici les étapes pour installer la base de données Northwind sur MySQL ou MariaDB:
                </p>
                <ol>
                    <li>
                        Télécharger les fichiers de la base de données Northwind ci-dessous.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                    <li>
                        Exécuter le script SQL <IC>northwind-data.sql</IC> avec PHPMyAdmin ou MySQL Workbench.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Téléchargement</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ northwind } name="northwind.zip"></DownloadBlock.File>
                    <DownloadBlock.File path={ northwindPhysique } name="northwind-physique.png"></DownloadBlock.File>
                </DownloadBlock>
            </section>

            
        </>;
    }
}
