import React from 'react';
import Video from '../component/Video';
import ColoredBox from '../component/ColoredBox'
import Situation from '../component/Situation';

class EACreation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    Avant de commencer à utiliser les SGBD, nous allons apprendre à bien créer une base de donnée. Ce
                    processus est assez complexe, mais devient vite facile avec un peu de pratique. 
                </p>
                <p>
                    Le processus de création d’une base de données se fera en trois étapes qui sont chacune divisées en
                    plusieurs autres étapes. Voici un bref aperçu des trois étapes de création d’une base de données.
                </p>
                <ol>
                    <li><strong>Création du modèle entité-association</strong></li>
                    <li>Création du modèle physique</li>
                    <li>Génération du code SQL DDL</li>
                </ol>
                <p>
                    Dans ce module, nous traiterons uniquement de la création du modèle entité-association. C’est l’étape 
                    la plus importante et la plus difficile lorsque l’on crée une base de données.
                    Le modèle entité-association est en fait un schémas qui nous permettra d’organiser les différents
                    éléments de la base de donnée. Notre but sera de créer ce diagramme en fonction de nos besoins et de
                    notre situation.
                </p>
                <ColoredBox heading="À noter">
                    Bien que le vrai terme soit, « modèle entité-association », il est très fréquement appelé « modèle entité-relation ».
                    Dans ce contexte, les termes « relation » et « association » sont généralement
                    interchangeable.
                </ColoredBox>
            </section>
            
            <section>
                <h2>Source d'information</h2>
                <p>
                    La création d’une base de données dépends énormément des données que nous devons stockées. En
                    effet, nous ne stockerons pas nécessairement des données sur des étudiants ou des livres de la même
                    façon. Nous partirons donc toujours d’une situation de départ. Dans notre cas, pour des fins
                    d’apprentissage, nous utiliserons un petit texte décrivant ce que nous voulons comme système et nous
                    devrons créer à partir de cette situation un modèle entité-association répondant aux besoins indiqués.
                </p>
                <p>
                    Pour les exemples dans ce module, nous utiliserons le petit texte suivant comme situation:
                </p>
                <Situation>
                    Vous travaillez pour les ressources informatique d’une compagnie de jouet. Présentement, la
                    compagnie vend ses produits uniquement en magasin, mais elle voudrait que ses produits puissent
                    être aussi vendu en ligne sur son site Web. Elle veut donc que ses clients puissent faire des
                    commandes de produits en ligne. Vous voulez créer la base de données pour conserver les
                    données des achats fait en ligne.
                </Situation>
                <ColoredBox heading="À noter">
                    Dans l’industrie, vous aurez généralement à vous assoir avec un client et à déterminer les besoins du
                    client en discutant avec celui-ci. C'est souvent une tâche difficile parce que peu de clients sont
                    facilement capable de bien exprimer leurs idées.
                </ColoredBox>
            </section>

            <section>
                <h2>Création du modèle entité-association</h2>
                <p>
                    Voici les étapes pour construire un modèle entité-association:
                </p>
                <ol>
                    <li>Définir les entités</li>
                    <li>Définir les associations entre les entités</li>
                    <li>Définir les cardinalités des associations</li>
                    <li>Définir les identifiants des entités</li>
                    <li>Définir les attributs des entités</li>
                    <li>Définir les attributs des associations si nécessaire</li>
                </ol>
                <p>
                    Si vous suivez les étapes ci-dessus, vous serez en mesure de produire votre modèle entité-association. 
                    Ces étapes seront décrites dans les pages suivantes de ce module.
                </p>
            </section>

            <section>
                <Video title="Modèle entité-association - Introduction" src="https://www.youtube.com/embed/xY9BKTWsKyU" />
            </section>
        </>;
    }
}

export default EACreation;