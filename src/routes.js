import React from 'react';
import Home from './component/Home';
import Search from './component/Search';
import Group from './component/Group';
import Section from './component/Section';
import data from './data';

let routes = [
    { path: '/', exact: true, strict: true, component: Home },
    { path: '/search', exact: true, strict: true, component: Search }
];

// Add group routes
for (let group of Object.values(data.groups)) {
    routes.push({ 
        path: `/group/${ group.id }`, 
        exact: true, 
        strict: true, 
        props: { ...group },
        component: Group
    });
}

// Add section and pages routes
for (let section of data.sections) {
    // Add section routes
    routes.push({ 
        path: `/${ section.id }/`, 
        exact: true, 
        strict: true, 
        props: { ...section },
        component: Section
    });

    // Add pages routes
    for (let page of section.pages) {
        if(page.component){
            routes.push({ 
                path: `/${ section.id }/${ page.id }`, 
                exact: true, 
                strict: true, 
                component: React.lazy(() => import('./pages/' + page.component)) 
            });
        }
    }
}

export default routes
