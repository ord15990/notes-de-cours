import React from 'react';
import { withRouter } from "react-router-dom";

import Header from './component/Header';
import Aside from './component/Aside';
import Main from './component/Main';
import Footer from './component/Footer';
import data from './data';

import './App.css';

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            menuIsOpen: false,
            asideIsOpen: false,
            currentSection: null,
            currentPage: null
        };

        this.props.history.listen(this.updateCurrentSection);
    }

    updateCurrentSection = (location) => {
        // Get the first route "directory" in the current location path
        let slashPosition = location.pathname.indexOf('/', 1);
        let directory = location.pathname.substring(1, slashPosition !== -1 ? slashPosition : undefined);

        // Find the current section from the current route "directory"
        let currentSection = null;
        for(let section of data.sections){
            if(directory === section.id){
                currentSection = section;
                break;
            }
        }

        // If we found a section, we check if we are in a page in this section
        let currentPage = null;
        if(currentSection !== null && slashPosition !== location.pathname.length - 1){
            currentPage = data.pages[location.pathname.substring(slashPosition + 1)];
        }
        
        // Update current section
        this.setState({ 
            currentSection: currentSection,
            currentPage: currentPage
        });
    }

    toggleMenu = () => {
        if(window.innerWidth < 640){
            this.setState((state) => ({
                menuIsOpen: !state.menuIsOpen
            }));
        }
    }

    closeMenu = () => {
        if(this.state.menuIsOpen){
            this.toggleMenu();
        }
    }

    toggleAside = () => {   
        if(window.innerWidth < 1200){
            document.body.classList.toggle('locked');
            this.setState((state) => ({
                asideIsOpen: !state.asideIsOpen
            }));
        }
    }

    closeAside = () => {
        if(this.state.asideIsOpen){
            this.toggleAside();
        }
    }

    componentDidMount(){
        this.updateCurrentSection(this.props.location)
    }
    
    render() {
        return <>
            <Header 
                toggleMenu={ this.toggleMenu } 
                closeMenu={ this.closeMenu }
                isMenuOpen={ this.state.menuIsOpen } />
            <Main 
                hidden={ this.state.asideIsOpen }
                currentSection={ this.state.currentSection }
                currentPage={ this.state.currentPage } />
            <Aside 
                toggleAside={ this.toggleAside } 
                closeAside={ this.closeAside }
                open={ this.state.asideIsOpen }
                currentSection={ this.state.currentSection }
                currentPage={ this.state.currentPage } />
            <Footer 
                hidden={ this.state.asideIsOpen } />
        </>
    }
}

export default withRouter(App);
