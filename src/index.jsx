import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom';
import { ThemeContextProvider } from './ThemeContext'

import App from './App'

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter basename={ process.env.PUBLIC_URL }>
            <ThemeContextProvider>
                <App />
            </ThemeContextProvider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
);